<?php
include"dbconfig.php";
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$vehicle_id=$db->real_escape_string($vddid);
$token=$db->real_escape_string($token);
if( !is_numeric($vehicle_id) || $token=='' || $vehicle_id==''  ){ header("location:error_400.php"); }
$vehicle_res=$db->query(" SELECT * FROM `techs_vehicle` where vehicle_id='$vehicle_id' and token='$token' ");
if( $vehicle_res->num_rows==0 ){ header("location:error_400.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
$res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");
$row01=$res01->fetch_assoc();
$config_res=$db->query("select * from `techs_profile` where id='1' ");
$config_row=$config_res->fetch_assoc();

$rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$vehicle_row['speed_id']."' ");
$rto_row01=$rto_res01->fetch_assoc();
?>


<!DOCTYPE html>
<html>
<head>
  <title>Speed Control Online Registration Certificate</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="Speed Control Online Registration Certificate" >
  <link href="assets/bootstrap.min.css" rel='stylesheet' type='text/css' />
  <link href="assets/style.css" rel='stylesheet' type='text/css' />
  <link rel="stylesheet" href="assets/icon-font.min.css" type='text/css' />
</head>
<body class="left-side-collapsed">
    <section>
<div class="dmy" style="margin-top:20px;">
<h2 class="text-center" style="font-size:100%;">Speed Control Online Registration Certificate</h2>
              <table class="table table-condensed table-bordered tp gi">
              <tbody>
      <tr>
                
                <td>RTO: <?=$rto_row01['code'].' '.$rto_row01['area']?></td>
                <td>DATE: <?=date('d-m-Y',strtotime($vehicle_row['created_on']));?></td>
      </tr>
      <tr>
                
                <td>VEHICLE NO: <strong><?=$vehicle_row['vehicle_no']?></strong></td>
                <td>SPEED GOVERNOR MAKE: <strong>Convertz</strong></td>
      </tr>
      <tr>
                
                <td>CHISS NO: <?=$vehicle_row['chiss_no']?></td>
                <td>SL.NO: <strong><?=$vehicle_row['sl_no']?></strong></td>
      </tr>
      <tr>
                
                <td>ENGINE NO: <?=$vehicle_row['engine_no']?></td>
                <td>REF.NO: <?=$vehicle_row['ref_no']?></td>
      </tr>
      <tr>
                
                <td>VEHICLE MAKE: <?=$vehicle_row['vehicle_make']?></td>
                <td>INVOICE NO: <?=$vehicle_row['invoice_no']?></td>
      </tr>
      <tr>
                
                <td>VEHICLE MODEL: <?=$vehicle_row['vehicle_model']?></td>
                <td>SET SPEED: <?=$vehicle_row['speed']?></td>
      </tr>
      <tr>
                
                <td>VEHICLE OWNER NAME & ADDRESS: <?=$vehicle_row['vehicle_address']?></td>
                <td>DEALER NAME & ADDRESS:
                  <?=$vehicle_row['dealer_name'].'<br>'.$vehicle_row['dealer_address'].'<br>'.$vehicle_row['dealer_mobile']?>
                </td>
      </tr>
      <tr>
                
                <td>OWNER PHONE NO: <?=$vehicle_row['mobile_num01']?>, </td>
                <td>ELECTRONIC SPEED GOVERNER FITTED DATE: <?=date('d-m-Y',strtotime($vehicle_row['go_date']));?></td>
      </tr>
      <tr>
                
                <td>ELECTRONIC SPEED GOVERNER SERIAL NO: <?=$vehicle_row['sl_no']?></td>
                <td>ELECTRONIC SPEED GOVERNER RENEWAL DATE: <?=date('d-m-Y',strtotime($vehicle_row['renewal_date']));?></td>
      </tr>
      <tr>
                
                <td><img src="document/<?=$vehicle_row['governor_photo']?>" class="img-responsive" style="height:150px;" /></td>
                <td><img src="document/<?=$vehicle_row['vehicle_photo']?>" class="img-responsive" style="height:150px;" /></td>
      </tr>
      <tr>
                <td><img src="qrimage/<?=$vehicle_row['qrimage']?>" class='img-responsive' style='height:100px;'></td>
                <td>
                </td>
      </tr>
      
      

              </tbody>
              </table>
            </div><!-- /.table-responsive -->
  </section>
</body>
</html>