<?php $page_title="Profile"; include 'header.php'; extract($_REQUEST) ?> 
<?php if(isset($update))
	{
		if($_FILES['image']['name']!="")
		{
			$path='../profile/';
			$filename=time().$_FILES['image']['name'];
			move_uploaded_file($_FILES['image']['tmp_name'],$path.$filename);
			if(file_exists($path.$old_image))
			{
				unlink($path.$old_image);
			}
		}else
		{
			$filename=$old_image;
		}
		$dealer_address=$db->real_escape_string($dealer_address);
		$dealer_name=$db->real_escape_string($dealer_name);
		$new_password=$password?sha1($password):sha1($old_password);
		$newtxt_password=$password?$password:$old_password;
		$updated=$db->query("update techs_profile set firstname='".$firstname."' , lastname='".$lastname."' , email='".$email."' , phone='".$phone."' , image='".$filename."' , pwd='".$new_password."' , txt_password='".$newtxt_password."',dealer_name='$dealer_name',dealer_address='$dealer_address' where id='".$_SESSION['tech360_admin_user']."'");
		if($updated)
		{
			$result=success_alert('Updated Successfully !');
		}else
		{
			$result=error_alert('Sorry.Unable To Update?');
		}
	}
$select=$db->query("select * from a_dealer where id='".$_SESSION['dealer_id']."'"); 
$fetch=$select->fetch_assoc(); 
?>

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
         </div>
          <div class="content-body">
          <?php echo $result; ?> 
          <div class="row">
          	<div class="col-md-3">
          		<div class="card" style="">
				<div class="card-header bg-blue">
					<h4 class="card-title" id="basic-layout-colored-form-control" style="color: #fff">Profile Image</h4> 
				</div>
				<div class="card-body collapse in">
					<div class="card-block well"> 
						<img src="../profile/<?php echo $fetch['image']?$fetch['image']:'no-image.png'; ?>" width="120px" class="img-circle" id="img_preview">
						<!-- <h1 align="center"><?php echo $fetch['firstname']; ?></h1> -->
					</div>
				</div>
          	</div>
          </div>
             <div class="col-md-9">
			<div class="card" style="">
				<div class="card-header bg-blue">
					<h4 class="card-title" id="basic-layout-colored-form-control" style="color: #fff">User Profile</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements"> 
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block">  
						<form class="form form-profile" method="post" enctype="multipart/form-data">
							<div class="form-body"> 
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="userinput1">Fist Name</label>
											<input id="userinput1" class="form-control " placeholder="First Name" name="firstname" type="text" value="<?php echo $fetch['firstname']; ?>">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="userinput2">Last Name</label>
											<input id="userinput2" class="form-control" placeholder="Last Name" name="lastname" type="text" value="<?php echo $fetch['lastname']; ?>">
										</div>
									</div>
								</div> 
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="userinput5">Email</label>
											<input class="form-control" placeholder="email" id="userinput5" type="email" name="email" value="<?php echo $fetch['email']; ?>">
										</div>
									</div>
									<div class="col-md-6"> 
										<div class="form-group">
											<label>Contact Number</label>
											<input class="form-control " id="userinput7" placeholder="Contact Number" type="tel" name="phone" value="<?php echo $fetch['phone']; ?>">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Image</label>
												<input class="form-control border-default"  type="file" name="image" onchange="readURL(this);">
												<input type="hidden" name="old_image" value="<?php echo $fetch['image']; ?>">
										</div> 
									</div>
									<div class="col-md-6"> 
										<div class="form-group">
											<label>Password</label>
											<input class="form-control" id="userinput7" placeholder="Password" type="password" name="password">
											<input type="hidden" name="old_password" value="<?php echo $fetch['txt_password']; ?>">
										</div>
									</div> 
								</div>

								<div class="form-group">
									<label>Dealer Name</label>
									<input class="form-control" placeholder="Dealer Name" name="dealer_name" value="<?=$fetch['dealer_name']?>">
								</div>
								<div class="form-group">
									<label>Dealer Address</label>
									<textarea class="form-control" placeholder="Dealer Address" name="dealer_address"><?=$fetch['dealer_address']?></textarea>
								</div>

							<div class="form-actions right"> 
								<button type="submit" class="btn btn-primary" name="update">
									<i class="fa fa-check-square-o"></i> Save
								</button>
							</div>
						</form>

					</div>
					</div>
			</div>
		</div>
            </div> 
       </div>
   </div>
</div>
<?php include 'footer.php'; ?> 
 <script type="text/javascript">
        $(document).ready(function() {
            $('.form-profile').validate({     
                   rules: { 
                    firstname : { 
                        required: true,  
                    }, 
                     email : { 
                        required: true,
                        email:true 
                    },
                    phone : { 
                        required: true, 
                        number:true,
                        minlength:10,
                        maxlength:10
                    },  
                },
                messages:{
                     firstname : { 
                        required: "Enter First Name",  
                    }, 
                     email : { 
                        required: "Enter Email Address", 
                    },
                    phone : { 
                        required: "Enter Phone Number",  
                    }, 
                       
                } ,

                 highlight: function (element) {
                        $(element).addClass('has-error')
                    },
                unhighlight: function (element) {
                        $(element).removeClass(' has-error')},

            });
        });
function readURL(input) { 
  if (input.files && input.files[0]) {
    var reader = new FileReader(); 
    reader.onload = function(e) {
      $('#img_preview').attr('src', e.target.result);
    } 
    reader.readAsDataURL(input.files[0]);
  }
} 
 </script>
