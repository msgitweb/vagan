<?php

ob_start();

session_start();

if(!isset($_SESSION['dealer_id'])){

  header("location:index.php");

  exit();

}

include '../../dbconfig.php';

$con=$_REQUEST['con'];

$cdate=date('Y-m-d H:i:s');

?>

<?php

  if($con==1){

    extract($_POST);

    $res=$db->query("select * from techs_speed_governor where id=$id");

    $row=$res->fetch_assoc();

?>

  

    <form class="form-horizontal" role="form" method="post" id="form1" enctype="multipart/form-data" autocomplete="off">

            <div class="modal-body">

 <div class="form-body">

    <div class="form-group">

      <label for="donationinput1">Title</label>

      <input type="text" class="form-control square" placeholder="Title" name="title" value="<?php echo $row['title'] ?>" />

    </div>

    <div class="form-group">

      <label for="donationinput1">State <span class="manfield">*</span></label>

      <select name="state_id" class="select2 state_id1" style="width: 100%">

        <option value="">Please select the State</option>

        <?php

          $state_res=$db->query(" SELECT * FROM `techs_state`  ");

          while( $state_row=$state_res->fetch_assoc() ){

        ?>

          <option value="<?=$state_row['state_id']?>" <?php if($state_row['state_id']==$row['state_id']){ echo 'selected="selected"'; } ?> ><?=$state_row['state']?></option>

        <?php } ?>

      </select>

    </div>



    <div class="form-group">

      <label for="donationinput1">RTO <span class="manfield">*</span></label>

      <div class="rtid1">

        <select name="rto_id" class="select2 rto_id" style="width: 100%">

          <option value="">Please select RTO</option>

          <?php $res01=$db->query(" SELECT * FROM `techs_rto` where state_id='".$row['state_id']."'  ");

           while( $row01=$res01->fetch_assoc() ){ ?>

          <option value="<?=$row01['rto_id']?>" <?php if($row01['rto_id']==$row['rto_id']){ echo 'selected="selected"'; } ?> ><?=$row01['code'].' - '.$row01['area']?></option>

          <?php } ?>

        </select>

      </div>

    </div>



    <div class="form-group">

      <label for="donationinput1">Agency Name <span class="manfield">*</span></label>

      <input type="text" class="form-control square" placeholder="Agency Name" name="agency_name" value="<?php echo $row['agency_name'] ?>"  />

    </div>



    <div class="form-group">

      <label for="donationinput1">Agency Address <span class="manfield">*</span></label>

      <textarea class="form-control square" placeholder="Agency Address" name="agency_address"><?php echo $row['agency_address'] ?></textarea>

    </div>



    <div class="form-group">

      <label for="donationinput1">Agency Mobile No 1 <span class="manfield">*</span></label>

    <input type="text" class="form-control square" placeholder="Agency Mobile No 1" name="mobile01" value="<?php echo $row['mobile01'] ?>"  />

    </div>



    <div class="form-group">

      <label for="donationinput1">Agency Mobile No 2</label>

    <input type="text" class="form-control square" placeholder="Agency Mobile No 2" name="mobile02" value="<?php echo $row['mobile02'] ?>"  />

    </div>

 </div>

                                                   

  </div>

  

<div class="modal-footer">

  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>

  <button type="submit" class="btn btn-outline-danger" name="up">Save changes</button>

  <input type="hidden" name="id" value="<?php echo $row['id']; ?>"  />

</div>

      

</form>



 <script>

 $(document).ready(function(e) {

    $(".select2").select2();

     $('#form1').validate({     

           rules: { 

            title : { required: true, },

            state_id : { required: true, },

            rto_id : { required: true, },

            agency_name : { required: true, },

            agency_address : { required: true, },

            mobile01 : { required: true, }, 

          },

          messages:{

               title : {  required: "Please Enter title", }, 

               state_id : {  required: "Please Select State", }, 

               rto_id : {  required: "Please Select RTO", }, 

               agency_name : {  required: "Please Enter Agency Name", }, 

               agency_address : {  required: "Please Enter Agency Address", }, 

               mobile01 : {  required: "Please Enter Mobile Number", }, 

          } ,

    });



     $(".state_id1").change(function(event) {

        $("body").loading('start');

        if( $(this).val()!='' ){

            $.post('ajax/getdata.php', {con: '3',state_id:$(this).val()}, function(resp) {

              $(".rtid1").html(resp); $(".select2").select2(); $("body").loading('stop');

            });

        }else{

          $(".rtid1").html('<select name="rto_id" class="select2 rto_id" style="width: 100%">'

                                +'<option value="">Please select RTO</option>'

                              +'</select>'); $(".select2").select2(); $("body").loading('stop');

        }

      });

        

});

</script>

<?php

  }

?>





<?php

  if($con==2){

    extract($_REQUEST);

    $res=$db->query(" select * from techs_rto where rto_id='$id' ");

    $row=$res->fetch_assoc();

?>

  

    <form class="form-horizontal" role="form" method="post" id="form1" enctype="multipart/form-data" autocomplete="off">

            <div class="modal-body">

 <div class="form-body">

    

    <div class="form-group">

      <label for="donationinput1">State</label>

      <select name="state_id" class="select2" style="width: 100%">

        <?php

          $state_res=$db->query(" SELECT * FROM `techs_state`  ");

          while( $state_row=$state_res->fetch_assoc() ){

        ?>

          <option value="<?=$state_row['state_id']?>" <?php if($state_row['state_id']==$row['state_id']){ echo 'selected="selected"'; } ?> ><?=$state_row['state']?></option>

        <?php } ?>

      </select>

    </div>

    

    <div class="form-group">

      <label for="donationinput1">Code</label>

      <input type="text" class="form-control square" placeholder="Code" name="code" value="<?=$row['code']?>"  />

    </div>



    <div class="form-group">

      <label for="donationinput1">Area</label>

      <input type="text" class="form-control square" placeholder="Area" name="area" value="<?=$row['area']?>"  />

    </div>



 </div>

                                                   

  </div>

  

<div class="modal-footer">

  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>

  <button type="submit" class="btn btn-outline-danger" name="up">Save changes</button>

  <input type="hidden" name="id" value="<?php echo $row['rto_id']; ?>"  />

</div>

      

</form>



 <script>

 $(document).ready(function(e) {

    $(".select2").select2();

     $('#form1').validate({     

           rules: { 

            title : { required: true, }, 

        },

        messages:{

             title : {  required: "Please Enter the Title", }, 

        } ,

    });

        

});

</script>

<?php

  }

?>



<?php

  if($con==3){

    extract($_REQUEST);

     $res01=$db->query(" SELECT * FROM `techs_rto` where state_id='$state_id'  ");

?>

    <select name="rto_id" class="select2 rto_id" style="width: 100%">

      <option value="">Please select RTO</option>

      <?php while( $row01=$res01->fetch_assoc() ){ ?>

      <option value="<?=$row01['rto_id']?>"><?=$row01['code'].' - '.$row01['area']?></option>

      <?php } ?>

    </select>

<?php

}

?>



<?php

if($con==4){

extract($_REQUEST);

$vehicle_res=$db->query(" SELECT * FROM `d_dealer_certificate_new` where d_dealer_certificate_id='$d_dealer_certificate_id' ");

$vehicle_row=$vehicle_res->fetch_assoc();

$res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$vehicle_row['rto']."' ");

$row01=$res01->fetch_assoc();

$dealer=$db->query("select * from a_dealer where dealer_id='".$vehicle_row['dealer_id']."'");
$dealer_res=$dealer->fetch_assoc();

?>

<div class="table-responsive">

<table class="table">

    <!-- <tr>

      <td></td>

      <td></td>

      <td><b>Invoice Number :</b> &nbsp; <?=$vehicle_row['invoice_num']?></td>

      <td><b>Date :</b> &nbsp; <?=date('d-M Y',strtotime($vehicle_row['dte']))?></td>

    </tr> -->

    
    
    
     <tr>

        <th>RTO</th>

        <td><?=$row01['code'].'-'.$row01['area']?></td>

         <th>Email</th>

        <td><?=$vehicle_row['email'] ?></td>
    </tr>
    
    
    
    

    <tr>

        <th>Owner Name</th>

        <td><?=$vehicle_row['owner_name'] ?></td>

         <th>Company Name</th>

        <td><?=$vehicle_row['company_name'] ?></td>
    </tr>

    <tr>

        <th>Mobile Number</th>

        <td><?=$vehicle_row['contact'] ?></td>

        <th>Chassis No</th>

        <td><?=$vehicle_row['chassis_no'] ?></td>

    </tr>

    <tr>

        <th>Owner Address</th>

        <td><?=$vehicle_row['owner_address'] ?></td>

        <th>Engine No</th>

        <td><?=$vehicle_row['engine_no'] ?></td>

    </tr>

   

    
    <tr>

        <th>Vehicle Make</th>

        <td><?=$vehicle_row['make_of_vehicle'] ?></td>

        <th>Vehicle Model</th>

        <td><?=$vehicle_row['model_of_vehicle'] ?></td>

    </tr>

    <tr>

        

        <th>Veh.Date of Registration</th>

        <td><?=$vehicle_row['date'] ?></td>
        
        <th>Renewal date</th>

        <td><?=$vehicle_row['renewal_date'] ?></td>

    </tr>

    <tr>

        <th>Vehicle No</th>

        <td><?=$vehicle_row['vehicle_no'] ?></td>

        <th>COP No</th>

        <td><?=$vehicle_row['cop'] ?></td>

    </tr>

    <tr>

        <th>Type</th>

        <td><?=($vehicle_row['type']==1)?"SLD":"VTS" ?></td>

        <th>Dealer Name</th>

        <td><?=$dealer_res['name'] ?></td>

    </tr>
    <?php if($vehicle_row['type']==1){?>
    <!-- <tr>-->

    <!--    <th>SLD Make</th>-->

    <!--    <td><?=$vehicle_row['sld_make'] ?></td>-->

    <!--    <th>SLD No</th>-->

    <!--    <td><?=$vehicle_row['sld_no'] ?></td>-->

    <!--</tr>-->
    <?php } else { ?>
    
    <!-- <tr>-->

    <!--    <th>VTS Make</th>-->

    <!--    <td><?=$vehicle_row['vts_make'] ?></td>-->

    <!--    <th>VTS No</th>-->

    <!--    <td><?=$vehicle_row['vts_no'] ?></td>-->

    <!--</tr>-->
    <?php } ?>

    <tr>

        <th>Dealer Mobile No</th>

        <td><?=$dealer_res['mobile'] ?></td>

        <th>Dealer Address</th>

        <td><?=$dealer_res['address'] ?></td>

    </tr>



    <tr>

        <td><img src="<?=BASE_URL?>qrimage/<?=$vehicle_row['qr_code'] ?>" class="img-thumbnail"></td>
         
 <td><img src="../../document/<?= $vehicle_row['front']; ?>" class="img-thumbnail" style='height: 196px;
width: 445px;'></td>

 <td><img src="../../document/<?= $vehicle_row['rear']; ?>" class="img-thumbnail" style='height: 196px;
width: 445px;'></td>

 <td><img src="../../sld_logo/<?= $vehicle_row['side1']; ?>" class="img-thumbnail"style='height: 196px;
width: 445px;'></td>
<td><img src="../../sld_logo/<?= $vehicle_row['side2']; ?>" class="img-thumbnail" style='height: 196px;
width: 925px;'></td>


    </tr>

    

</table>

</div>

<?php } ?>



