<?php
ob_start();
session_start();
include("../dbconfig.php");
$con=$_REQUEST['con'];

if($con==1){
  extract($_REQUEST);
  $sql='';
  if( !empty($vehicle_no) ){
    $sql.=" AND vehicle_no='".$vehicle_no."' ";
  }
  if( !empty($speed_id) ){
    $sql.=" AND speed_id='$speed_id' ";
  }
  if( !empty($odate) ){
    $sql.=" AND date(created_on)='$odate' ";
  }
  if( $rrtype==0 ){  // all record
    $res=$db->query("select * from `techs_vehicle` where vehicle_id!='' $sql order by vehicle_id desc");
  }
  if( $rrtype==1 ){ // active
    $res=$db->query("select * from `techs_vehicle` where renewal_date>='".date('Y-m-d')."' $sql order by vehicle_id desc");
  }
  if( $rrtype==2 ){  // expired
    $res=$db->query("select * from `techs_vehicle` where renewal_date<'".date('Y-m-d')."' $sql order by vehicle_id desc");
  }
  $records = array();
  $records["data"] = array(); 
  $cnt=0;
  while($row=$res->fetch_assoc()) {

  $vw='<a href="vehicle_details.php?vehicle_id='.$row['vehicle_id'].'" title="View Vehicle Details" class="btn btn-warning btn-sm" target="_blank"><i class="icon-docs"></i></a>';
  $ed='<a href="edit_vehicle.php?vehicle_id='.$row['vehicle_id'].'" title="Update Vehicle Details" class="btn btn-success btn-sm"><i class="ft-edit"></i></a>';
  $dell='<button class="btn btn-danger btn-sm" title="Delete Order" onClick="del('.$row['order_id'].')" ><i class="ft-trash"></i></button>';
  $id = ++$cnt;

  $res01=$db->query(" SELECT * FROM `techs_rto` where state_id=29 and rto_id='".$row['speed_id']."' ");
  $row01=$res01->fetch_assoc();

    $doc_res=$db->query(" SELECT * FROM `sg360_order_document` where order_id='".$row['order_id']."' ");
  if( $doc_res->num_rows>0 ){ $doc='<a href="ajax/getdata.php?order_id='.$row['order_id'].'&con=10" title="Download Documents"  
    class="btn btn-success btn-sm" ><i class="fa fa-download"></i> Download</a>'; }else{ $doc='-'; }

    $records["data"][] = array(
      $id,
      '<span class="tag tag-default">'.$row01['code'].'-'.$row01['area'].'</span>',
      '<span class="tag tag-danger">'.$row['vehicle_no'].'</span>',
      $row['sl_no'],
      date('d-M Y',strtotime($row['renewal_date'])),
      $row['vehicle_address'],
      //$row['mobile_num01'],
      $ed.' '.$vw,
   );
  }
  echo json_encode($records);
}

if($con==2){
  extract($_REQUEST);
  $records = array();
  $records["data"] = array(); 
  $res=$db->query("select * from `techs_rto` order by rto_id desc");
  $cnt=0;
  while($row=$res->fetch_assoc()) {
    $res01=$db->query(" SELECT * FROM `techs_state` where state_id='".$row['state_id']."' ");
    $row01=$res01->fetch_assoc();
      $id = ++$cnt;
      $records["data"][] = array(
          $id,
          $row01['state'],
          str_replace(",","",$row['code']),
          '<a href="javascript:;" data-toggle="modal" class="btn btn-success btn-sm" onClick="get('.$row['rto_id'].')"><i class="ft-edit"></i></a> <button class="btn btn-danger btn-sm" onClick="del('.$row['rto_id'].')" ><i class="ft-trash"></i></button>'
       );
  }
  echo json_encode($records);
}


if($con==3){
    //  include_once(PATH."dealer_apanel/pdf/index.php");
  extract($_REQUEST);
  $sql='';
  if( !empty($vehicle_no) ){
    $sql.=" AND vehicle_no='".$vehicle_no."' AND dealer_id='".$_SESSION['dealer_id']."' ";
  }
  if( !empty($rto) ){
    $sql.=" AND rto='$rto' AND dealer_id='".$_SESSION['dealer_id']."' ";
  }
  if( !empty($date) ){
    $sql.=" AND renewal_date>='$date' AND dealer_id='".$_SESSION['dealer_id']."' ";
  }
  if( $rrtype==0 ){  // all record
    $res=$db->query("select * from `d_dealer_certificate_new` where d_dealer_certificate_id!='' $sql and dealer_id='".$_SESSION['dealer_id']."' order by d_dealer_certificate_id desc");
  }
  if( $rrtype==1 ){ // active
    $res=$db->query("select * from `d_dealer_certificate_new` where renewal_date>='".date('Y-m-d')."' $sql and dealer_id='".$_SESSION['dealer_id']."' order by d_dealer_certificate_id desc");
  }
  if( $rrtype==2 ){  // Expired
    $res=$db->query("select * from `d_dealer_certificate_new` where renewal_date<'".date('Y-m-d')."' $sql and dealer_id='".$_SESSION['dealer_id']."' order by d_dealer_certificate_id desc");
  }
  $records = array();
  $records["data"] = array(); 
  $cnt=0;
  while($row=$res->fetch_assoc()) {

   $vw='<a href="javascript:;" onClick="get('.$row['d_dealer_certificate_id'].')" data-toggle="modal" title="View Certificate Details" class="btn btn-blue btn-sm"><i class="icon-eye"></i></a>';
  
   $pr='<a href="pdf/dealer_invoice.php?d_dealer_certificate_id='.$row['d_dealer_certificate_id'].'&did='.$row['dealer_id'].'" title="Print" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-print"></i></a>';
  

//   $pr='<a href="http://demo.360degreeinfo.website/certificate/dealer_apanel/certify.php/html1.php?d_dealer_certificate_id'.$row['d_dealer_certificate_id'].'.pdf" title="Print" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-print"></i></a>';

  $id = ++$cnt;

  $res01=$db->query(" SELECT * FROM `techs_rto` where  rto_id='".$row['rto']."' ");
  $row01=$res01->fetch_assoc();


   if($row['renewal_date'] > $row['date']){
  $btn="btn btn-primary btn-sm";
   $status="Active";
      } else {
      $btn="btn btn-danger btn-sm";
     $status="Expired";
      }

    $records["data"][] = array(
      $id,
      $row['owner_name'],
      '<span class="tag tag-default">'.$row01['code'].'-'.$row01['area'].'</span>',
      
      $row['contact'],
      date('d-M Y',strtotime($row['renewal_date'])),
      
      '<a href="javascript:;" style="padding:4px" class="'.$btn.'">'.$status.'</a>',
      $vw.' '.$pr,
      
   );
  }
  echo json_encode($records);
}

?>