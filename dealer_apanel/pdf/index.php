<?php
       
        $certificate_res=$db->query("select * from d_dealer_certificate where dealer_id='".$_SESSION['dealer_id']."' order by d_dealer_certificate_id desc limit 1"); 
        $certificate_row=$certificate_res->fetch_assoc();
        $rto_res=$db->query("select * from techs_rto where rto_id='".$certificate_row['rto']."'");
        $rto_row=$rto_res->fetch_assoc();
        $dealer_res=$db->query("select * from a_dealer where dealer_id='".$_SESSION['dealer_id']."'");
        $dealer_row=$dealer_res->fetch_assoc();
        
        $file__name='PDF-'.$certificate_row['d_dealer_certificate_id'].'.pdf';
 
require_once dirname(__FILE__).'/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;



try {
    ob_start();
    include dirname(__FILE__).'/html.php';
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('P', 'Letter', 'fr');
    $html2pdf->setDefaultFont('times');
    $html2pdf->writeHTML($content); 
    $html2pdf->Output(PATH.'pdf_document/'.$file__name, 'F'); 
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
