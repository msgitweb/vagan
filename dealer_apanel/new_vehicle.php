<?php $page_title="New Certificate Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['up']==1){
    $result=success_alert('Vehicle added successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this data !');
}
extract($_REQUEST);
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  $data=array();
  foreach ($_POST as $key => $value) {
    if( !empty($value) ){ $data[$key]=@$db->real_escape_string($value); }
  }
  $image_error=0;
  $filename=$_FILES['governor_photo']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url1='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url1='governor_photo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['governor_photo']['tmp_name'],"../document/".$url1);
  }else{
    ++$image_error;
  }

  $filename=$_FILES['vehicle_photo']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url2='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url2='vehicle_photo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['vehicle_photo']['tmp_name'],"../document/".$url2);
  }else{
    ++$image_error;
  }

  if( $image_error==0 ){
    $renewal_date=date('Y-m-d',strtotime("+1 Year"));
    $token=md5($data['vehicle_no'].date('Y-m-d'));
   
    $up=$db->query(" insert into `techs_vehicle` set speed_id='".$data['speed_id']."',go_date='".$data['go_date']."',vehicle_no='".$data['vehicle_no']."',chiss_no='".$data['chiss_no']."',engine_no='".$data['engine_no']."',vehicle_make='".$data['vehicle_make']."',vehicle_model='".$data['vehicle_model']."',vehicle_address='".$data['vehicle_address']."',mobile_num01='".$data['mobile_num01']."',mobile_num02='".$data['mobile_num02']."',sl_no='".$data['sl_no']."',ref_no='".$data['ref_no']."',invoice_no='".$data['invoice_no']."',speed='".$data['speed']."',certificate='".$data['certificate']."',governor_photo='".$url1."',vehicle_photo='".$url2."',
       created_on='$cdate',updated_on='$cdate',renewal_date='$renewal_date',token='$token',dealer_name='".$data['dealer_name']."',
       dealer_address='".$data['dealer_address']."',dealer_mobile='".$data['dealer_mobile']."'  "); 
    $vehicle_id=$db->insert_id;
    if( $up ){ 
      $qrcode_link='http://demo.beema.co.in/technoservice/vehicle_details.php?vddid='.$vehicle_id.'&token='.$token;
      include"qrcode.php";
      $db->query(" update techs_vehicle set qrimage='$qimage' where vehicle_id='$vehicle_id' ");

      #################### Mail Content ########################
      ob_start();
      include"printve.php";
      $mail_cont=ob_get_clean();
      $from_email='mail@demo.beema.co.in'; //$config_row['email'];
      $reply_to_email=$config_row['email']; 
      $sender_name='Techno Service';
      require '../Mailer/PHPMailerAutoload.php';
      $mail = new PHPMailer;
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'mail.demo.beema.co.in';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'mail@demo.beema.co.in';                 // SMTP username
      $mail->Password = 'client@360';                           // SMTP password
      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;                                    // TCP port to connect to
      $mail->AddReplyTo($reply_to_email, $sender_name);
      //$mail->AddBCC($config_row['email']);
      $mail->setFrom($from_email, $sender_name);
      $mail->addAddress( $config_row['email'] );        
      $mail->isHTML(true);        
      $mail->Subject = "New Vehicle Details - ".$vehicle_no;
      $mail->Body    ="<p style='font-size: 12pt; font-family: Cambria;' >Hi,<br></p>
  <p style='font-size: 12pt; font-family: Cambria;' >A New vehicle details added. Please check it below.</p><br>".$mail_cont;
      $mail->SMTPOptions = array(
        'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
        )
      );
    $mail->send();

      #################### Mail Content END ####################

      header("location:new_vehicle.php?up=1");  

    }else{ $result=error_alert('Have some error...'); }

  }else{ $result=error_alert('File must be jpg, jpeg or png'); }

}

?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-blue bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Vehicle Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="view_vehicle.php" title="View List"><i class="fa fa-arrow-right fa-lg text-white"></i></a></li>
                              </ul>
                          </div>                
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body">
                        
                        <div class="form-group">
                          <label for="donationinput1">Speed Governor <span class="manfield">*</span></label>
                          <select name="speed_id" class="select2" style="width: 100%">
                            <option value="">Please select the Speed Governor</option>
                            <?php
                              $speed_res=$db->query(" SELECT * FROM `techs_rto` where state_id=29  ");
                              while( $speed_row=$speed_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$speed_row['rto_id'];?>" <?php if($speed_row['rto_id']==$speed_id){ echo 'selected="selected"'; } ?> ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Governor fitted Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder="Governor fitted Date" name="go_date"
                            value="<?=$go_date?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_no" value="<?=$vehicle_no?>" />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chiss_no" value="<?=$chiss_no?>"   />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Engine No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No" name="engine_no" value="<?=$engine_no?>"   />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Make" name="vehicle_make" 
                            value="<?=$vehicle_make?>"   />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Model <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Model" name="vehicle_model" 
                            value="<?=$vehicle_model?>"   />
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="donationinput1">Vehicle Owner Name & Address <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Vehicle Owner Name & Address" name="vehicle_address" ><?=$vehicle_address?></textarea>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Owner Mobile No 1 <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Owner Mobile No 1" name="mobile_num01" 
                            value="<?=$mobile_num01?>" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Owner Mobile No 2</label>
                            <input type="text" class="form-control square" placeholder="Vehicle Owner Mobile No 2" name="mobile_num02" 
                            value="<?=$mobile_num02?>" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Name <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Dealer Name" name="dealer_name" 
                            value="JAI GANESH ENTERPRICES" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Mobile No</label>
                            <input type="text" class="form-control square" placeholder="Dealer Mobile No" name="dealer_mobile" 
                            value="8939983777" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="donationinput1">Dealer Address <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Dealer Address" name="dealer_address" >NEW NO.38A , old NO.44A 2ND FLOOR ,GST ROAD GUINDY CHENNAI-600032</textarea>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Sl No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Sl.No" name="sl_no" value="<?=$sl_no?>" />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Ref No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Ref.No" name="ref_no" value="<?=$ref_no?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Invoice No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Invoice No" name="invoice_no" value="<?=$invoice_no?>" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Speed <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="speed">
                              <option value="">Please select the speed</option>
                              <option value="40" <?php if($speed=='40'){ echo 'selected="selected"'; } ?>>40</option>
                              <option value="45" <?php if($speed=='45'){ echo 'selected="selected"'; } ?>>45</option>
                              <option value="50" <?php if($speed=='50'){ echo 'selected="selected"'; } ?>>50</option>
                              <option value="60" <?php if($speed=='60'){ echo 'selected="selected"'; } ?>>60</option>
                              <option value="80" <?php if($speed=='80'){ echo 'selected="selected"'; } ?>>80</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Certificate <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="certificate">
                                <option value="">Please select Certificate</option>
                                <option value="Fitment" <?php if($certificate=='Fitment'){ echo 'selected="selected"'; } ?>>Fitment</option>
                                <option value="Re-calibration" <?php if($certificate=='Re-calibration'){ echo 'selected="selected"'; } ?>>Re-calibration</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Speed Governor Photo <span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="governor_photo"  />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Photo <span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="vehicle_photo"  />
                          </div>
                        </div>
                      </div>
                        


                        <button type="submit" name="submit" class="btn btn-primary pull-right">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                speed_id:{ required: true, }, 
                vehicle_no :{  required: true, },
                chiss_no : { required: true,  },  
                engine_no:{ required: true, },
                vehicle_make:{ required: true, },
                vehicle_model:{ required: true, },
                vehicle_address:{ required: true, },
                mobile_num01:{ required: true, },
                sl_no:{ required: true,},
                ref_no:{ required: true },
                invoice_no:{ required: true },
                speed:{ required: true },
                certificate:{ required: true },
                governor_photo:{ required: true },
                vehicle_photo:{ required: true },
                go_date:{ required: true },
                dealer_name:{ required: true },
                dealer_address:{ required: true },
                dealer_mobile:{ required: true },
            },
            messages:{
                 speed_id : { required: "Please Select Speed Governor", }, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                 chiss_no : { required: "Please enter Chiss No", }, 
                 engine_no : { required: "Please Enter Engine No",}, 
                 vehicle_make : { required: "Please Enter Vehicle Make",}, 
                 vehicle_model : { required: "Please Enter Vehicle Model",},
                 vehicle_address : { required: "Please Enter Vehicle Owner Name & Address",}, 
                 mobile_num01 : { required: "Please Enter Mobile No",}, 
                 sl_no : { required: "Please Enter Sl No",}, 
                 ref_no : { required: "Please Enter Ref No",}, 
                 invoice_no : { required: "Please Enter Invoice No",}, 
                 speed : { required: "Please select the Speed",}, 
                 certificate : { required: "Please Select the Certificate",}, 
                 governor_photo: { required: "Please upload Speed Governor Photo",}, 
                 vehicle_photo: { required: "Please upload Vehicle Photo",},
                 go_date : { required: "Please enter the date",},
                 dealer_name:{ required: "Please enter the dealer name" },
                 dealer_address:{ required: "Please enter dealer address" },
                 dealer_mobile:{ required: "Please enter dealer mobile no" },
            } ,
    });
  });
</script>