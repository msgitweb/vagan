
<?php include("../dbconfig.php");?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" media='all'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
    .red {
            color: red;
        }
        .qrr
        {
     margin-left: 13px;
    padding: 10px 10px;
    width:225px;
    height:225px;
        }
    .qrcode
    {
    width: 250px;
    height: 230px;
    border: 1px solid black;
    margin:10px 65px;
    }
        .form-area {
            background-color: #FAFAFA;
            padding: 10px 40px 60px;
            margin: 10px 0px 60px;
            border: 1px solid GREY;
        }
        
        label {
            display: inline-block;
            width: 250px;
            margin-bottom: 5px;
            font-weight: 700;
            padding: 9px 20px;
            text-align: right;
            margin-right: 0px;
        }
        
        .form-inline .form-control {
            display: inline-block;
            width: 250px;
            vertical-align: middle;
            border-bottom: 1px solid #000;
            margin-top: -20px;
            border-radius: 0px;
            background:none;
            font-weight:600;
        }
        
        .form-control:focus {
            border-color: none;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none
        }
    }
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 12px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        /*background-color: #fff;*/
        
        background-image: none;
        /* border: 1px solid #ccc; */
        
        border-radius: 4px;
        /* -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075); */
        /* box-shadow: inset 0 1px 1px rgba(0,0,0,.075); */
        
        -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    }
    label::after {
        margin-left: 30px;
    }
    .speed {
        border: 1px solid blue;
        margin: 60px 150px 0px 150px;
    }
    .limiteds {
        border: 1px solid black;
        margin: 20px 20px;
        font-family: arial;
    }
    .menu {
        font-family: arial;
        font-weight: bold;
        font-size: 35px;
    }
    .content {
        font-size: 18px;
        text-align: justify;
        font-family: arial;
        padding: 0px 0px;
        margin-top: 26px;
        margin-bottom: 25px;
    }
    .certificate {
        text-align: center;
        font-weight: 800;
        font-size: 20px;
        font-family: arial;
        text-transform: uppercase;
    }
    .approval {
        border-bottom: 1px solid #000!important;
        color: #fff;
    }
    .ps-top-to-bottom {
        /*position: relative;*/
        border-bottom:none !important;
        border: 3px solid #353da2a6;
        /*box-shadow: 2px 2px yellow, 2px 2px blue;*/
    }
    <!--.ps-top-to-bottom:before,
    .ps-top-to-bottom:after {
        content: "";
        position: absolute;
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#353da2c2), to(transparent));
        background-image: -webkit-linear-gradient(#353da2c2, transparent);
        background-image: -moz-linear-gradient(#353da2c2, transparent);
        background-image: -o-linear-gradient(#353da2c2, transparent);
        background-image: linear-gradient(#353da2c2, transparent);
        top: -3px;
        bottom: -3px;
        width: 3px;
        box-shadow: 2px 2px yellow, 2px 2px blue;
    }
    .ps-top-to-bottom:before {
        left: -3px;
    }
    .ps-top-to-bottom:after {
        right: -3px;
    }
    --> .no-border {
        border: 0;
        box-shadow: none;
    }
    .arrange {
        margin-left: -75px;
    }
    .pp {
        border-bottom: 1px solid black;
        width:100px;
    }
    .pp:focus {
        border-color: none;
        outline: 0;
        -webkit-box-shadow: none;
        box-shadow: none
    }
    .speeds {
        font-family: arial;
    }
    .qr
    {
    border: 1px solid black;
    width: 1049px;
    border-top: none;
    border-left: none;
    border-right: none;
    }
    .authority
    {
     margin-left: 50px;
    font-weight: 600;
    }
    
    </style>
</head>
<?php
 $res01=$db->query(" SELECT * FROM `d_dealer_certificate` where  d_dealer_certificate_id='".$_GET['d_dealer_certificate_id']."' ");
 
  $row01=$res01->fetch_assoc();
  
  $res02=$db->query(" SELECT * FROM `techs_rto` where  rto_id='".$row01['rto']."' ");
  $row02=$res02->fetch_assoc();

?>
<body class="limited ">
    <div class="speed ps-top-to-bottom">
        <!------ Include the above in your HEAD tag ---------->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin-left:-40px;">
                    <h1 class="menu speeds">SPEED LIMITING DEVICE</h1>
                    <h4 class="speeds">ONLINE RE-CALIBRATION CERTIFICATE </h4>
                    <h5 class="speeds">(COMPLIANCE TO AIS 018,AIS 037 STANDARD)</h5>
                    <h5 class="speeds">AIS 004( PART 3)</h5>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 arrange">
                <div class="form-areaa">
                    <form role="form">
                        <br style="clear:both">

                        <div class="form-inline">
                            <label class="speeds">RTO &nbsp;&nbsp;&nbsp;&nbsp;:</label>
                            <input type="text" class="form-control  no-border " name="rto" value="<?php echo $row02['code']?>-<?php echo $row02['area']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> VEHICLE NO :</label>
                            <input type="text" class="form-control  no-border" name="vehicle_no" value="<?php echo $row01['vehicle_no']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> CHASSIS NO :</label>
                            <input type="text" class="form-control  no-border" name="chassis_no" value="<?php  echo$row01['chassis_no']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> ENGINE NO :</label>
                            <input type="text" class="form-control  no-border" name="engine_no" value="<?php  echo$row01['engine_no']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> MAKE OF VEHICLE :</label>
                            <input type="text" class="form-control  no-border" name="make_of_vehicle" value="<?php  echo$row01['make_of_vehicle']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> MODEL OF VEHICLE :</label>
                            <input type="text" class="form-control  no-border" name="model_of_vehicle" value="<?php  echo$row01['model_of_vehicle']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> OWNER NAME :</label>
                            <input type="text" class="form-control  no-border" name="owner_name" value="<?php  echo$row01['owner_name']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> CONTACT NO :</label>
                            <input type="text" class="form-control  no-border" name="contact" value="<?php echo $row01['contact']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> OWNER ADDRESS :</label>
                            <input type="text" class="form-control  no-border" name="OWNER ADDRESS" value="<?php echo  $row01['owner_address']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> DEALER ADDRESS :</label>
                            <input type="text" class="form-control  no-border" name="DEALER ADDRESS" value="<?php echo $row01['dealer_address']?>" disabled>
                        </div>

                        <!--<button type="button" id="submit" name="submit" class="btn btn-primary pull-right">Submit Form</button>-->
                    </form>
                </div>
            </div>

            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 arrange">
                <div class="form-areaa">
                    <form role="form">
                        <br style="clear:both">

                        <div class="form-inline">
                            <label class="speeds">DATE :</label>
                            <input type="text" class="form-control  no-border" name="date" value="<?= date('d-M Y',strtotime($row01['date']))?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> VALIDITY :</label>
                            <input type="text" class="form-control  no-border" name="vdate" value="<?= date('d-M Y',strtotime($row01['vdate']))?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> SLD MAKE :</label>
                            <input type="text" class="form-control  no-border" name="sld_make" value="<?php echo $row01['sld_make']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> SLD NO :</label>
                            <input type="text" class="form-control  no-border" name="sld_no" value="<?php echo $row01['sld_no']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> TAC NO :</label>
                            <input type="text" class="form-control  no-border" name="tac_no" value="<?php echo $row01['tac_no']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> SPEED :</label>
                            <input type="text" class="form-control  no-border" name="speed" value="<?php echo $row01['speed']?>" disabled>
                        </div>
                        <div class="form-inline">
                            <label class="speeds"> COP :</label>
                            <input type="text" class="form-control  no-border" name="cop" value="<?php echo $row01['cop']?>" disabled>
                        </div>

                    </form>
                </div>
            </div>
            
        </div>
        <div class="container qr">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="qrcode">
                     <img class="qrr" src="<?=BASE_URL?>qrimage/<?php echo $row01['qr_code']; ?>" alt=""  >
                </div>
                <div class="caption">
                    
                    <p style="text-align:center;margin-left: 85px"><b>QR CODE</b></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="qrcode">
                    <img class="qrr" src="<?=BASE_URL?>document/<?php echo $row01['vehicle_photo']; ?>" alt="" >
                </div>
                <div class="caption">
                    <p style="text-align:center;margin-left: 85px"><b>VEHICLE PHOTO</b></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <p class="authority">Authority Seal</p>
            </div>
        </div>
    </div>
    </div>
   
    <div class="container" style="border: 3px solid #7b80c2;width: 1050px;border-top: none;border-bottom:none">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                <p class="content speeds">This is certify that following vehicle has been fitted with approved
                    <input class="pp no-border" type="text" name="text" id="text" value="<?php echo $row01['sld_make']?>" maxlength="5">&nbsp;&nbsp;&nbsp;fuel type /Electronic type /Cable type <b>SPEED GOVERNOR</b>,which is set to a maximum pre set speed of
                    <input class="pp no-border" type="text" name="text" id="text" value="<?php echo $row01['speed']?>" maxlength="5">&nbsp;&nbsp;&nbsp;KMPH(+/-2%) and shall not exceed this speed in any circumstances,unless the device is tampered or the seal is broken by unauthorised technician or individuals </p>
            </div>
        </div>
    </div>
    <div class="container" style="border: 3px solid #7b80c2;width: 1050px;border-top: none;border-top: 1px solid black;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center">
                <p class="certificate speeds">***This certificate is being issued only after testing ***</p>
            </div>
        </div>
    </div>

</body>

</html>