

<!-- Horizontal navigation-->

    <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">

      <!-- Horizontal menu content-->

      <div data-menu="menu-container" class="navbar-container main-menu-content">
          
          
           <nav class="header-navbar navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-center">
      <div class="navbar-wrapper">
        <div class="navbar-header" style="margin-left:10px;">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item" style="width: 300px;"><a href="dashboard.php" class="navbar-brand">
                <h4 class="brand-text hidden-md-down" style="text-align:center;"><?php echo $config_row['name'];?> </h4>
               
                </a></li>
            <li class="nav-item hidden-md-up" style="margin-top: 3%;"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <!--<li class="nav-item hidden-sm-down"><a href="dashboard.php" class="nav-link "><i class="fa fa-home font-large-2"></i></a></li>-->
             
            </ul>
             <ul class="nav navbar-nav" style="margin-top:30px;">
              
              <li class="dropdown dropdown-user nav-item">
                  
                  <a href="profile.php" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online">
                      <!--<img src="img/d_profile.png">-->
                      </span><span class="user-name"><?=$config_row['name']?></span></a>
                  
                  
                
                <div class="dropdown-menu">
                  <div class="dropdown-divider"></div><a href="logout.php" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                </div>
              </li>
            </ul> 
          </div>
        </div>
      </div>
    </nav>
    
    

        <!-- include ../../../includes/mixins-->

        <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">

          

          <li  class="nav-item"><a href="dashboard.php" class="nav-link"><span>Dashboard</span></a></li>
          <li  class="nav-item"><a href="certificate.php" class="nav-link"><span>New Certificate</span></a></li>
          <!-- <li  class="nav-item"><a href="renewal_certificate.php" class="nav-link"><span>Renewal Certificate</span></a></li> -->
            <!--<li  class="nav-item"><a href="vtd_certificate.php" class="nav-link"><i class="ft-home"></i><span>VTD Certificate</span></a></li>-->
          <li  class="nav-item"><a href="view_certificate.php" class="nav-link"><span>View Certificate</span></a></li>
          

          

        

        <!-- <li data-menu=""><a href="rto.php" data-toggle="dropdown" class="dropdown-item"><i class="fa fa-building-o"></i>RTO</a></li>  -->     

            </ul>

          </li>

  

        </ul>

      </div>

      <!-- /horizontal menu content-->

    </div>

    <!-- Horizontal navigation-->



    <div class="app-content content container-fluid">

      <div class="content-wrapper">