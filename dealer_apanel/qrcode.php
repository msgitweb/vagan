<?php
//set it to writable location, a place for temp generated PNG files
    //$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrimage'.DIRECTORY_SEPARATOR;
    $PNG_TEMP_DIR='../qrimage/';
    //html PNG location prefix
    $PNG_WEB_DIR = '../qrimage/';

    include "../phpqrcode/qrlib.php";    
    
    //ofcourse we need rights to create temp dir
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    
    
    $filename = $PNG_TEMP_DIR.'test.png';
    
    //processing form input
    //remember to sanitize user input in real-life solution !!!
    $errorCorrectionLevel ='L';    
    $matrixPointSize = 5;
	 // user data
    $qimage=$token.md5($qrcode_link.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
	$filename = $PNG_TEMP_DIR.$token.md5($qrcode_link.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
	QRcode::png($qrcode_link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
?>