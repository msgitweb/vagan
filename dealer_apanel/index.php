<?php
ob_start();
// session_start();
include ("../dbconfig.php");
if(isset($_SESSION['tech360_admin_user'])){
	header("location:dashboard.php");
}
if(isset($_REQUEST['login_sub']))
{
	extract($_REQUEST);
	$user=$db->real_escape_string($username);
	$pwd=$db->real_escape_string($password);

	$res=$db->query("select * from a_dealer where md5(name)='".md5($user)."' and md5(pwd)='".md5($pwd)."'  ");
	$row=$res->fetch_assoc();
	$count=$res->num_rows;
	if($count>0)
	{
     $_SESSION["dealer_address"] = $row['address'];
     $_SESSION["dealer_name"] = $row['name'];
     $_SESSION["dealer_id"] = $row['dealer_id'];
     // $_SESSION["dealer_email"] = $row['dealer_id'];

   	if(isset($remember)){
		  setcookie('univ_username',$username,time()+365*24*60*60);
		  setcookie('univ_password',$password,time()+365*24*60*60);
		}

		$_SESSION['dealer_id']=$row['dealer_id'];
		header("location:dashboard.php");
	}else{
		$msg=1;
	}
	
}

?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Login Page - Vagansafety </title>
  <?php echo'<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/feather/style.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/custom.css">
  <!-- END VENDOR CSS-->
  
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
  
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/login-register.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
  <!-- END Custom CSS-->';?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page" style="background-image:url(http://vagansafety.org/apanel/img/cbgs.jpg)">
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content container-fluid" style="background:none !important">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-md-4 offset-md-7 col-xs-10 offset-xs-1  box-shadow-2 p-0" style="width:300px;">
            <div class="card border-grey border-lighten-3 m-0">
              <div class="card-header no-border">
                <div class="card-title text-xs-center">
                  <!--<div class="p-0">-->
                <h4 style="font-weight: bold;">TSVS union</h4>
                  <!--   <div class="">-->
                  <!--       Vagansafety PVT LTD-->
                  <!--  <img  src="img/10893.png" alt="Amma Enterprises" width="150px" height="150px">-->
                  <!--</div>-->
                  <!--</div>-->
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                  <span>Please Login Here</span>
                </h6>
                <?php if(@$msg==1){  ?>
                <div class="alert bg-danger alert-icon-left alert-dismissible fade in mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>Oh snap!</strong> Sorry Wrong Details
                </div>
                <?php } ?>
              </div>
              <div class="card-body collapse in">
                <div class="card-block" style="padding-top:0px !important;">
                  <form class="form-horizontal form-simple" novalidate method="post">
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" class="form-control form-control-lg input-lg" name="username" id="user-name" placeholder="Your Username"
                      required value="<?php echo @$_COOKIE['univ_username'] ?>" />
                      <div class="form-control-position">
                        <i class="ft-user"></i>
                      </div>
                    </fieldset>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="password" class="form-control form-control-lg input-lg" name="password" id="user-password"
                      placeholder="Enter Password" required value="<?php echo @$_COOKIE['univ_password'] ?>" />
                      <div class="form-control-position">
                        <i class="fa fa-key"></i>
                      </div>
                    </fieldset>
                    <!--<fieldset class="form-group row">-->
                    <!--  <div class="col-md-6 col-xs-12 text-xs-center text-md-left">-->
                    <!--    <fieldset>-->
                    <!--      <input type="checkbox" id="remember-me" name="remember" class="chk-remember">-->
                    <!--      <label for="remember-me"> Remember Me</label>-->
                    <!--    </fieldset>-->
                    <!--  </div>-->
                    <!--  <div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a href="recover_password.php" class="card-link">Forgot Password?</a></div>-->
                    <!--</fieldset>-->
                    <button type="submit" name="login_sub" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                  </form>
                </div>
              </div>
              <div class="card-footer">
                
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <!-- BEGIN VENDOR JS-->
  <script src="../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <script src="../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"
  type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  
  <script src="../app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="../app-assets/js/core/app.min.js" type="text/javascript"></script>

  <!-- BEGIN PAGE LEVEL JS-->
  <script src="../app-assets/js/scripts/forms/form-login-register.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>

<style>
.has-icon-left .form-control.input-lg{border-radius:20px !important; background:#f5f2f1;}
    .card, .card-footer, .card-header{border-radius:10px !important;}
    .btn-primary { border-radius:20px !important;
    border-color: #552f84 !important;
    background-color:#552f84 !important;
    color: #FFF;
}

input.form-control.input-lg, select.form-control:not([size]):not([multiple]).input-lg{font-size:1.0rem !important;}
</style>