<?php
include"../dbconfig.php";
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$vehicle_id=$db->real_escape_string($vddid);
$token=$db->real_escape_string($token);
if( !is_numeric($vehicle_id) || $token=='' || $vehicle_id==''  ){ header("location:error_400.php"); }
$vehicle_res=$db->query(" SELECT * FROM `techs_vehicle` where vehicle_id='$vehicle_id' and token='$token' ");
if( $vehicle_res->num_rows==0 ){ header("location:error_400.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
$res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");
$row01=$res01->fetch_assoc();
$config_res=$db->query("select * from `techs_profile` where id='1' ");
$config_row=$config_res->fetch_assoc();
$rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$vehicle_row['speed_id']."' ");
$rto_row01=$rto_res01->fetch_assoc();
if( $vehicle_row['certificate']=='Fitment' ){
    $certi='img/header-bg-1.jpg';
}else{
    $certi='img/header-bg.jpg';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Techo Service</title>
</head>

<body style=" width:100%; margin:0 auto;font-family:'Arial', Gadget, sans-serif;">
<div class="main" style="background:url(img/bg.png) no-repeat scroll top center; width:990px; height:1380px; margin:0 auto;">
	<div class="header">
        <p style=" font-family: arial;
    font-size: 14px;
    font-weight: 400;
    padding-top: 120px;
    text-align: center;"> This Speed limiting device has been critically sealed 
at 5 points as per AIS 018 </p>
		<img src="<?=$certi?>" style=" margin-left: 115px;
   /* margin-top: 110px;*/
    text-align: center;
    width: 760px;" />	
	</div>
    
    <div class="content" style="width:700px; margin:0 auto;">
    	<div class="si-no" style="float:right; width:300px; margin-bottom:10px; margin-top:10px;">
        	<div class="si-no-left" style="float:left; width:100px;">  SI.NO : </div>
            <div class="si-no-right"  style="float:left; width:200px;border-bottom:2px dotted #000; text-align:center;"> <span style="">
            <?=$vehicle_row['sl_no']?> </span></div>
        	
        </div>
         <div class="clr" style="clear:both;"></div>
    	<div class="content-first" style="border:1px solid #ccc;">
        	<div class="content-first-left" style=" background:#eeeeee; float:left; width:300px; padding:10px;">
            	<h2 style="text-transform:uppercase; font-size:18px; text-align:center; font-family:'Arial', Gadget, sans-serif;font-weight: 400"> To Regional Transport <br /> Offices </h2>
                <h5 style=" border-bottom:2px dotted #000; font-size: 13px; font-family: "Arial Black",Gadget,sans-serif;"><?=$rto_row01['code'].' '.$rto_row01['area']?></h5>
               
                
            </div>
            <div class="boder" style="float: left;width: 50px;"><img style="height: 133px;" src="img/boder.jpg"></div>

            <div class="content-first-right" style="float:right; width:300px; padding:10px;font-weight: 600;">
            	<div class="first-left-left" style="text-transform:uppercase; float:left; width:100px; font-size: 15px;">Dealer: </div>

                <div class="first-left-right" style="text-transform:uppercase; float:right; width:240px; margin-top:-23px;">
                		<h4 style="font-weight:bold; border-bottom:2px dotted #000; text-align:right;margin-top: 12px;margin-bottom: 15px;"><?=$vehicle_row['dealer_name']?></h4>
                        <h5 style=" border-bottom:2px dotted #000; font-weight:400; text-align:right;margin-top: 15px;margin-bottom: 15px;font-weight:600; font-size: 12px"><?=$vehicle_row['dealer_address']?></h5>
                        <h5 style=" border-bottom:2px dotted #000; text-align:right;margin-top: 15px;margin-bottom: 15px;font-weight:600;"><?=$vehicle_row['dealer_mobile']?></h5>
                </div>
                <div class="clr" style="clear:both;"></div>
            </div>
            <div class="clr" style="clear:both;"></div>
        </div>
        
        <div class="content-second">
        	<div class="content-first-left" style="float:left; width:320px; padding:10px; margin-top:60px; 
            border-right:3px solid #999;">

            	<div class="first-left-left" style="text-transform:uppercase; font-weight:normal; font-size:15px; float:left; ">
                	<p style="width:100px; line-height: 30px;">Chasis No : </p>

                    <p style="width:100px; "> Engine No: </p>
				 <p> Vehicle Make: </p>
				<p>Set Speed: </p>
                 </div>
                <div class="first-left-right" style="text-transform:uppercase; font-size:19px; float:right;  margin-top:-23px; width:200px;">

        <h6 style=" border-bottom:2px dotted #000; padding:0 0 5px 0; font-weight: bold;text-align:right;  "><?=$vehicle_row['chiss_no']?></h6>

        <h6 style=" border-bottom:2px dotted #000; margin-top:-10px; font-weight: bold; padding:0;  text-align:right; width:210px;"><?=$vehicle_row['engine_no']?></h6>
        <h6 style=" border-bottom:2px dotted #000; margin-top:-10px; padding:0; text-align:right;"><?=$vehicle_row['vehicle_make']?></h6>
        <h6 style=" border-bottom:2px dotted #000; margin-top:-10px; padding:0; text-align:right;"><?=$vehicle_row['speed']?></h6>

                </div>
                <div class="clr" style="clear:both;"></div>
               
                
            </div>
            <div class="content-first-right" style="float:right; width:320px; padding:0 10px;font-weight: normal;">
            <p style="float:right;">DATE : <?=date('d-m-Y',strtotime($vehicle_row['go_date']))?></p>
            <div class="clr" style="clear:both;"></div>
            	<div class="first-left-left" style="text-transform:uppercase; font-weight:normal; font-size:14px; float:left; 
                width:130px;">
                <p style="margin-top: 20px;margin-bottom: 10px;">Vehicle No : </p>

                <p style="margin-top: 20px;margin-bottom: 10px;"> sld serial No: </p>
				<p style="margin-top: 20px;margin-bottom: 10px;"> Vehicle model: </p>
				<p style="margin-top: 20px;margin-bottom: 10px;">Fitted Date: </p>
                <p style="margin-top: 20px;margin-bottom: 10px;">Renew Due Date: </p>
                 </div>
                <div class="first-left-right" style="text-transform:uppercase; font-size:20px; float:right; width:190px; margin-top:-23px;">
                		
    <h6 style=" border-bottom:2px dotted #999; padding:0 0 5px 0; text-align:right; font-weight: bold;"><?=$vehicle_row['vehicle_no']?></h6>
    <h6 style=" border-bottom:2px dotted #999; margin-top:-10px;  padding:0;  text-align:right;"><?=$vehicle_row['sl_no']?></h6>
    <h6 style=" border-bottom:2px dotted #999; margin-top:-10px; padding:0; text-align:right;"><?=$vehicle_row['vehicle_model']?></h6>
    <h6 style=" border-bottom:2px dotted #999; margin-top:-10px; padding:0; text-align:right;"><?=date('d-m-Y',strtotime($vehicle_row['go_date']))?></h6>
    <h6 style=" border-bottom:2px dotted #999; margin-top:-10px; padding:0; text-align:right;"><?=date('d-m-Y',strtotime($vehicle_row['renewal_date']))?></h6>
                </div>
                <div class="clr" style="clear:both;"></div>
            </div>
            <div class="clr" style="clear:both;"></div>
        </div>
        


 <div class="clr" style="clear:both;"></div>
 
        <div class="content-third">
        	<div class="cnt-lft" style="float:left; width:220px;">
            	<img src="../document/<?=$vehicle_row['governor_photo'] ?>" style="width:150px; height:155px;" />
            </div>
            <div class="cnt-center" style="float:left; width:220px;">
            <img src="../document/<?=$vehicle_row['vehicle_photo'] ?>" style="width:150px; height:155px;" />
            </div>
            <div class="cnt-right" style="float:right; width:220px;">
            	<img src="../qrimage/<?=$vehicle_row['qrimage'] ?>" style="width:150px; height:155px; margin-left: 34px" />
                <img src="img/4.jpg" />
            </div>
            <div class="clr" style="clear:both;"></div>
        </div>
        
        <div class="clr" style="clear:both;"></div>
        
        <div class="cnt-four">
        	<div class="four-left" style="width:120px; font-size:11px; float:left; text-transform:uppercase;">
            	<h3>Vehicle owner: </h3>
            </div>
            <div class="four-right" style="width:580px; float:right;border-bottom:2px dotted #999; font-size:14px;font-weight: normal; ">
            	<p style="text-align:right; text-transform:uppercase; font-weight: normal; margin:12px 0 0; padding:0px;"><?=$vehicle_row['vehicle_address'] ?></p>
            </div>
            <div class="clr" style="clear:both;"></div>
        </div>
        <div class="clr" style="clear:both;"></div>
        
        <div class="cnt-four">
        	<div class="four-left" style="width:250px; font-size:11px; font-weight: normal; float:left; text-transform:uppercase;">
            	<h3>Vehicle owner contact number:</h3>
            </div>
            <div class="four-right" style="width:450px; font-size:14px; float:right;border-bottom:2px dotted #999; ">
            	<p style="text-align:center;  margin:12px 0 0; font-weight: bold; text-transform:uppercase;"><?=$vehicle_row['mobile_num01'] ?></p>
            </div>
            <div class="clr" style="clear:both;"></div>
        </div>
        
        <div class="clr" style="clear:both;"></div>
        <div class="btn" style="background:#800000; text-align:center; padding:10px; border-radius:5px; color:#fff;
          width:300px; margin:0 auto; margin-top:20px; margin-bottom:20px; text-transform:uppercase;">Valid upto 1 year</div>
          <div class="clr" style="clear:both;"></div>
          
          <div class="sign" style="text-transform:uppercase; padding-top:55px;">
          <p>Dealer signature & seal</p>
          </div>
    </div>
</div>
</body>
</html>
