<?php $page_title="Vehicle Details"; include("header.php");

$cdate=date('Y-m-d H:i:s');

extract($_REQUEST);

$d_dealer_certificate_id=$db->real_escape_string($d_dealer_certificate_id);

if( !is_numeric($d_dealer_certificate_id) ){ header("location:dashboard.php"); }

$vehicle_res=$db->query(" SELECT * FROM `d_dealer_certificate` where d_dealer_certificate_id='$d_dealer_certificate_id' ");

if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }

$vehicle_row=$vehicle_res->fetch_assoc();

// $res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");

// $row01=$res01->fetch_assoc();



$rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$vehicle_row['rto_id']."' ");

$rto_row01=$rto_res01->fetch_assoc();

?>

<style type="text/css">

  .tab1 tr:nth-child(even){ background: #f2f2f2; }

  .tab1 td:hover{ background: #2f93f7;font-weight: bold;color:#fff  }

</style>

<div class="content-body">

<div class="row">

        

          <div class="col-md-12">

              <div class="card">

                <div class="card-body collapse in">

                  <div class="bg-lighten-1 height-50">

                        <div class="card-header" style="background:  #575757; height: 50px">

                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">Vehicle Details</h4> 

                          <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>

                          <div class="heading-elements">

                              <ul class="list-inline mb-0">

                                <li><a href="view_vehicle.php" title="View List"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>

                                <li><a href="print_details.php?vddid=<?=$vehicle_id;?>&token=<?=$vehicle_row['token'] ?>" target="_blank" title="Print"><i class="fa fa-print fa-lg text-white"></i></a></li>

                              </ul>

                          </div>                

                        </div>

                       </div>

                  <div class="card-block">

                    

                      <table class="table tab1">

                          <tr>

                              <th width="20%">RTO</th>

                              <td><?=$rto_row01['code'].' '.$rto_row01['area']?></td>

                              <th width="20%">DATE</th>

                              <td><?=date('d-M Y',strtotime($vehicle_row['created_on']));?></td>

                          </tr>

                          <tr>

                              <th>VEHICLE NO</th>

                              <td><?=$vehicle_row['vehicle_no']?></td>

                              <th>SPEED GOVERNOR MAKE</th>

                              <td>Convertz <!-- <?=$row01['title']?> --></td>

                          </tr>

                          <tr>

                              <th>CHISS NO</th>

                              <td><?=$vehicle_row['chiss_no']?></td>

                              <th>SL.NO</th>

                              <td><?=$vehicle_row['sl_no']?></td>

                          </tr>

                          <tr>

                              <th>ENGINE NO</th>

                              <td><?=$vehicle_row['engine_no']?></td>

                              <th>REF.NO</th>

                              <td><?=$vehicle_row['ref_no']?></td>

                          </tr>

                          <tr>

                              <th>VEHICLE MAKE</th>

                              <td><?=$vehicle_row['vehicle_make']?></td>

                              <th>INVOICE NO</th>

                              <td><?=$vehicle_row['invoice_no']?></td>

                          </tr>

                          <tr>

                              <th>VEHICLE MODEL</th>

                              <td><?=$vehicle_row['vehicle_model']?></td>

                              <th>SET SPEED</th>

                              <td><?=$vehicle_row['speed']?></td>

                          </tr>

                          <tr>

                              <th>VEHICLE OWNER NAME & ADDRESS</th>

                              <td><?=$vehicle_row['vehicle_address']?></td>

                              <th>DEALER NAME & ADDRESS</th>

                              <td><?=$vehicle_row['dealer_name'].', '.$vehicle_row['dealer_address'].', '.$vehicle_row['dealer_mobile']?></td>

                          </tr>

                          <tr>

                              <th>OWNER PHONE NO</th>

                              <td><?=$vehicle_row['mobile_num01']?></td>

                              <th>ELECTRONIC SPEED GOVERNER FITTED DATE</th>

                              <td><?=date('d-M Y',strtotime($vehicle_row['go_date']));?></td>

                          </tr>

                          <tr>

                              <th>ELECTRONIC SPEED GOVERNER SERIAL NO</th>

                              <td><?=$vehicle_row['sl_no']?></td>

                              <th>ELECTRONIC SPEED GOVERNER RENEWAL DATE</th>

                              <td><?=date('d-M Y',strtotime($vehicle_row['renewal_date']));?></td>

                          </tr>

                      </table>



                      <table class="table">

                          <tr>

                              <td><img src="../document/<?=$vehicle_row['governor_photo']?>" class="img-thumbnail" 

                                style="width: 300px; height: 200px" ></td>

                              <td><img src="../document/<?=$vehicle_row['vehicle_photo']?>" class="img-thumbnail"

                              style="width: 300px; height: 200px" ></td>

                          </tr>

                          <tr>

                              <td><img src="../qrimage/<?=$vehicle_row['qrimage']?>" class="img-thumbnail" ></td>

                              <td></td>

                          </tr>

                      </table>



                  </div>

                </div>

              </div>

            </div>

            

                

    </div>





</div>



<?php include("footer.php") ?>