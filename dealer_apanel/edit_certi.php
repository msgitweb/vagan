<?php $page_title="Update Certificate Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['up']==1){
    $result=success_alert('Data updated successfully !');
}
extract($_REQUEST);
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  $data=array();
  foreach ($_POST as $key => $value) {
    if( !empty($value) ){ $data[$key]=@$db->real_escape_string($value); }
  }
   
    $up=$db->query(" update `techs_certificate` set speed_id='".$data['speed_id']."',customer_name='".$data['customer_name']."',address='".$data['address']."',mobile_number='".$data['mobile_number']."',certificate='".$data['certificate']."',test_report_num='".$data['test_report_num']."',serial_num='".$data['serial_num']."',vehicle_make='".$data['vehicle_make']."',vehicle_model='".$data['vehicle_model']."',reg_num='".$data['reg_num']."',chassis_num='".$data['chassis_num']."',engine_num='".$data['engine_num']."',model_num='".$data['model_num']."',speed='".$data['speed']."',invoice_num='".$data['invoice_num']."',dte='".$dte."',
      year_manufacture='".$data['year_manufacture']."',date_register='".$data['date_register']."',cop_num='".$data['cop_num']."',
      tpe='".$data['tpe']."',dealer_name='".$data['dealer_name']."',dealer_address='".$data['dealer_address']."',
      dealer_mobile='".$data['dealer_mobile']."',updated_on='$cdate',seal_no='".$data['seal_no']."',
      vehicle_num='".$data['vehicle_num']."',copvalid='$copvalid' where certificate_id='$certificate_id' "); 
     
    if( $up ){ 
      unlink("../qrimage/".$qimg);
      $res01=$db->query(" SELECT b.area FROM `techs_speed_governor` a left join `techs_rto` b on a.rto_id=b.rto_id where a.id='$speed_id' ");
      $row01=$res01->fetch_assoc();
      $qrcode_link='Mfg:Mercyda z, '.'Instl:'.substr($dealer_name, 0,6).'...'.'Sr.No:'.$serial_num.', Fit Dt:'.date('d/m/Y',strtotime($dte)).', Ren. Dt:'.date('d/m/Y',strtotime($renewal_date)).', '.
      'Own.Name:'.$customer_name.', Ph:'.$mobile_number.', Veh.No:'.$vehicle_num.', Eng.No:'.$engine_num.', Ch.No:'.$chassis_num.', Seal:'.$seal_no.', RTO:'.$row01['area'].'Tac No:AA1343, '.'Veh.Make:'.$vehicle_make.', Veh. Model:'.$vehicle_model.', Speed:'.$speed.', Test Report No:'.$test_report_num.', Type:'.$tpe;
      include"qrcode.php";
      $db->query(" update techs_certificate set qrimage='$qimage' where certificate_id='$certificate_id' ");
      header("location:?up=1&certificate_id=".$certificate_id);  

    }else{ $result=error_alert('Have some error...'); }


}

$certificate_id=$db->real_escape_string($certificate_id);
if( !is_numeric($certificate_id) ){ header("location:dashboard.php"); }
$vehicle_res=$db->query(" SELECT * FROM `techs_certificate` where certificate_id='$certificate_id' ");
if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-warning bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">Update Certificate Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="view_certificate.php" title="View List"><i class="fa fa-arrow-right fa-lg text-white"></i></a></li>
                              </ul>
                          </div>                
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body">

                        <div class="row">
                          <div class="col-md-8"></div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control square" placeholder="Invoice Number" name="invoice_num" 
                              style="border-top: none;border-left: none;border-right: none;" value="<?=$vehicle_row['invoice_num'] ?>" />
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <input type="text" class="form-control square date" placeholder="Date" name="dte"
                              style="border-top: none;border-left: none;border-right: none;" value="<?=$vehicle_row['dte'] ?>"  />
                            </div>
                          </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                  <label for="donationinput1">RTO <span class="manfield">*</span></label>
                                  <select name="speed_id" class="select2" style="width: 100%">
                                    <option value="">Please select the RTO</option>
                                    <?php
                                      $speed_res=$db->query(" SELECT * FROM `techs_rto` where state_id=29  ");
                                      while( $speed_row=$speed_res->fetch_assoc() ){
                                    ?>
                                      <option value="<?=$speed_row['rto_id'];?>" <?php if($speed_row['rto_id']==$vehicle_row['speed_id']){ echo 'selected="selected"'; } ?> ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                                    <?php } ?>
                                  </select>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                  <label for="donationinput1">Test Report No <span class="manfield">*</span></label>
                                  <input type="text" class="form-control square" placeholder="Test Report No" name="test_report_num"
                                  value="<?=$vehicle_row['test_report_num'] ?>"  />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                  <label for="donationinput1">Seal No <span class="manfield">*</span></label>
                                  <input type="text" class="form-control square" placeholder="Seal No" name="seal_no"
                                  value="<?=$vehicle_row['seal_no'] ?>"  />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Customer Name <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Customer Name" name="customer_name"
                            value="<?=$vehicle_row['customer_name'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Mobile Number <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Mobile Number" name="mobile_number"
                            value="<?=$vehicle_row['mobile_number'] ?>"  />
                          </div>
                        </div>
                      </div>

                        <div class="form-group">
                        <label for="donationinput1">Address <span class="manfield">*</span></label>
                        <textarea class="form-control square" placeholder="Address" name="address" ><?=$vehicle_row['address'] ?></textarea>
                      </div>
                      
                        
                       
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Certificate <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="certificate">
                                <option value="">Please select Certificate</option>
                                <option value="Fitment" <?php if( $vehicle_row['certificate']=='Fitment' ){ echo "selected='selected'"; } ?> >Fitment</option>
                                <option value="Re-calibration" <?php if( $vehicle_row['certificate']=='Re-calibration' ){ echo "selected='selected'"; } ?> >Re-calibration</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Serial Number <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Serial Number" name="serial_num"
                            value="<?=$vehicle_row['serial_num'] ?>" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Make" name="vehicle_make"
                            value="<?=$vehicle_row['vehicle_make'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Model <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Model" name="vehicle_model" 
                            value="<?=$vehicle_row['vehicle_model'] ?>"  />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_num" 
                            value="<?=$vehicle_row['vehicle_num'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Register No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Register No" name="reg_num"
                            value="<?=$vehicle_row['reg_num'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chassis_num"
                            value="<?=$vehicle_row['chassis_num'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Engine No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No" name="engine_num"
                            value="<?=$vehicle_row['engine_num'] ?>"  />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Model No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Model No" name="model_num"
                            value="<?=$vehicle_row['model_num'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Speed <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="speed">
                              <option value="">Please select the speed</option>
                              <option value="40" <?php if($vehicle_row['speed']=='40'){ echo 'selected="selected"'; } ?>>40</option>
                              <option value="45" <?php if($vehicle_row['speed']=='45'){ echo 'selected="selected"'; } ?>>45</option>
                              <option value="50" <?php if($vehicle_row['speed']=='50'){ echo 'selected="selected"'; } ?>>50</option>
                              <option value="60" <?php if($vehicle_row['speed']=='60'){ echo 'selected="selected"'; } ?>>60</option>
                              <option value="80" <?php if($vehicle_row['speed']=='80'){ echo 'selected="selected"'; } ?>>80</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle year of manufacturing <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle year of manufacturing" name="year_manufacture" 
                            value="<?=$vehicle_row['year_manufacture'] ?>" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Date of Registration<span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder="Vehicle Date of Registration" name="date_register"
                            value="<?=$vehicle_row['date_register'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">COP No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="COP No" name="cop_num"
                            value="<?=$vehicle_row['cop_num'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Valid up to <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder="Valid up to" name="copvalid" 
                            value="<?=$vehicle_row['copvalid'] ?>" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="donationinput1">Type <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Type" name="tpe"
                            value="<?=$vehicle_row['tpe'] ?>"  />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Name <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Dealer Name" name="dealer_name"
                            value="<?=$vehicle_row['dealer_name'] ?>"  />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Mobile No</label>
                            <input type="text" class="form-control square" placeholder="Dealer Mobile No" name="dealer_mobile" 
                            value="<?=$vehicle_row['dealer_mobile'] ?>"  />
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="donationinput1">Dealer Address <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Dealer Address" name="dealer_address" ><?=$vehicle_row['dealer_address'] ?></textarea>
                      </div>

        
                        <input type="hidden" name="certificate_id" value="<?=$certificate_id?>">
                        <input type="hidden" name="qimg" value="<?=$vehicle_row['qrimage']?>">
                        <button type="submit" name="submit" class="btn btn-primary pull-right">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                speed_id:{ required: true, }, 
                customer_name :{  required: true, },
                address : { required: true,  },  
                mobile_number:{ required: true, },
                certificate:{ required: true, },
                test_report_num:{ required: true, },
                serial_num:{ required: true, },
                vehicle_make:{ required: true, },
                vehicle_model:{ required: true,},
                reg_num:{ required: true },
                chassis_num:{ required: true },
                engine_num:{ required: true },
                model_num:{ required: true },
                speed:{ required: true },
                invoice_num:{ required: true },
                dte:{ required: true },
                year_manufacture:{ required: true },
                date_register:{ required: true },
                cop_num:{ required: true },
                tpe:{ required: true },
                dealer_name:{ required: true },
                dealer_address:{ required: true },
                vehicle_num:{ required: true },
            },
            messages:{
                  speed_id:{ required: "Please Select The Speed Governor", }, 
                  customer_name :{  required: "Please Enter Customer Name", },
                  address : { required: "Please Enter The Address",  },  
                  mobile_number:{ required: "Please Enter Mobile Number", },
                  certificate:{ required: "Please Select The Certificate", },
                  test_report_num:{ required: "Please Enter Test Report No", },
                  serial_num:{ required: "Please Enter Serial No", },
                  vehicle_make:{ required: "Please Enter Vehicle Make", },
                  vehicle_model:{ required: "Please Enter Vehicle Model",},
                  reg_num:{ required: "Please Enter Register Number" },
                  chassis_num:{ required: "Please Enter Chassis No" },
                  engine_num:{ required: "Please Enter Engine No" },
                  model_num:{ required: "Please Enter Model No" },
                  speed:{ required: "Please Select The Speed" },
                  invoice_num:{ required: "Please Enter Invoice No" },
                  dte:{ required: "Please Enter The Date" },
                  year_manufacture:{ required: "Please Enter Vehicle Year of manufacturing" },
                  date_register:{ required: "Please Enter The Date" },
                  cop_num:{ required: "Please Enter COP No" },
                  tpe:{ required: "Please Enter The Type" },
                  dealer_name:{ required: "Please Enter The Dealer Name" },
                  dealer_address:{ required: "Please Enter Dealer Address" },
                  vehicle_num:{ required: "Please Enter Vehicle No" },
            } ,
    });
  });
</script>