<?php $page_title="New Certificate Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);

$result='';

if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  echo "insert into `d_dealer_certificate` set dealer_id='".$_SESSION['dealer_id']."',rto='$rto',date='$date',vehicle_no='$vehicle_no',renewal_date='$renewal_date',vdate='$vdate',chassis_no='$chassis_no',sld_make='$sld_make',engine_no='$engine_no',sld_no='$sld_no',make_of_vehicle='$make_of_vehicle',tac_no='$tac_no',model_of_vehicle='$model_of_vehicle',speed='$speed',owner_name='$owner_name',cop='$cop',vehicle_photo='".$url2."',contact='$contact',owner_address='$owner_name',dealer_address='$dealer_address',created_on='$cdate',token='$token'";

  // $data=array();
  // foreach ($_POST as $key => $value) {
  //   if( !empty($value) ){ $data[$key]=@$db->real_escape_string($value); }
  // }
  
  $image_error=0;


  $filename=$_FILES['vehicle_photo']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url2='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url2='vehicle_photo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['vehicle_photo']['tmp_name'],"../document/".$url2);
  }else{
    ++$image_error;
  }
  if( $image_error==0 ){
    // $renewal_date=date('Y-m-d',strtotime("+1 Year"));
    // $token=md5($data['vehicle_no'].date('Y-m-d'));
  
   $account_res=$db->query("select * from a_dealer_credit_account where dealer_id='".$_SESSION['dealer_id']."'");
   $account_row=$account_res->fetch_assoc();
   $token=md5($vehicle_no.date('Y-m-d'));
   $renewal_date=date('Y-m-d', strtotime($date. ' + 365 day'));
   
   if($account_row['credit_balance'] > 0){
    
    $up=$db->query(" insert into `d_dealer_certificate` set dealer_id='".$_SESSION['dealer_id']."',rto='$rto',date='$date',vehicle_no='$vehicle_no',renewal_date='$renewal_date',vdate='$vdate',chassis_no='$chassis_no',sld_make='$sld_make',engine_no='$engine_no',sld_no='$sld_no',make_of_vehicle='$make_of_vehicle',tac_no='$tac_no',model_of_vehicle='$model_of_vehicle',speed='$speed',owner_name='$owner_name',cop='$cop',vehicle_photo='".$url2."',contact='$contact',owner_address='$owner_name',dealer_address='$dealer_address',created_on='$cdate',token='$token'  ");
    

  
 

  if($up){
    $d_dealer_certificate_id=$db->insert_id;
   $up1=$db->query("update a_dealer_credit_account set used_credit='".$account_row['used_credit']."'+1,credit_balance='".$account_row['credit_balance']."'-1 where dealer_id='".$_SESSION['dealer_id']."'");

//   $qrcode_link='RTO:'.$rto.', Vehicle No:'.$vehicle_no.', Vehicle Make:'.$make_of_vehicle.', SLD No:'.$sld_no.', SLD Make:'.$sld_make.', Validity:'.$vdate;
//       include"qrcode.php";
//       $db->query(" update d_dealer_certificate set qr_code='".$url1."' where d_dealer_certificate_id='$d_dealer_certificate_id' ");


    $result=success_alert('Certificate added successfully'); 
   
  }  else { $result=error_alert('Have Some error'); }
      
     } else { $result=error_alert('Please Renew your credit balance'); }
   
}
}
?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-blue bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Certificate Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="dashboard.php" title="View List"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                              </ul>
                          </div>                
                        </div>
                       </div>
                       
                  <div class="card-block">
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body" style="margin-left:200px">
                        <div class="row">
                        <div class="form-group col-md-4">
                          <label for="donationinput1">RTO<span class="manfield">*</span></label>
                          <select name="rto" class="select2" style="width: 100%">
                            <option value="">Please select the RTO</option>
                            <?php
                              $speed_res=$db->query(" SELECT * FROM `techs_rto`");
                              while( $speed_row=$speed_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$speed_row['rto_id'];?>" ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                       <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder=" Date" name="date"
                             />
                          </div>
                        </div>
 
                      </div>
                      <div class="row">
                       
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_no" />
                          </div>
                        </div>

                           <div class="form-group col-md-4">
                            <label for="donationinput1">Validity<span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder=" Validity" name="vdate"
                             />
                          </div>

                      
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chassis_no"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SLD Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="SLD Make " name="sld_make" 
                              />
                          </div>
                        </div>
                       
                      </div>
                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">ENGINE NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No " name="engine_no"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SLD NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="SLD No " name="sld_no" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MAKE OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MAKE OF VEHICLE " name="make_of_vehicle"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">TAC NO </label>
                            <input type="text" class="form-control square" placeholder="TAC No " name="tac_no" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MODEL OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MODEL OF VEHICLE " name="model_of_vehicle"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SPEED</label>
                            <input type="text" class="form-control square" placeholder="SPEED " name="speed" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">OWNER NAME<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="OWNER NAME " name="owner_name"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">COP</label>
                            <input type="text" class="form-control square" placeholder="COP " name="cop" 
                              />
                          </div>
                        </div>
                      </div>
                       <div  class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <label for="donationinput1">CONTACT NO<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="CONTACT NO " name="contact"    />
                          </div>
                        </div>
                      </div>
                       
                      <div class="form-group">
                        <label for="donationinput1">OWNER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Owner Address" name="owner_address" ></textarea>
                      </div>

                       <div class="form-group">
                        <label for="donationinput1">DEALER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="DEALER Address" name="dealer_address" ></textarea>
                      </div>             


                    
                       <div class="row">
                        <!-- <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">QR CODE<span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="qr_code"  />
                          </div>
                        </div> -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">VEHICLE PHOTO </label>
                            <input type="file" class="form-control square" name="vehicle_photo"  />
                          </div>
                        </div>
                      </div>
                        


                        <button type="submit" name="submit" class="btn btn-primary pull-right">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>



<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<style>
</style>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                rto:{ required: true, }, 
                date:{ required: true, },
                vehicle_no :{  required: true, },
                vdate:{ required: true,},
                chassis_no : { required: true,  }, 
                sld_make:{ required: true }, 
                engine_no:{ required: true, },
                sld_no:{ required: true },
                make_of_vehicle:{ required: true, },
               
                model_of_vehicle:{ required: true, },
                owner_name:{ required: true },
                contact:{ required: true },
                owner_address:{ required: true },
                dealer_address:{ required: true, },
               
                               
                
            },
            messages:{
                 rto : { required: "Please Select Speed Governor", }, 
                 date : { required: "Please Fill the Dtae",}, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                 vdate : { required: "Please Fill the Validy Date",},
                 chassis_no : { required: "Please enter Chiss No", }, 
                 sld_make : { required: "Please Enter SLD Make",},
                 engine_no : { required: "Please Enter Engine No",},
                 sld_no : { required: "Please Enter SLD No",}, 
                 make_of_vehicle : { required: "Please Enter Vehicle Make",},
                
                 model_of_vehicle : { required: "Please Enter Vehicle Model",},
                 owner_name : { required: "Please Enter Owner Name",}, 
                 contact:{ required: "Please enter  mobile no" },
                 owner_address:{ required: "Please enter owner address" },
                 dealer_address : { required: "Please Enter Vehicle Owner  Address",}, 
                 
                 
                
                 
            } ,
    });
  });
</script>