<?php $page_title="Fitment Renewal Certificate Details"; include("header.php");
date_default_timezone_set('Asia/Kolkata');
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$print_trigger=0;
if(isset($_REQUEST['submit'])){
  extract($_REQUEST); 
  
  if($payment_type=='1'){
    $logo = 'avery.PNG';   
    $company_name = 'AVERY DENNISON (INDIA) PVT LTD';
    $field_name = 'ICICD';
    $back = 'avery-bg.jpg';
    $cop='SHL/16/2019-2020/3000008304/COP/3099';
    $mark='SHL/16/2016-2017/9203/1186';
    $aptype='A95224 &E13 104R';
  }else{
    $logo = '3m.PNG';   
    $company_name = '3M INDIA LIMITED';
    $field_name = 'UID';
     $back = '3m-bg.jpg';
      $cop='SHL/16/2018-2019/3000002951/COP/2711';
      $mark='SHL/16/2013/-2014/9149/2783 /17.12.2014';
       $aptype='A94495';
  }
  $image_error=0; 
  $filename=$_FILES['front']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url2='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url2='vehicle_photo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['front']['tmp_name'],"../document/".$url2);
  }else{
    ++$image_error;
  } 
  
   $image_error=0;
  $filename=$_FILES['rear']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url1='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url1='governor_photo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['rear']['tmp_name'],"../document/".$url1);
  }else{
    ++$image_error;
  }
  
  $image_error=0;
  $filename=$_FILES['side1']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url3='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url3='sld_logo'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['side1']['tmp_name'],"../sld_logo/".$url3);
  }else{
    ++$image_error;
  }
  $image_error=0;
  $filename=$_FILES['side2']['name'];
  $ext=end((explode('.',$filename)));
  $ext=strtolower($ext);
  $url4='';
  if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
    $url4='side4'.date('Y-m-d').uniqid().'.'.$ext;
    move_uploaded_file($_FILES['side2']['tmp_name'],"../sld_logo/".$url4);
  }else{
    ++$image_error;
  }
  
//   if( $image_error==0 ){
 
  
   $account_res=$db->query("select * from a_dealer_credit_account where dealer_id='".$_SESSION['dealer_id']."'");
   $account_row=$account_res->fetch_assoc();
   $token=md5($vehicle_no.date('Y-m-d'));
   $renewal_date=date('Y-m-d', strtotime($date. ' + 365 day'));
  $certificate_number='CRF'.date('Y').rand(1000,99999);
   if($account_row['credit_balance'] > 0){

    $up=$db->query(" insert into `d_dealer_certificate_new` set dealer_id='".$_SESSION['dealer_id']."',rto='$rto',company_name='$company_name',date='$date',logo='$logo',vehicle_no='$vehicle_no',email='$email',chassis_no='$chassis_no',`year`='$year',`class4`='$class4',engine_no='$engine_no',red20='$red20',make_of_vehicle='$make_of_vehicle',red50='$red50',model_of_vehicle='$model_of_vehicle',white20='$white20',owner_name='$owner_name',white50='$white50',rear='".$url2."',contact='$contact',owner_address='$owner_address',yellow50='$yellow50',cir150='$cir150',cir250='$cir250',cir350='$cir350',class3='$class3',dealer_address='$dealer_address',created_on='$cdate',renewal_date='$renewal_date',vts_make='$vts_make',vts_no='$vts_no',type='1',front='".$url1."',side1='".$url3."',side2='".$url4."',invoice_number='$invoice_number',certificate_type='sld',certificate_number='$certificate_number',`back`='$back',`cop`='$cop',`mark`='$mark',`aptype`='$aptype'");
    
    $d_dealer_certificate_id=$db->insert_id;
    
   $rto_res=$db->query("select * from techs_rto where rto_id='".$_POST['rto']."'");
   
   $rto_row=$rto_res->fetch_assoc();

  if($up){
    
     $rto_res1=$db->query("select * from a_dealer where dealer_id='".$_SESSION['dealer_id']."'");
   $rto_row1=$rto_res1->fetch_assoc();
    
   $up1=$db->query("update a_dealer_credit_account set used_credit=used_credit+1,credit_balance=credit_balance-1 where dealer_id='".$_SESSION['dealer_id']."'");

   $qrcode_link='RTO:'.$rto_row['code'].'-'.$rto_row['area'].', Vehicle No:'.$vehicle_no.',Engine No:'.$engine_no.',Chassis No:'.$chassis_no.',Vehicle Make:'.$make_of_vehicle.', Year:'.$year.', Model:'.$model_of_vehicle.',Owner Address:'.$owner_address.',Owner Name:'.$owner_name;
include "qrcode.php";
      $db->query(" update d_dealer_certificate_new set qr_code='".$qimage."' where d_dealer_certificate_id='".$d_dealer_certificate_id."' ");
   
    // include_once(PATH."dealer_apanel/pdf/index.php");

    $result=success_alert('Certificate added successfully'); 

$print_trigger=1;
   
  }  else { $result=error_alert('Have some error'); }
      
     } else { $result=error_alert('Please Renew your credit balance'); }
   
}
// }
?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body ">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-blue bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Certificate Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <!-- <li><a href="dashboard.php" title="View List"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li> -->
                                <?php if($up)  {
                                 $certificate_res=$db->query("select * from d_dealer_certificate where dealer_id='".$_SESSION['dealer_id']."' order by d_dealer_certificate_id desc limit 1"); 
                                 $certificate_row=$certificate_res->fetch_assoc();
                                
                                ?>
                                
                                 <li><a href="mail.php" title="Email"><i class="fa fa-envelope-o fa-lg text-white email"></i></a></li> 
                                 <li><a href="http://ammaenterprises.in/dealer_apanel/certify.php/html1.php?d_dealer_certificate_id=<?php echo $certificate_row['d_dealer_certificate_id']; ?>" class="print_trigger" target="_blank" title="Print"><i class="fa fa-print fa-lg text-white print"></i></a></li>
                            <?php } ?>
                              </ul>
                          </div>                
                        </div>
                       </div>
                       
                  <div class="card-block">
                     
                      <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body">
                          	 <div class="row">
                               <div class="col-md-5">
                        <label>Type</label>
                        <fieldset>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" checked=""
                            value="1">
                            <span class="c-indicator bg-info custom-control-indicator"></span>
                            <span class="custom-control-description">AVERY</span>
                          </label>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" value="2">
                            <span class="c-indicator bg-warning custom-control-indicator"></span>
                            <span class="custom-control-description">3M</span>
                          </label>
                          
                        </fieldset>
                      </div>
                          </div>
                        <div class="row">
                        <div class="form-group col-md-6">
                          <label for="donationinput1">RTO<span class="manfield">*</span></label>
                          <select name="rto" class="select2" style="width: 100%">
                            <option value="">Please select the RTO</option>
                           <?php
                            $speed_res=$db->query(" SELECT * FROM `techs_rto`");
                             while( $speed_row=$speed_res->fetch_assoc() ){
                          ?>
                              <option value="<?=$speed_row['rto_id'];?>" ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                       <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1"> Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder=" Date" name="date"
                             />
                          </div>
                        </div>
 
                      </div>
                      <div class="row">
                       
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Registration No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Registration No" name="vehicle_no" />
                          </div>
                        </div>

                           <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Manufacture Year </label>
                            <input type="text" class="form-control square" placeholder="Vehicle Manufacture Year" name="year" />
                          </div>
                        </div>

                      
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chassis_no"    />
                          </div>
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">ENGINE NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No " name="engine_no"    />
                          </div>
                        </div>
                        
                        
                       
                      </div>
                      <div  class="row">
                          
                         
                        
                         <!--<div class="form-group col-md-6">-->
                         <!--   <label for="donationinput1">Email</label>-->
                         <!--   <input type="text" class="form-control square" placeholder="Email" name="email"    />-->
                         <!-- </div>-->
                        
                    
                        
                      </div>
                      
                      
                      <!--<div class="row sld_input">-->
                         
                      <!--    <div class="col-md-4">-->
                      <!--    <div class="form-group">-->
                      <!--      <label for="donationinput1">SLD Serial NO <span class="manfield">*</span></label>-->
                      <!--      <input type="text" class="form-control square" placeholder="SLD No " name="sld_no" -->
                      <!--        />-->
                      <!--    </div>-->
                      <!--  </div>-->
                        
                      <!--  <div class="col-md-4">-->
                      <!--    <div class="form-group">-->
                      <!--      <label for="donationinput1">SLD Make <span class="manfield">*</span></label>-->
                      <!--      <input type="text" class="form-control square" placeholder="SLD Make " name="sld_make" -->
                      <!--        />-->
                      <!--    </div>-->
                      <!--  </div>-->
                       
                      <!--</div>-->
                      
                      
                      <div class="row">
                          <div class="col-md-12">
                          <div class="form-group">
                            <label for="donationinput1">MAKE OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MAKE OF VEHICLE " name="make_of_vehicle"    />
                            </div>
                          </div>
                      </div>
                      
                    
                    <div  style="background:#fff; padding:20px 10px;margin-bottom:10px;border:1px solid #ccc;">
                      <div  class="row">
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Red 20MM</label>
                            <input type="text" class="form-control square" placeholder="Red 20MM " name="red20" 
                              />
                          </div>
                        </div>
                        
                         <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">White 20MM</label>
                            <input type="text" class="form-control square" placeholder="White 20MM " name="white20" 
                              />
                          </div>
                        </div>
                         
                      </div>
                      </div>
                      
                      
                      <div class="yellowss" style="background:#e4eff3;padding:10px;margin-bottom:10px;border:1px solid #ccc;">
                      
                        <div  class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Red 50MM<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Red 50MM " name="red50"    />
                          </div>
                        </div>
                       
                       <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">White 50MM<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="White 50MM " name="white50"    />
                          </div>
                        </div>
                        
                      </div>
                      
                      
                      <div  class="row">
                        
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="donationinput1">Yellow 50MM</label>
                            <input type="text" class="form-control square" placeholder="Yellow 50MM " name="yellow50" 
                              />
                          </div>
                        </div>
                        
                      </div>
                      
                    </div>  
                      
                      
                      <div style="background:#fff; padding:10px;margin-bottom:10px;border:1px solid #ccc;">
                       <div  class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Circular  80MM<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Circular  80MM " name="cir150"    />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Circular  80MM<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Circular  80MM " name="cir250"    />
                              
                          </div>
                        </div>
                        
                      </div>
                      
                      <div class="row">
                          <div class="col-md-12">
                          <div class="form-group">
                            <label for="donationinput1">Circular  80MM<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Circular  80MM " name="cir350"    />
                          </div>
                        </div>
                      </div>
                      </div>
                      
                      
                      
                      
                       <div  class="row">
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Class 3<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Class 3 " name="class3"    />
                              
                          </div>
                        </div>
                        
                         <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Class 4<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Class 4 " name="class4"    />
                              
                          </div>
                        </div>
                        
                      </div>
  <div  class="row">
                        <!--<div class="col-md-4">-->
                        <!--  <div class="form-group">-->
                        <!--    <label for="donationinput1">Circular  80MM<span class="manfield">*</span></label>-->
                        <!--    <input type="text" class="form-control square" placeholder="Circular  80MM " name="cir350"    />-->
                        <!--  </div>-->
                        <!--</div>-->
                       
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="donationinput1">MODEL OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MODEL OF VEHICLE " name="model_of_vehicle"    />
                          </div>
                        </div>
                        
                      </div>

                      <div  class="row">
                        
                        <!--<div class="col-md-4">-->
                        <!--  <div class="form-group">-->
                        <!--    <label for="donationinput1"> SET SPEED</label>-->
                        <!--    <input type="text" class="form-control square" placeholder="SPEED " name="speed" -->
                        <!--      />-->
                        <!--  </div>-->
                        <!--</div>-->
                      </div>

                      <div  class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">OWNER NAME<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="OWNER NAME " name="owner_name"    />
                          </div>
                        </div>
                        
                         <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">PHONE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="PHONE " name="contact"    />
                          </div>
                        </div>
                        
                        
                        <!--<div class="col-md-4">-->
                        <!--  <div class="form-group">-->
                        <!--    <label for="donationinput1">COP</label>-->
                        <!--    <input type="text" class="form-control square" placeholder="COP " name="cop" -->
                        <!--      />-->
                        <!--  </div>-->
                        <!--</div>-->
                      </div>
                       <div  class="row">
                        
                      </div>
                       
                      <div class="form-group">
                        <label for="donationinput1">OWNER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Owner Address" name="owner_address" ></textarea>
                      </div>
                         <?php
                         $dealer_rows=$db->query("select * from a_dealer where dealer_id='12345'");
                         $dealer_ress=$dealer_rows->fetch_assoc();
                         ?>
                       <div class="form-group">
                        <label for="donationinput1">DEALER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="DEALER Address" name="dealer_address" readonly><?php echo $dealer_ress['address']; ?></textarea>
                      </div>             


                    
                       <div class="row">
                        <!-- <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">QR CODE<span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="qr_code"  />
                          </div>
                        </div> -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Front (image max size 700KB)</label>
                            <input type="file" class="form-control square" name="front"  />
                          </div>
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Rear (image max size 700KB)</label>
                            <input type="file" class="form-control square" name="rear"  />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Side 1 (image max size 700KB)</label>
                            <input type="file" class="form-control square" name="side1"  />
                          </div>
                        </div>
                        
                         <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Side 2 (image max size 700KB)</label>
                            <input type="file" class="form-control square" name="side2"  />
                          </div>
                        </div>
                          
                      </div>
                        <div class="row">
                         
                          
                      </div>


                        <button type="submit" name="submit" class="btn btn-primary pull-right ">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>



<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<style>
</style>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                rto:{ required: true, }, 
                date:{ required: true, },
                vehicle_no :{  required: true, },
                chassis_no : { required: true,  }, 
                engine_no:{ required: true, },
                make_of_vehicle:{ required: true, },
                model_of_vehicle:{ required: true, },
                owner_name:{ required: true },
                contact:{ required: true },
                owner_address:{ required: true },
                
                
            },
            messages:{
                 rto : { required: "Please Select Speed Governor", }, 
                 date : { required: "Please Fill the Dtae",}, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                 chassis_no : { required: "Please enter Chiss No", }, 
                 engine_no : { required: "Please Enter Engine No",},
                 make_of_vehicle : { required: "Please Enter Vehicle Make",},
                 model_of_vehicle : { required: "Please Enter Vehicle Model",},
                 owner_name : { required: "Please Enter Owner Name",}, 
                 contact:{ required: "Please enter  mobile no" },
                 owner_address:{ required: "Please enter owner address" },
                
                
            } ,
    });
  });
</script>

<script>
// <?php if($print_trigger==1){ ?>
// $(".print_trigger").trigger('click');
// $(".print_trigger")[0].click();
// <?php } ?>



  $(".email").click(function(event) {
           
// alert();
$.post('ajax/mail.php',$("#form").serialize(), function(data, textStatus, xhr) {
     
  if($.trim(data)==1)
  {
      
    // $('#view_interestenquiry')[0].reset(); 
$(".email_result").html('<a href="javascript:;" class="alert alert-success" >Thank You Mail sended successfully</a>');
  }
   if($.trim(data)==0)
   {
$(".email_result").html('<a href="javascript:;" class="alert alert-danger" >Unable to send mail</a>');

   }
});

});

$(".print").click(function(){
$.post('../pdf/index.php',function(res){
});
});


  </script>
  
 