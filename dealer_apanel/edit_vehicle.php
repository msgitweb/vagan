<?php $page_title="Update Vehicle Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['up']==1){
    $result=success_alert('Vehicle Details Updated Successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this data !');
}
extract($_REQUEST);
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  $data=array();
  foreach ($_POST as $key => $value) {
    if( !empty($value) ){ $data[$key]=@$db->real_escape_string($value); }
  }
  $image_error=0;
  $filename=$_FILES['governor_photo']['name'];
  $url1=$gphoto;
  if( $filename!='' ){
    $ext=end((explode('.',$filename)));
    $ext=strtolower($ext);
    if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
      $url1='governor_photo'.date('Y-m-d').uniqid().'.'.$ext;
      unlink('../document/'.$gphoto);
      move_uploaded_file($_FILES['governor_photo']['tmp_name'],"../document/".$url1);
    }else{
      ++$image_error;
    }
  }

  $filename=$_FILES['vehicle_photo']['name'];
  $url2=$vphoto;
  if( $filename!='' ){
    $ext=end((explode('.',$filename)));
    $ext=strtolower($ext);
    if( $ext=='png' || $ext=='jpg' || $ext=='jpeg' ){
      $url2='vehicle_photo'.date('Y-m-d').uniqid().'.'.$ext;
      unlink('../document/'.$vphoto);
      move_uploaded_file($_FILES['vehicle_photo']['tmp_name'],"../document/".$url2);
    }else{
      ++$image_error;
    }
  }

  if( $image_error==0 ){

    $up=$db->query(" update `techs_vehicle` set speed_id='".$data['speed_id']."',go_date='".$data['go_date']."',vehicle_no='".$data['vehicle_no']."',chiss_no='".$data['chiss_no']."',engine_no='".$data['engine_no']."',vehicle_make='".$data['vehicle_make']."',vehicle_model='".$data['vehicle_model']."',vehicle_address='".$data['vehicle_address']."',mobile_num01='".$data['mobile_num01']."',mobile_num02='".$data['mobile_num02']."',sl_no='".$data['sl_no']."',ref_no='".$data['ref_no']."',invoice_no='".$data['invoice_no']."',speed='".$data['speed']."',certificate='".$data['certificate']."',governor_photo='".$url1."',vehicle_photo='".$url2."',
       updated_on='$cdate',dealer_name='".$data['dealer_name']."', dealer_address='".$data['dealer_address']."',
       dealer_mobile='".$data['dealer_mobile']."'  where vehicle_id='$vehicle_id' "); 
 
    if( $up ){ 
      header("location:edit_vehicle.php?up=1&vehicle_id=".$vehicle_id);  
    }else{ $result=error_alert('Have some error...'); }

  }else{ $result=error_alert('File must be jpg, jpeg or png'); }

}
$vehicle_id=$db->real_escape_string($vehicle_id);
if( !is_numeric($vehicle_id) ){ header("location:dashboard.php"); }
$vehicle_res=$db->query(" SELECT * FROM `techs_vehicle` where vehicle_id='$vehicle_id' ");
if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-blue bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">Update Vehicle Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="view_vehicle.php" title="View List"><i class="fa fa-arrow-right fa-lg text-white"></i></a></li>
                              </ul>
                          </div>                
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body">
                        
                        <div class="form-group">
                          <label for="donationinput1">Speed Governor <span class="manfield">*</span></label>
                          <select name="speed_id" class="select2" style="width: 100%">
                            <option value="">Please select the Speed Governor</option>
                            <?php
                              $speed_res=$db->query(" SELECT * FROM `techs_rto` where state_id=29  ");
                              while( $speed_row=$speed_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$speed_row['rto_id'];?>" <?php if($speed_row['rto_id']==$vehicle_row['speed_id']){ 
                                echo 'selected="selected"'; } ?> ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Governor fitted Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder="Governor fitted Date" name="go_date"
                            value="<?=$vehicle_row['go_date']?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_no" 
                            value="<?=$vehicle_row['vehicle_no']?>" />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chiss_no" 
                            value="<?=$vehicle_row['chiss_no']?>"   />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Engine No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No" name="engine_no" 
                            value="<?=$vehicle_row['engine_no']?>"   />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Make" name="vehicle_make" 
                            value="<?=$vehicle_row['vehicle_make']?>"   />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Model <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Model" name="vehicle_model" 
                            value="<?=$vehicle_row['vehicle_model']?>"   />
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="donationinput1">Vehicle Owner Name & Address <span class="manfield">*</span></label>
<textarea class="form-control square" placeholder="Vehicle Owner Name & Address" name="vehicle_address" ><?=$vehicle_row['vehicle_address']?></textarea>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Owner Mobile No 1 <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle Owner Mobile No 1" name="mobile_num01" 
                            value="<?=$vehicle_row['mobile_num01']?>" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Owner Mobile No 2</label>
                            <input type="text" class="form-control square" placeholder="Vehicle Owner Mobile No 2" name="mobile_num02" 
                            value="<?=$vehicle_row['mobile_num02']?>" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Name <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Dealer Name" name="dealer_name" 
                            value="<?=$vehicle_row['dealer_name']?>" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Dealer Mobile No</label>
                            <input type="text" class="form-control square" placeholder="Dealer Mobile No" name="dealer_mobile" 
                            value="<?=$vehicle_row['dealer_mobile']?>" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="donationinput1">Dealer Address <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Dealer Address" name="dealer_address" ><?=$vehicle_row['dealer_address']?></textarea>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Sl No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Sl.No" name="sl_no" value="<?=$vehicle_row['sl_no']?>" />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Ref No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Ref.No" name="ref_no" 
                            value="<?=$vehicle_row['ref_no']?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Invoice No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Invoice No" name="invoice_no" 
                            value="<?=$vehicle_row['invoice_no']?>" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Speed <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="speed">
                              <option value="">Please select the speed</option>
                              <option value="40" <?php if($vehicle_row['speed']=='40'){ echo 'selected="selected"'; } ?>>40</option>
                              <option value="45" <?php if($vehicle_row['speed']=='45'){ echo 'selected="selected"'; } ?>>45</option>
                              <option value="50" <?php if($vehicle_row['speed']=='50'){ echo 'selected="selected"'; } ?>>50</option>
                              <option value="60" <?php if($vehicle_row['speed']=='60'){ echo 'selected="selected"'; } ?>>60</option>
                              <option value="80" <?php if($vehicle_row['speed']=='80'){ echo 'selected="selected"'; } ?>>80</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">Certificate <span class="manfield">*</span></label>
                            <select class="form-control square select2" name="certificate">
                                <option value="">Please select Certificate</option>
                                <option value="Fitment" <?php if($vehicle_row['certificate']=='Fitment'){ echo 'selected="selected"'; } ?>>Fitment</option>
                                <option value="Re-calibration" <?php if($vehicle_row['certificate']=='Re-calibration'){ echo 'selected="selected"'; } ?>>Re-calibration</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Speed Governor Photo <span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="governor_photo"  />
                          </div>
                        </div>
                        <div class="col-md-2"><img src="../document/<?=$vehicle_row['governor_photo']?>" class="img-thumbnail" ></div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle Photo <span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="vehicle_photo"  />
                          </div>
                        </div>
                        <div class="col-md-2"><img src="../document/<?=$vehicle_row['vehicle_photo']?>" class="img-thumbnail" ></div>
                      </div>
                        

                        <br>
                        <input type="hidden" name="vphoto" value="<?=$vehicle_row['vehicle_photo'] ?>" />
                        <input type="hidden" name="gphoto" value="<?=$vehicle_row['governor_photo'] ?>" />
                        <button type="submit" name="submit" class="btn btn-primary pull-right">
                          <i class="fa fa-check-square-o"></i> Update
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                speed_id:{ required: true, }, 
                vehicle_no :{  required: true, },
                chiss_no : { required: true,  },  
                engine_no:{ required: true, },
                vehicle_make:{ required: true, },
                vehicle_model:{ required: true, },
                vehicle_address:{ required: true, },
                mobile_num01:{ required: true, },
                sl_no:{ required: true,},
                ref_no:{ required: true },
                invoice_no:{ required: true },
                speed:{ required: true },
                certificate:{ required: true },
                go_date:{ required: true },
                dealer_name:{ required: true },
                dealer_address:{ required: true },
                dealer_mobile:{ required: true },
            },
            messages:{
                 speed_id : { required: "Please Select Speed Governor", }, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                 chiss_no : { required: "Please enter Chiss No", }, 
                 engine_no : { required: "Please Enter Engine No",}, 
                 vehicle_make : { required: "Please Enter Vehicle Make",}, 
                 vehicle_model : { required: "Please Enter Vehicle Model",},
                 vehicle_address : { required: "Please Enter Vehicle Owner Name & Address",}, 
                 mobile_num01 : { required: "Please Enter Mobile No",}, 
                 sl_no : { required: "Please Enter Sl No",}, 
                 ref_no : { required: "Please Enter Ref No",}, 
                 invoice_no : { required: "Please Enter Invoice No",}, 
                 speed : { required: "Please select the Speed",}, 
                 certificate : { required: "Please Select the Certificate",}, 
                 go_date : { required: "Please enter the date",},
                 dealer_name:{ required: "Please enter the dealer name" },
                 dealer_address:{ required: "Please enter dealer address" },
                 dealer_mobile:{ required: "Please enter dealer mobile no" },
            } ,
    });
  });
</script>