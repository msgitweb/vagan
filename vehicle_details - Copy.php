<?php
include"dbconfig.php";
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$vehicle_id=$db->real_escape_string($vddid);
$token=$db->real_escape_string($token);
if( !is_numeric($vehicle_id) || $token=='' || $vehicle_id==''  ){ header("location:error_400.php"); }
$vehicle_res=$db->query(" SELECT * FROM `techs_vehicle` where vehicle_id='$vehicle_id' and token='$token' ");
if( $vehicle_res->num_rows==0 ){ header("location:error_400.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
$res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");
$row01=$res01->fetch_assoc();
$config_res=$db->query("select * from `techs_profile` where id='1' ");
$config_row=$config_res->fetch_assoc();

$rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$row01['rto_id']."' ");
$rto_row01=$rto_res01->fetch_assoc();
?>
<style type="text/css">
  .tab1 tr:nth-child(even){ background: #f2f2f2; }
/*  .tab1 td:hover{ background: #2f93f7;font-weight: bold;color:#fff  }
*/  .table { max-width: 100%; }
  .table td, .table th {
      border-top: table-borderless;
      padding: .75rem 2rem;
  }
  .table td, .table th {
      border-bottom: 1px solid #E3EBF3;
  }
  .table td, .table th {
      padding: .75rem;
      vertical-align: top;
      border-top: 1px solid #98A4B8;
  }
  table {
    border-collapse: collapse;
    background-color: transparent;
  }
  .table th{ text-align: left; font-size: 14px;  }
  body {
      font-family: 'Open Sans',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif;
      font-size: 1rem;
      line-height: 1.45;
      color: #404E67;
  }
</style>

<!DOCTYPE html>
<html>
<head>
  <title>Techno Service -  Vehicle Details</title>
  <meta charset="utf-8">
  <meta name="theme-color" content="#ED2939">
  <meta name="description" content="Techno Service -  Vehicle Details" >
</head>
<body>  
                      
                      <table class="table tab1">
                          <tr>
                            <th colspan="4" style="text-align: center; background: #a0a0ff; font-size: 18px; color: #fff">Techno Service -  Vehicle Details</th>
                          </tr>
                          <tr>
                              <th width="20%">RTO</th>
                              <td><?=$rto_row01['code'].' '.$rto_row01['area']?></td>
                              <th width="20%">DATE</th>
                              <td><?=date('d-M Y',strtotime($vehicle_row['created_on']));?></td>
                          </tr>
                          <tr>
                              <th>VEHICLE NO</th>
                              <td><?=$vehicle_row['vehicle_no']?></td>
                              <th>SPEED GOVERNOR MAKE</th>
                              <td><?=$row01['title']?></td>
                          </tr>
                          <tr>
                              <th>CHISS NO</th>
                              <td><?=$vehicle_row['chiss_no']?></td>
                              <th>SL.NO</th>
                              <td><?=$vehicle_row['sl_no']?></td>
                          </tr>
                          <tr>
                              <th>ENGINE NO</th>
                              <td><?=$vehicle_row['engine_no']?></td>
                              <th>REF.NO</th>
                              <td><?=$vehicle_row['ref_no']?></td>
                          </tr>
                          <tr>
                              <th>VEHICLE MAKE</th>
                              <td><?=$vehicle_row['vehicle_make']?></td>
                              <th>INVOICE NO</th>
                              <td><?=$vehicle_row['invoice_no']?></td>
                          </tr>
                          <tr>
                              <th>VEHICLE MODEL</th>
                              <td><?=$vehicle_row['vehicle_model']?></td>
                              <th>SET SPEED</th>
                              <td><?=$vehicle_row['speed']?></td>
                          </tr>
                          <tr>
                              <th>VEHICLE OWNER NAME & ADDRESS</th>
                              <td style="word-wrap: normal;width: 400px;"><?=$vehicle_row['vehicle_address']?></td>
                              <th>DEALER NAME & ADDRESS</th>
                              <td style="word-wrap: normal;width: 400px;"><?=$vehicle_row['dealer_name'].', '.$vehicle_row['dealer_address'].', '.$vehicle_row['dealer_mobile']?></td>
                          </tr>
                          <tr>
                              <th>OWNER PHONE NO</th>
                              <td><?=$vehicle_row['mobile_num01']?></td>
                              <th>ELECTRONIC SPEED GOVERNER FITTED DATE</th>
                              <td><?=date('d-M Y',strtotime($vehicle_row['go_date']));?></td>
                          </tr>
                          <tr>
                              <th>ELECTRONIC SPEED GOVERNER SERIAL NO</th>
                              <td><?=$vehicle_row['sl_no']?></td>
                              <th>ELECTRONIC SPEED GOVERNER RENEWAL DATE</th>
                              <td><?=date('d-M Y',strtotime($vehicle_row['renewal_date']));?></td>
                          </tr>
                      </table>

                      <table class="table" style="width: 100%">
                          <tr>
                              <td><img src="document/<?=$vehicle_row['governor_photo']?>" class="img-thumbnail" 
                                style="width: 300px; height: 200px" ></td>
                              <td><img src="document/<?=$vehicle_row['vehicle_photo']?>" class="img-thumbnail"
                              style="width: 300px; height: 200px" ></td>
                          </tr>
                          <tr>
                            <td><img src="qrimage/<?=$vehicle_row['qrimage']?>" class="img-thumbnail" ></td>
                            <td></td>
                          </tr>
                      </table>
 </body>
</html>