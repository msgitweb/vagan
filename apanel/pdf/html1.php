<? 
ob_end_clean();
include("dbconfig.php");
$certificate_id=$_GET['d_dealer_certificate_id'];
$certificate_res=$db->query("select * from d_dealer_certificate_new  where   d_dealer_certificate_id='".$certificate_id."'"); 
$certificate_row=$certificate_res->fetch_assoc();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vagansafety</title>
</head>
<link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans:wght@600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"> 

<body>
	<div class="main" class="graph-image graph-7" style="background:url(<?= $certificate_row['back']; ?>)">
		<div class="main-top">
			<div class="main-top-left">
				<img style="height:150px;" src="../../qrimage/<?= $certificate_row['qr_code']; ?>">
			</div>
			<div class="main-top-center">
				<h2 style=" font-family:Arial, Helvetica, sans-serif;font-size:28px;"><?= $certificate_row['company_name']; ?></h2>
				<h3 style="font-family:Arial, Helvetica, sans-serif;">VECHICLE CONSPICUITY ONLINE MIS CERTIFICATE <br>
COMPLIANCE TO AUTOMOTIVE INDUSTRY STANDARD - 089,090&037</h3>
	<!--<div id="print2" style="font-size: 10px;text-align: left;padding-left: 10px;">Copy Link :http://ammaenterprises.in/new/apanel/pdf/html1.php?d_dealer_certificate_id=<?=$certificate_id ?></div>	 -->
			</div>
			<div class="main-top-left rightt">
				<img src="<?= $certificate_row['logo']; ?>" class="logoo" style="padding-top:0px;">
			
			</div>
			<div class="clr"></div>
		</div>

		<div class="to-detailss">
			<div class="to-left">
				<p>TO : </p>
				<? $vtb1=$db->query("select * from techs_rto where rto_id='$certificate_row[rto]'");
				
				$r1=$vtb1->fetch_assoc();
				
				?><p>The Regional Transport Office</p>
				<p> <?= $r1['code'].' '.$r1['area']; ?></p>

			</div>
			<div class="to-left">
				<p class="paste-here">Paste Hologram here</p>
			</div>
			<div class="to-left rightr">
				<? $date=date_create($certificate_row['date']); ?>
				<p> Certificate NO : <b> <?= $certificate_row['certificate_number']; ?> </b></p>
				<p>Fitment Date : <?= date_format($date,"d/m/Y");?></p>
			</div>
			<div class="clr"></div>
		</div>

		<div class="vehicle-details">
			<h3>Vehicle Details</h3>
			<div class="vehicle-details-left">
				<p style="border-left:none;">Registration No : <b> <?= $certificate_row['vehicle_no']; ?>  </b> </p>
				<p style="border-left:none;">Chassis No : <b><?= $certificate_row['chassis_no']; ?></b></p>
				<p style="border-left:none;"> Vehicle Make : <b><?= $certificate_row['make_of_vehicle']; ?></b></p>
			</div>
			<div class="vehicle-details-left">
				<p style="border-right:none;">Registration Year : <b> <?= $certificate_row['year']; ?>  </b> </p>
				<p style="border-right:none;">Engine No : <b><?= $certificate_row['engine_no']; ?></b></p>
				<p style="border-right:none;"> Vehicle Model : <b><?= $certificate_row['model_of_vehicle']; ?></b></p>
			</div>
			<div class="clr"></div>
		</div>


		<div class="vehicle-owner-details">
			<div class="owner-left">
				<h3>Vehicle Owner Details</h3>
				<p style="border-left:none;"> Company Name/Owner Name : <?= $certificate_row['owner_name']; ?> </br>
				 Contact Number : <?= $certificate_row['contact']; ?> </p>
				 <p style="border-left:none;" class="height-details" style="height:85px;"><b> Owner Address / Register Address : </b> <br>
<?= $certificate_row['owner_address']; ?>
</p>
			</div>
			<div class="owner-right">
				<h3>Manufacture & Dealer Details</h3>
				<p>Manufacturer Name : <?= $certificate_row['company_name']; ?> <br>
Dealer Name : Vagansafety PVT LTD</p>

			<div class="height-details" style="border:1px solid #ccc;">
				<span class="righttt-textt">
					<b> MFG Address: </b> <br>
			Narshingpur Industrial Area<br>
			Six Kilometer Stone<br>
			Delhi Jaipur Highway,Gurgaon<br>
			Delhi / India.

				</span>
				<span class="righttt-textt" style="padding-left: 10px; border-right: none;">
					<b> Dealer Address: </b> <br>
		<?= $certificate_row['dealer_address']; ?><br>


				</span>
				<div class="clr"></div>
			</div>

			</div>
			<div class="clr"></div>
		</div>



		<div class="tapesss">
			<div class="tapesss-left">
				<p style="height:35px;"><b style="font-size: 16px;line-height: 16px;">Conspicuity Tapes 20MM <br> Fitment Details</b></p>
				<p style="height:35px;">20MM - RED : <?= $certificate_row['red20']; ?> Mtrs  </p>
				<p  style="height:35px;">20MM WHITE :<?= $certificate_row['white20']; ?> mtr</p>
				<p   style="height:35px;"><b style="font-size: 16px;line-height: 16px;">Conspicuity Tapes 50MM <br> Fitment Details </b> </p>
				<p   style="height:35px;">50MM-RED : <?= $certificate_row['red50']; ?> Mtrs </p>
				<p style="height:25px;">50MM-WHITE : <?= $certificate_row['white50']; ?> Mtrs  </p>
				<p>50MM-YELLOW : <?= $certificate_row['yellow50']; ?> Mtrs </p>
			</div>
			<div class="tapesss-center">
				<p style="height:35px;"><b style="font-size: 16px;line-height: 16px;">Rear Marking Plates Details</b></p>
				<p style="height:35px;">Class 3 : <?= $certificate_row['class3']; ?> - <br>
Alternative Strips</p>
				<p  style="height:35px;">Class 4 : <?= $certificate_row['class4']; ?></p>
				<p   style="height:35px;"><b style="font-size: 16px;line-height: 16px;">80MM Circular Reflector </b> </p>
				<p   style="height:35px;">80MM Red Circular Reflector : <?= $certificate_row['cir150']; ?> Nos</p>
				<p style="height:25px;">80MM White Circular Reflector :<?= $certificate_row['cir250']; ?> Nos</p>
				<p>80MM Yellow Circular Reflector : <?= $certificate_row['cir350']; ?> Nos</p>
			</div>
			<div class="tapesss-right">
				<p style="height:35px;"><b style="font-size: 16px;line-height: 16px;">Certificate Details</b></p>
				<p style="height:35px;">Type Approved Number: <?= $certificate_row['aptype']; ?></p>
				<p  style="height:35px;">COP Number : <br> <?= $certificate_row['cop']; ?></p>
				<p   style="height:35px;">Test Report Number : SHL/16/2007-2008/2528/1584</p>
				<p   style="height:35px;">REAR MARK :<?= $certificate_row['mark']; ?></p>
				<p style="border-bottom: none;">Certified By : The Automotive Research Association
of India (ARAI) </p>
			</div>
			<div class="clr"></div>
		</div>


		<div class="the-maxx">
			<p>The Maximun all-inclusive price (including GST and Fitment Charges)for the products specified in this Certificate is as below: <br> 20mm: Rs. 30/Feet; 50mm: Rs. 46/feet; C3: Rs. 650/piece; C4: Rs. 750/piece;</p>
			<p><b>Fitment Images</b></p>
			<div class="front-design">
				<div class="front-dd">
					<p>Front</p>
					<img src="../../document/<?= $certificate_row['front']; ?>">
				</div>
				<div class="front-dd">
					<p>Back</p>
					<img src="../../document/<?= $certificate_row['rear']; ?>">
				</div>
				<div class="front-dd">
					<p>Side-1</p>
					<img src="../../sld_logo/<?= $certificate_row['side1']; ?>">
				</div>
				<div class="front-dd" style="border-right: none;">
					<p>Side-2</p>
					<img src="../../sld_logo/<?= $certificate_row['side2']; ?>">
				</div>
				<div class="clr"></div>
			</div>
		</div>

		<div class="parrr">
			<div class="parr-left">
				<p>This is to certify that we have authorised Distributor / Dealer for the sale AIS-089,090,&037 Compliant <?= $certificate_row['company_name']; ?> Brand Retro reflective Tapes <br> Supplied by us as per CMVR 104-1989</p>
				<p style="border-top: none;">We hereby certify that we have supplied/installed ICAT/ARAI Approved Retro Reflective Tapes as per CMRV rule 104 specified under CMVR <br> GSR 784 (E)</p>
			</div>
			<div class="parr-right">
				<p style="border-bottom: 1px solid #ccc;">Authorized Dealer Seal & Signature</p>
			 </div>
			<div class="clr"></div>
		</div>


		<div class="signature-sig" style="padding-top: 30px; border: 1px solid #000; border-top: none;">
			<div class="signature-left">
				<p style="padding-bottom: 5px;">________________________________</p>
				<p>Authorized Dealer Signature</p>
			</div>
			<div class="signature-right">
				<p style="padding-bottom: 5px;">__________________________</p>
				<p>Customer Signature</p>
			</div>
			<div class="clr"></div>
		</div>

		<p style="padding: 10px; font-weight: bold; text-align: center; font-size: 11px;">[Note: HOLOGRAM MANDATORY WITHOUT HOLOGRAM CERTIFICATE NOT VALID]</p>
 
<p style="width:150px;text-align:center;margin:0 auto;">	<input type="button" id="print" value="Customer Copy" style="border-radius:5px;background:#552f84; color:#fff;padding:10px 20px;border:none;" onclick="javascript:printDiv('printablediv')" />
	</p>
<!--	<a id="print1" target="_blank" href="../../qrimage/<?= $certificate_row['qr_code']; ?>" style="text-decoration:none;">Download QR Code</a> -->

	</div>
	

	
</body>
</html>
<style type="text/css">

	.parr-right{ float: left; width: 25%; }
	.signature-left{ float: left; width: 45%; font-size: 13px; padding: 10px; }
	.signature-right{ float: right; width: 50%; text-align: right; font-size: 13px; padding: 10px; }
	.parr-right p{ font-size: 12px; text-align: center; font-weight: bold; vertical-align: bottom; padding-top: 85px; padding-bottom: 10px; }
	.parrr{ border: 1px solid #000; border-top: none; border-bottom: none; }
	.parr-left{ float: left; width: 75%;  }
	.parr-left p{ border: 1px solid #ccc; padding:5px 10px; font-size: 11px; line-height: 22px; }
	.front-dd{     float: left;
    width: 24.8%;
  
    border: 1px solid #ccc; }
    .front-dd img{ padding-top: 10px; padding-bottom: 10px; padding-left: 5px; width:80%; height:170px; }
	.the-maxx{ border: 2px solid #000; margin-top: 2px; border-bottom: none; }
	.the-maxx p{ border-bottom: 1px solid #ccc; font-size: 14px !important; padding:5px 10px; }
	.tapesss-left p{border: 1px solid #ccc; padding:3px 10px;  font-size: 14px; }
	.tapesss-center p{border: 1px solid #ccc; padding:3px 10px;  font-size: 14px;  }
	.tapesss-right p{border: 1px solid #ccc; padding:3px 10px;  font-size: 14px;  }
	.tapesss-left{ float: left; width: 27%; }
	.tapesss-center{ float: left; width: 37%;}
	.tapesss-right{ float: left; width: 36%; }
	.tapesss{ border: 2px solid #000; margin-top: 2px; }

	b font-size: 14px; }
	.height-details{ height: 90px; padding: 10px;border: 1px solid #ccc;}
	.righttt-textt{ width: 45%; float: left; border-right: 2px solid #ccc; height: 90px; font-size:14px; padding-left:12px; }
	.vehicle-owner-details{ border: 2px solid #000; margin-top: 2px; }
	.vehicle-owner-details p{ border: 1px solid #ccc; padding:3px 10px; text-align: left; font-size: 14px; }
	.owner-left{ float: left; width: 52%; }
	.owner-right{ float: left; width: 48%; }
	.vehicle-owner-details h3{ padding:5px 10px;border-right:2px solid #ccc;border-bottom:1px solid #ccc;}
	.vehicle-details-left p{ border: 1px solid #ccc; padding:5px 10px; text-align: center; }
	.vehicle-details{ border: 2px solid #000; font-size: 15px; }
	.vehicle-details h3{  padding:3px 10px; border-bottom:1px solid #ccc; font-size:18px !important; }
	*{ margin: 0; padding: 0; }
	h2{ font-size: 32px;}
	.main{ width: 990px; margin: 0 auto; /*border: 5px solid #ccc; */font-family: }
	.main-top-left{ width: 20%; float: left; }
	.main-top-center{ float: left; width: 55%; text-align: center; text-transform: uppercase; vertical-align: center; margin-top: 35px; }
	.clr{ clear: both; }
	.main-top-center h3{  line-height: 20px;  font-size: 14px; font-weight:bold; }
	.logoo{ padding-top: 25px; }
	.rightt{ float: right; padding-right: 30px; }
	.to-left{ float: left; width: 30%; padding: 10px; }
	p{}
	.paste-here{     border: 1px dashed #ccc;
    padding: 10px;
    width: 200px;
    margin: 0 auto;
    text-align: center; }
	.rightr{ float: right !important; text-align: right; }
	.vehicle-details-left{ float: left; width: 50%; }
	.front-dd img {height:170px;}
	
/*	.graph-7{background: url(../img/graphs/graph-7.jpg) no-repeat;}*/
/*.graph-image img{display: none;}*/
/*	@media print{*/
/*  .graph-image img{display:inline;}*/
}
</style>
 <script language="javascript" type="text/javascript">
        function printDiv(divID) {
            document.getElementById("print").style.display = "none";
          //   document.getElementById("print1").style.display = "none";
              document.getElementById("print2").style.display = "none";
            window.print();
          
        }
    </script>