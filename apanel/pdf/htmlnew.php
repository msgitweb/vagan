
 <style type="text/css">
 	div{ 
 		background-image: url(http://demo.360degreeinfo.website/Dealer_certification/dealer_apanel/pdf/background.jpg);
		background-repeat: no-repeat;  
		background-size: cover;
		width: 100%;
		height: 1000px;
		min-height: 900px;
		padding-top: 70px;
		padding-left: 40px;
		padding-right: 40px;
		 
	   }
	 h1, h2 ,h3 { 
	    text-align: center;
	    margin: 0px;
	    padding: 0px; 
	    text-transform: uppercase;
			}	
	table , tr , td{
	 	border: 1px solid #CCC;
		border-collapse: collapse; 
		vertical-align: top;
		padding: 5px; 
		font-size: 18px; 
		 
	}
	.first  tr  td{ 
		width: 310px; 
	}

	.second  tr  td{ 
		width: 199px; 
	}
	/*.second > tr > td { width: 78% !important; }*/

	 

 </style>
<!DOCTYPE html>
<html>
<head>
	<title>Speed Limiting Device</title>
</head> 
<body style="">
	<div style="">
		<h1 style="text-align: center;">Speed Limiting Device</h1><br>
		<h3 style="text-align: center;">Online Renewal Certificate</h3>
		<h3 style="text-align: center;">(COMPLAINCE to AIS 018,AIS 037 STANDARD)</h3>
		<h3 style="text-align: center;">AIS (PART-3)</h3>
		<br>
		<table class="first">
			<tr>
				<td>RTO : <?php echo $rto_row['code']?>-<?php echo $rto_row['area']?></td>
			<td>Date: <?=date('d-F-Y',strtotime($certificate_row['date']));?></td>
			</tr>
			<tr>
				<td>Vehicle No: <?php echo $certificate_row['vehicle_no']?></td>
				<td>Validity From : To : <?=date('d-F-Y',strtotime($certificate_row['renewal_date']));?></td>
			</tr>
			<tr>
				<td>Chassis No: <?php echo $certificate_row['chassis_no']?></td>
				<td>SLD Make: <?php echo $certificate_row['sld_make']?></td>
			</tr>
			<tr>
				<td>Engine No: <?php echo $certificate_row['engine_no']?></td>
				<td>SLD Serial No: <?php echo $certificate_row['sld_no']?></td>
			</tr>
			<tr>
				<td>Vehicle Make: <?php echo $certificate_row['make_of_vehicle']?></td>
					<td>TAC NO: <?php echo $certificate_row['tac_no']?></td>
			</tr>
			<tr>
				<td>Vehicle Model: <?php echo $certificate_row['model_of_vehicle']?></td>
			<td>Set Speed : <?php echo $certificate_row['speed']?></td>
			</tr>
			<tr>
				<td>Owner Name: <?php echo $certificate_row['owner_name']?></td>
				<td>COP Validtity: <?php echo $certificate_row['cop']?></td>
			</tr>
			<tr>
				<td>Phone: <?php echo $certificate_row['contact']?></td>
				<td></td>
			</tr>
			<tr>
				<td>Invoice No:</td>
				<td></td>
			</tr>
			<tr>
				<td><b>Owner Address: </b></td>
				<td><b>Dealer Address: </b></td>
			</tr>
			<tr>
				<td> <?php echo $certificate_row['owner_address']?></td>
                <td> <?php echo $certificate_row['dealer_address']?></td>

				<!-- <td>Chennai , Tamilnadu</td> -->
				<!-- <td>Gomathi automobile technology pvt and solution,4/196,arcot road,kodambakkam,chennai-600042</td> -->
			</tr>
		</table>
		  <table class="second">
			<tr>
				<td align="center"><b>Certificate QR Code</b></td>
				<td align="center"><b>Vehicle Photo</b></td>
				<td align="center"></td>
				
			</tr>
			<tr>
				<td>
				    <?php if($certificate_row['qr_code']!=""){?>
				        <img src="<?= BASE_URL?>qrimage/<?php echo $certificate_row['qr_code']?>" width="200" height="140">
				    <?php } ?>
				</td>
				<td>
				    <?php if($certificate_row['vehicle_photo']!=""){?>
				        <img src="<?=BASE_URL?>document/<?php echo $certificate_row['vehicle_photo']?>" width="200" height="140">
				    <?php } ?>
				    </td>
			
			</tr>
			<tr>
				<td colspan="3" align="right"><b>Authorised</b></td> 
			</tr> 
			</table>
			<table>
			<tr>
				<td width="640" style="text-align: justify;"> This is to certify that following has been fitted with approved <b><?php echo $certificate_row['sld_make']?></b> Electronic Speed Governor which is set to a maximum pre set speed of <b><?php echo $certificate_row['speed']?></b> and shall not exceed this speed in any circumstances,unless the device is tampered or the seal is broken by unauthorised technicians or individual.</td>
			
			</tr>
			<tr>
				<td width="640">
						We have sealed with device at critical points  and the certificate is being issued only after testing
				</td>
			
			</tr>
		
			    

		</table> 
		 
		 	<td valign="bottom" align="center" width="210">Dealer Signature</td>
		
		 
	</div>
	
</body>
</html>

 