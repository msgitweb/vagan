<?php $page_title="Update Renewal Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
$dealer_credits_id = $db->real_escape_string($dealer_credits_id);
$entry_by = $db->real_escape_string($entry_by);
$notes = $db->real_escape_string($notes);

$dealer_credit_res=$db->query("select * from a_dealer_credits 
  where dealer_credits_id='".$dealer_credits_id."' and approval_status=0 ");

/* if data is not approver */
if( $dealer_credit_res->num_rows > 0 ) {
  if( $payment_type == 1 ) { // credit

    $up=$db->query("update `a_dealer_credits` set  dealer_id='$dealer_id', `date`='$date', entry_by='$entry_by', notes='$notes', updated_on='".date('Y-m-d H:i:s')."', credit_debit_count='$credit', `type`='$payment_type' where dealer_credits_id='$dealer_credits_id' and approval_status=0 ");  
    if($up){
      $result=success_alert('Data Updated successfully.');
    } else {
      $result=error_alert('Sorry. We cant Update this Dealer Credits!');
    }

  }

  if( $payment_type == 2 ) { // debit

      $account_info_res = $db->query(" SELECT * FROM `a_dealer_credit_account`where dealer_id='$dealer_id' ");
      $account_info_row = $account_info_res->fetch_assoc();

      if( $account_info_row['credit_balance'] >= $credit ){

        $up=$db->query("update `a_dealer_credits` set  dealer_id='$dealer_id', `date`='$date', entry_by='$entry_by', notes='$notes', updated_on='".date('Y-m-d H:i:s')."', credit_debit_count='$credit', `type`='$payment_type' where dealer_credits_id='$dealer_credits_id' and approval_status=0 ");  
        if($up){
          $result=success_alert('Data Updated successfully.');
        } else {
          $result=error_alert('Sorry. We cant Update this Dealer Credits!');
        }

      } else {
        $result=error_alert('Sorry. Credit is not available in your account !!!');
      }

      
  }

} else {
/* if data is approved */
  $up=$db->query("update `a_dealer_credits` set  `date`='$date', entry_by='$entry_by', notes='$notes', updated_on='".date('Y-m-d H:i:s')."' where dealer_credits_id='$dealer_credits_id' and approval_status=1 ");  
  if($up){
    $result=success_alert('Data Updated successfully. Except credit details.');
  } else {
    $result=error_alert('Sorry. We cant Update this Dealer Credits!');
  }
}
    

}

$dealer_credits_id=$db->real_escape_string($dealer_credits_id);
if( !is_numeric($dealer_credits_id) ){ header("location:dashboard.php"); }
$vehicle_res=$db->query(" SELECT * FROM `a_dealer_credits` where dealer_credits_id='$dealer_credits_id' ");

if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();

$account=$db->query("select * from a_dealer_credit_account where dealer_id='".$vehicle_row['dealer_id']."'");
$account_res=$account->fetch_assoc();

// $account1=$db->query("select * from a_dealer_credit_debit_history where dealer_id='".$vehicle_row['dealer_id']."'");
// $account_res1=$account1->fetch_assoc();


?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
  <?php echo $result; ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="card border-blue">
        <div class="card-header bg-blue height-50">
          <h4 class="card-title text-white">Edit Renewal Entry</h4>
          <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a href="view_dealer_credit.php" title="View List"><i class="fa fa-arrow-right fa-lg white"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-body collapse in">
          <div class="card-block card-dashboard">
            
            <div class="row">
              <div class="col-md-12">
                <form method="post" id="form">
                  <div class="col-md-7">
                    <div class="row balance_option" style="margin-bottom:10px">
                      <div class="col-md-12">
                        <label>Payment Type</label>
                        <fieldset>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" checked=""
                            value="1">
                            <span class="c-indicator bg-info custom-control-indicator"></span>
                            <span class="custom-control-description">Credit</span>
                          </label>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" value="2">
                            <span class="c-indicator bg-warning custom-control-indicator"></span>
                            <span class="custom-control-description">Debit</span>
                          </label>
                          
                        </fieldset>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="userinput2">Dealer</label>
                          <select name="dealer_id" class="select2 dealer_id" style="width: 100%" <?php if($vehicle_row['approval_status'] == 1){ echo 'disabled';} ?>>
                            <option value="">Please select Your dealer</option>
                            <?php
                            $property=$db->query("select * from a_dealer");
                            while($property_result=$property->fetch_assoc())
                            {
                            ?>


<option value="<?=$property_result['dealer_id']?>" <?=($property_result['dealer_id']==$vehicle_row['dealer_id'])?'selected':''?> ><?php echo $property_result['name']; ?> / <?php echo $property_result['mobile']; ?></option>


                            
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="userinput2">Date</label>
                          <input type="text" class="form-control square date" placeholder="Date" value="<?php echo $vehicle_row['date']?>" name="date"
                          />
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group" >
                          <label for="userinput2">Credit</label>
                          <input type="text" class="form-control square" placeholder="Credits" name="credit" value="<?php echo $vehicle_row['credit_debit_count']?>" <?php if($vehicle_row['approval_status'] == 1){ echo 'disabled';} ?>/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="userinput2">Entry By</label>
                          <input type="text" class="form-control square" placeholder="Entry By" name="entry_by" value="<?php echo $vehicle_row['entry_by']?>"  />
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="userinput2">Note</label>
                          <textarea class="form-control square" placeholder="Notes" name="notes"><?= $vehicle_row['notes'];?></textarea>
                        </div>
                      </div>
                    </div>
                    
                    <input type="submit" name="submit" value="Update" class="btn btn-success">
                  </div>
                  <div class="col-md-5" style="margin-top: 10px;">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="card bg-primary">
                          <div class="card-body" style="box-shadow: 5px 10px 8px #888888;">
                            <div class="card-block">
                              <div class="media">
                                <!-- <div class="media-left media-middle">
                                  <span style="color: #fff;font-size: 24px;">RM</span>
                                </div> -->
                                <div class="media-body white text-xs-right">
                                  <h6>Balance Renewal</h6>
                                  <span id="credit_balance" ><?=($account_res['credit_balance'])?$account_res['credit_balance']:"0";?></span>

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card bg-danger">
                          <div class="card-body" style="box-shadow: 5px 10px 8px #888888;">
                            <div class="card-block">
                              <div class="media">
                                <!-- <div class="media-left media-middle">
                                  <span style="color: #fff;font-size: 24px;">RM</span>
                                </div> -->
                                <div class="media-body white text-xs-right">
                                  <h6>Total Renewal</h6>
                                  <span id="total_credit" ><?=($vehicle_row['credit_debit_count'])?$vehicle_row['credit_debit_count']:"0";?></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="card border-blue" style="box-shadow: 5px 10px 8px #888888;">
                        <div class="card-header bg-blue height-50">
                          <h4 class="card-title text-white">Recent Renewal Entry List</h4>
                        </div>
                        <div class="card-body collapse in">
                          <div class="card-block card-dashboard">
                            <table class="table table-bordered">
                              <thead style="background: #9e9e9e; color: #fff">
                                <tr>
                                  <th>Type</th>
                                  <th>Count</th>
                                  <th>Date</th>
                                </tr>
                              </thead>
                              <tbody class="dealers_history">
                                <tr>
                                <td><span id="type"><?=($vehicle_row['type']==1)?"Credit":"Debit"?></span></td>
                                <td><span id="count"><?=($vehicle_row['credit_debit_count'])?$vehicle_row['credit_debit_count']:"0"?></span></td>
                                <td><span id="date"><?=($vehicle_row['created_on'])?$vehicle_row['created_on']:"0000-00-00"?></span></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: {
dealer_id:{ required: true, },
date :{  required: true, },
credit : { required: true,  },
entry_by:{ required: true, },

},
messages:{
dealer_id : { required: "Please Select Reference Number", },
date : { required: "Please Fill the Date",},
credit : { required: "Please enter Credit Count", },
entry_by : { required: "Please Enter Entry By Name",},

} ,
    });
  });
</script>