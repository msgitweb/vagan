<?php $page_title="Add Renewal"; include("header.php");
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);

$dealer=$db->query("select * from a_dealer where dealer_id='".$_GET['dealer_id']."'");
$dealer_res=$dealer->fetch_assoc();

$acc=$db->query("select * from a_dealer_credit_account where dealer_id='".$dealer_res['dealer_id']."'");
$acc_res=$acc->fetch_assoc();


if(isset($_REQUEST['submit'])){


$reference_number='CRF'.time().mt_rand(1000,99999);

$entry_by=$db->real_escape_string($entry_by);
$notes=$db->real_escape_string($notes);

 

$up=$db->query(" INSERT INTO `a_dealer_credits`(`reference_number`, `dealer_id`, `date`,`entry_by`,`notes`,`created_on`,`credit_debit_count`,`type`) VALUES
('".$reference_number."','".$dealer_id."', '".$date."', '".$entry_by."','".$notes."','".$cdate."','".$credit."','".$payment_type."') ");


if($up){
$result=success_alert('Dealer Credits added Successfully. Please Approve it!');
} else {
$result=error_alert('Sorry. We cant Add this Dealer Credits!');
}

} 
?>
<div class="content-body">
  <?php echo $result; ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="card border-blue">
        <div class="card-header bg-blue height-50">
          <h4 class="card-title text-white">Add Renewal</h4>
          <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
           <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a href="view_renewal.php" title="View List"><i class="fa fa-arrow-right fa-lg white"></i></a></li>
            </ul>
          </div> 
        </div>
        <div class="card-body collapse in">
          <div class="card-block card-dashboard">
            
            <div class="row">
              <div class="col-md-12">
                 <div class="col-md-2"></div>
                    <div class="col-md-10">
                <form method="post" id="form">
                  <div class="col-md-12">
                    <div class="col-md-10" style="padding:0px;">
                        <div class="form-group" >
                          <label for="userinput2">Dealer Name</label>
                          <select name="dealer_id" class="select2 dealer_id" style="width: 100%">
                            <option value="">Please select Your dealer</option>
                            <?php
                            $property=$db->query("select * from a_dealer");
                            while($property_result=$property->fetch_assoc())
                            {
                            ?>
<option value="<?=$property_result['dealer_id']?>" <?=($property_result['dealer_id']==$dealer_res['dealer_id'])?'selected':''?> ><?=$property_result['name']?></option>

                           <!--  <option value="<?php echo $property_result['dealer_id'];?>"><?php echo $property_result['name']; ?></option> -->
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="row col-md-10" style="padding:0px;">
                      <!-- <div class="card border-blue" style="box-shadow: 5px 10px 8px #888888;"> -->
                        
                        <div class="card-body collapse in">
                          <div class="card-block card-dashboard">
                            <table class="table table-bordered">
                              <thead style="background: #9e9e9e; color: #fff">
                                <tr>
                                  <th>Total Renewal</th>
                                  <th>Used Renewal</th>
                                  <th>Balance Renewal</th>
                                </tr>
                              </thead>
                              <tbody class="dealers_history">
                                <tr>
                                <td><span id="type"><?=($acc_res['total_credit'])?$acc_res['total_credit']:"0"?></span></td>
                                <td><span id="count"><?=($acc_res['used_credit'])?$acc_res['used_credit']:"0"?></span></td>
                                <td><span id="date"><?=($acc_res['credit_balance'])?$acc_res['credit_balance']:"0"?></span></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      <!-- </div> -->
                    </div>
                    <div class="row balance_option" style="margin-bottom:10px">
                      <div class="row">
                      <div class="col-md-5">
                        <label>Payment Type</label>
                        <fieldset>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" checked=""
                            value="1">
                            <span class="c-indicator bg-info custom-control-indicator"></span>
                            <span class="custom-control-description">Credit</span>
                          </label>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" value="2">
                            <span class="c-indicator bg-warning custom-control-indicator"></span>
                            <span class="custom-control-description">Debit</span>
                          </label>
                          
                        </fieldset>
                      </div>
                    <div class="col-md-5">
                        <div class="form-group">
                          <label for="userinput2">Date</label>
                          <input type="text" class="form-control square date" placeholder="Date" name="date" 
                          />
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      
                      
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="userinput2">Credit</label>
                          <input type="text" class="form-control square" placeholder="Credits" name="credit"  />
                        </div>
                      </div>

                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="userinput2">Entry By</label>
                          <input type="text" class="form-control square" placeholder="Entry By" name="entry_by"   />
                        </div>
                      </div>
                      
                    </div>
                    
                    <div class="row">
                       
                      <div class="col-md-10">
                        <div class="form-group">
                          <label for="userinput2">Note</label>
                          <textarea class="form-control square" placeholder="Notes" name="notes"></textarea>
                        </div>
                      </div>
                    </div>
                    
                    <input type="submit" name="submit" value="Save" class="btn btn-success">
                  </div>
                  
                </form>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.manfield{ color:  #fa5635;  }
form label{
text-transform: capitalize;
font-weight: bold;
}
</style>
<style>
.picker__holder{
position: relative;
bottom:-150px!important;
z-index: 1000000000000;
}
</style>
<?php include("footer.php") ?>

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">

jQuery(document).ready(function($) {
$(".date").pickadate({format:'yyyy-mm-dd'});
$(".select2").select2();
$('#form').validate({
rules: {
dealer_id:{ required: true, },
date :{  required: true, },
credit : { required: true,  },
entry_by:{ required: true, },

},
messages:{
dealer_id : { required: "Please Select Dealer Name", },
date : { required: "Please Fill the Date",},
credit : { required: "Please enter Credit Count", },
entry_by : { required: "Please Enter Entry By Name",},

} ,
});

});
</script>
 <script type="text/javascript">
  $(".dealer_id").change(function(event) {
    
if( $(this).val()!='' ){
   
$("body").loading('start');
$.post('ajax/getdata.php', {con: '4',dealer_id:$(".dealer_id").val()}, function(data, textStatus, xhr) {
jsn=$.parseJSON(data);
$("#type").text(jsn.total_credit);
$("#count").text(jsn.used_credit);
$("#date").text(jsn.credit_balance);
});
$.post('ajax/getdata.php', {con: '5',dealer_id:$(this).val()}, function(data, textStatus, xhr) {
  jsn=$.parseJSON(data);
$("#type").text(jsn.total_credit);
$("#count").text(jsn.used_credit);
$("#date").text(jsn.credit_balance);
});
$("body").loading('stop');
}else{
$("#payment_details").html('');
$("#balance_amount_txt").text('');
$("#paid_amount_txt").text('');
$("#total_amount_txt").text('');
}
});

</script> 