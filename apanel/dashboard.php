<?php $page_title='Dashboard'; include("header.php");
$speed_list=$db->query(" SELECT dealer_id FROM `a_dealer`  ");
$record_list=$db->query(" SELECT dealer_id FROM `a_dealer`  ");
$records_list1=$db->query(" SELECT d_dealer_certificate_id FROM `d_dealer_certificate_new`  ");


// $record_active_list=$db->query(" SELECT vehicle_id FROM `techs_vehicle` where renewal_date>='".date('Y-m-d')."'  ");
// $record_exp_list=$db->query(" SELECT vehicle_id FROM `techs_vehicle` where renewal_date<'".date('Y-m-d')."'  ");
?>
<style type="text/css">
    .card{ margin-bottom: 5px !important; }
</style>
<div class="content-body">

<div class="row">
    <div class="col-md-12">
        <div class="col-md-4">
            <!--<a href="speed_governor.php">-->
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-primary bg-darken-2 media-left media-middle">
                            <i class="fa fa-user font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-primary white media-body">
                            <h5><b>Total Dealer</b></h5>
                            <h5 class="text-bold-400"><?=number_format($speed_list->num_rows) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <!--</a>-->
        </div>
        <!--<div class="col-md-3">-->
            <!--<a href="view_vehicle.php">-->
        <!--    <div class="card">-->
        <!--        <div class="card-body">-->
        <!--            <div class="media">-->
        <!--                <div class="p-2 text-xs-center bg-warning bg-darken-2 media-left media-middle">-->
        <!--                    <i class="fa fa-line-chart font-large-2 white"></i>-->
        <!--                </div>-->
        <!--                <div class="p-2 bg-warning white media-body">-->
        <!--                    <h5><b>Dealer Renewal</b></h5>-->
        <!--                    <h5 class="text-bold-400"><?=number_format($record_list->num_rows) ?></h5>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
            <!--</a>-->
        <!--</div>-->
        <?php 
        $records_list=$db->query(" SELECT sum(`credit_balance`) as total_count FROM `a_dealer_credit_account`  ");
        $record_res=$records_list->fetch_assoc();
        ?>
        <div class="col-md-4">
            <!--<a href="active_vehicle.php">-->
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-success bg-darken-2 media-left media-middle">
                            <i class="fa fa-sort-amount-desc font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-success white media-body">
                            <h5><b>Total Balance Credit</b></h5>
                            <h5 class="text-bold-400"><?=number_format($record_res['total_count']) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <!--</a>-->
        </div>

        <div class="col-md-4">
            <!--<a href="expired_vehicle.php">-->
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <div class="p-2 text-xs-center bg-danger bg-darken-2 media-left media-middle">
                            <i class="fa fa-certificate font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-danger white media-body">
                            <h5><b>Certificates Issued</b></h5>
                            <h5 class="text-bold-400"><?=number_format($records_list1->num_rows) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <!--</a>-->
        </div>

    </div>

         
        
    </div>
<br>
<div class="row">
   <div class="col-md-12">
        <div class="card">
            <div class="card-header height-50" style="background: #030b18">
                <h4 class="card-title text-white">Recent Certificate List</h4>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead style="background: #867ef6; color:#fff">
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="15%">RTO</th>
                          <th width="15%">Vehicle No</th>
                          <th width="10%">Renewal Date</th>
                          <th width="20%">Vehicle Owner</th>
                          <th width="20%">Action</th>
                          <!--<th width="12%">View</th>-->
                        </tr>
                      </thead>
                      <tbody>
                            <?php
                             $sno=0;
                             $res=$db->query("select * from `d_dealer_certificate_new` order by d_dealer_certificate_id desc limit 10");
                             while( $row=$res->fetch_assoc() ){
                                $res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$row['rto']."' ");
                                $row01=$res01->fetch_assoc();
                            ?>
                                <tr>
                                    <td><?=++$sno;?></td>
                                    <td><span class="tag tag-default"><?=$row01['code']?>-<?=$row01['area']?></span></td>
                                    <td><span class="tag tag-danger"><?=$row['vehicle_no']?></span></td>
                                    <td><?=date('d-M Y',strtotime($row['renewal_date']))?></td>
                                    <td><?=$row['owner_name']?></td>
                                    <td><a href="http://vagansafety.org/apanel/pdf/html1.php?d_dealer_certificate_id=<?php echo $row['d_dealer_certificate_id']; ?>" title="View Vehicle Details" class="btn btn-warning btn-sm" target="_blank"><i class="fa fa-print printt"></i></a><a id="print1" class="qqrrcode" target="_blank" href="../qrimage/<?= $row['qr_code']; ?>" style="text-decoration:none;">QR Code</a></td>
                                </tr>
                            <?php } ?>
                      </tbody>
                      
                    </table>
                </div>
                </div>                    
            </div>
        </div>
    </div>
</div>
<style>
    .qqrrcode {
    background: #333;
    margin-left: 5px;
    padding: 0px!important;
    padding-left:2px!important;;
    padding-right:2px!important;;
    padding-top:5px!important;;
    padding-bottom:5px!important;;
    color: #fff;
    font-size: 12px;
    border-radius: 5px;
}
</style>

</div>
<?php include("footer.php") ?>

<script src="../app-assets/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="../app-assets/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="../app-assets/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="../app-assets/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(e) {
$("#recent-buyers").perfectScrollbar({wheelPropagation:!0});   
            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 250;

            // random data generator for plot charts

            function getRandomData() {
                if (data.length > 0) data = data.slice(1);
                // do a random walk
                while (data.length < totalPoints) {
                    var prev = data.length > 0 ? data[data.length - 1] : 50;
                    var y = prev + Math.random() * 10 - 5;
                    if (y < 0) y = 0;
                    if (y > 100) y = 100;
                    data.push(y);
                }
                // zip the generated y values with the x values
                var res = [];
                for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
                return res;
            }

            function randValue() {
                return (Math.floor(Math.random() * (1 + 50 - 20))) + 10;
            }
            
            <?php
            $visitor='';
            $res5=$db->query("SELECT count(d_dealer_certificate_id) as total, upper(date_format(created_on,'%b')) as mnth, month(created_on) as mnth_num FROM `d_dealer_certificate` where year(created_on)='".date('Y')."' group by mnth order by mnth_num ");
                while($row5=$res5->fetch_assoc()){
                    $visitor.="['".$row5['mnth']."',".$row5['total']."],";
                }
                $visitor=trim($visitor,',');
            ?>
            

            var visitors = [
                <?php echo $visitor; ?>
            ];


            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"), [{
                        data: visitors,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#f89f9f']
                    }, {
                        data: visitors,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: "#f89f9f",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + '');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }
                    
});
</script>