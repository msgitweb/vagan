<?php
ob_start();
session_start();
include ("../dbconfig.php");
if(isset($_REQUEST['login_sub']))
{
	extract($_REQUEST);
	$user=$db->real_escape_string($username);
	$pwd=$db->real_escape_string($password);

	$res=$db->query("select * from techs_profile where email='".$email."' and id=1 ");
	$row=$res->fetch_assoc();
	$count=$res->num_rows;
	if($count>0)
	{

    $from_email='mail@demo.beema.co.in'; 
    $reply_to_email=$row['email']; 
    $sender_name='Techno Service';
    require '../Mailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.demo.beema.co.in';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'mail@demo.beema.co.in';                 // SMTP username
    $mail->Password = 'client@360';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to
    $mail->AddReplyTo($reply_to_email, $sender_name);
    $mail->setFrom($from_email, $sender_name);
    $mail->addAddress( $row['email'] );        
    $mail->isHTML(true);        
    $mail->Subject = "Password - Techno Service";
    $mail->Body    ="<p style='font-size: 12pt; font-family: Cambria;' >Hi,<br></p>
<p style='font-size: 12pt; font-family: Cambria;' >As per your request please check the below password. You can change it in your profile page</p>
<p style='font-size: 12pt; font-family: Cambria;' ><b>Password : </b>".$row['txt_password']."</p>";
    $mail->SMTPOptions = array(
      'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    );
  if($mail->send()){ $msg=2; }else{ $msg=3; }
	
	}else{
		$msg=1;
	}
	
}

?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Recover Password - TechnoService </title>
  <?php echo'<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/feather/style.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/custom.css">
  <!-- END VENDOR CSS-->
  
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
  
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/login-register.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
  <!-- END Custom CSS-->';?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 m-0">
              <div class="card-header no-border">
                <div class="card-title text-xs-center">
                  <div class="p-0">
                    <h4 style="font-weight: bold;">TechnoService</h4>
                  </div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                  <span>Please Enter your mail id here to receive password</span>
                </h6>
                <?php if($msg==1){  ?>
                <div class="alert bg-danger alert-icon-left alert-dismissible fade in mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>Oh snap!</strong> Please Enter Correct Mail ID
                </div>
                <?php } ?>

                <?php if($msg==2){  ?>
                <div class="alert bg-success alert-icon-left alert-dismissible fade in mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>Please check your mail...
                </div>
                <?php } ?>

                <?php if($msg==3){  ?>
                <div class="alert bg-danger alert-icon-left alert-dismissible fade in mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>Oh snap!</strong> Please try later...
                </div>
                <?php } ?>

              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <form class="form-horizontal form-simple" novalidate method="post">
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" class="form-control form-control-lg input-lg" name="email"  placeholder="Email ID" required="required"  />
                      <div class="form-control-position">
                        <i class="ft-mail"></i>
                      </div>
                    </fieldset>

                    <div class="text-xs-center text-md-right pull-right"><a href="index.php" class="card-link"><b>Login ?</b></a></div>

                    <button type="submit" name="login_sub" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Send</button>
                  </form>
                </div>
              </div>
              <div class="card-footer">
                
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <!-- BEGIN VENDOR JS-->
  <script src="../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <script src="../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"
  type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  
  <script src="../app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="../app-assets/js/core/app.min.js" type="text/javascript"></script>

  <!-- BEGIN PAGE LEVEL JS-->
  <script src="../app-assets/js/scripts/forms/form-login-register.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>