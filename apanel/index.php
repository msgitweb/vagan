<?php ob_start();
// session_start();
include ("../dbconfig.php");
if(isset($_SESSION['tech360_admin_user'])){
	header("location:dashboard.php");
}
if(isset($_REQUEST['login_sub']))
{
	extract($_REQUEST);
	$user=$db->real_escape_string($username);
	$pwd=$db->real_escape_string($password);

	$res=$db->query("select * from techs_profile where md5(user)='".md5($user)."' and pwd='".sha1($pwd)."' and status=1 ");
	$row=$res->fetch_assoc();
  // print_r($row['dealer_address']);die;
	$count=$res->num_rows;
	if($count>0)
	{
    $_SESSION["admin_address"] = $row['dealer_address'];
    $_SESSION["admin_id"] = $row['id'];
		if(isset($remember)){
		  setcookie('univ_username',$username,time()+365*24*60*60);
		  setcookie('univ_password',$password,time()+365*24*60*60);
		}
		$_SESSION['tech360_admin_user']=$row['id'];
		header("location:dashboard.php");
	}else{
		$msg=1;
	}
}

?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>Login Page - Vagansafety PVT LTD </title>
  <?php echo'<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/feather/style.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/icheck.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/custom.css">
  <!-- END VENDOR CSS-->
  
  <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/app.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
  
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/login-register.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
  <!-- END Custom CSS-->';?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page" style="background-image:url(img/cbgs.jpg)">
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="app-content1 content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-md-12 col-xs-12  box-shadow-2 p-0">
              <div class="col-md-8"></div>
            <div class="col-md-4" style="padding:0px;">
                
              <div class="login-details-list"> 
            <div class="card border-grey border-lighten-3 m-0">
              <div class="card-header no-border">
                <div class="card-title text-xs-center">
                  <div class="p-0">
                    <h4 style="font-weight: bold;">Vagansafety PVT LTD</h4>
                  </div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                  <span>Please Login Here</span>
                </h6>
                <?php if(@$msg==1){  ?>
                <div class="alert bg-danger alert-icon-left alert-dismissible fade in mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>Oh snap!</strong> Sorry Wrong Details
                </div>
                <?php } ?>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <form class="form-horizontal form-simple" novalidate method="post">
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" class="form-control form-control-lg input-lg" name="username" id="user-name" placeholder="Your Username"
                      required value="<?php echo @$_COOKIE['univ_username'] ?>" />
                      <div class="form-control-position">
                        <i class="ft-user"></i>
                      </div>
                    </fieldset>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="password" class="form-control form-control-lg input-lg" name="password" id="user-password"
                      placeholder="Enter Password" required value="<?php echo @$_COOKIE['univ_password'] ?>" />
                      <div class="form-control-position">
                        <i class="fa fa-key"></i>
                      </div>
                    </fieldset>
                    <!--<fieldset class="form-group row">-->
                    <!--  <div class="col-md-6 col-xs-12 text-xs-center text-md-left">-->
                    <!--    <fieldset>-->
                    <!--      <input type="checkbox" id="remember-me" name="remember" class="chk-remember">-->
                    <!--      <label for="remember-me"> Remember Me</label>-->
                    <!--    </fieldset>-->
                    <!--  </div>-->
                    <!--  <div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a href="recover_password.php" class="card-link">Forgot Password?</a></div>-->
                    <!--</fieldset>-->
                    <button type="submit" name="login_sub" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                  </form>
                </div>
              </div>
              </div> 
              
              
              <div class="card-footer">
                
              </div>
            </div>
            </div>  
            
            
            <!--<div class="col-md-6" style="padding:0px;">-->
            <!--    <div class="right-imggg">-->
            <!--        <h2>VECHICLE CONSPICUITY LABEL</h2>-->
            <!--    </div>-->
            <!--</div>-->
            
            
          </div>
        </section>
      </div>
    </div>
  </div>
  
  <!-- BEGIN VENDOR JS-->
  <script src="../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
  <script src="../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"
  type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  
  <script src="../app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="../app-assets/js/core/app.min.js" type="text/javascript"></script>

  <!-- BEGIN PAGE LEVEL JS-->
  <script src="../app-assets/js/scripts/forms/form-login-register.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
<link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet"> 
<style>
.line-on-side{margin:0px;}
.has-icon-left .form-control.input-lg{border-radius:20px;background:#f5f2f1; font-size:14px;}
.login-details-list{width:300px !important; }
.login-details-list .card {border-radius:10px !important;}
.card, .card-footer, .card-header{border-radius:10px !important;}
.box-shadow-2{box-shadow:none;}
.right-imggg { margin-top:-30px;
padding: 100px 10px 240px 10px; border:10px solid #072cb7; border-radius:0 50px 50px 0px;
 font-family: 'Russo One', sans-serif; } 
 
 .btn-primary{background:#552f84 !important; border:none;border-radius:20px; }
.card-footer{display:none;}
.right-imggg h2{ font-family: 'Russo One', sans-serif !important;font-size:35px;text-align: center; color: #fff;}

@media screen and (min-device-width: 320px) and (max-device-width: 480px) {  .card, .card-footer, .card-header{padding:0px !important; background:#fff !important;padding-top:30px !important;} }
</style>