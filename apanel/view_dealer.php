<?php $page_title="View Dealer"; include("header.php"); 



// if($_GET['p']==1){
//     $dealer_id=$_GET['dealer_id'];
  
//     $del=$db->query("delete from a_dealer where dealer_id='".$dealer_id."'");
//     if($del){
//         $result=success_alert('Dealer removed from the list successfully !');
        
//     } else {  $result=error_alert('Sorry. We cant remove this record !'); 
        
//     }
    
// }

if(isset($_POST['up'])){
  extract($_REQUEST);
  
 $cdate=date('Y-m-d H:i:s');
    $up=$db->query(" UPDATE `a_dealer` SET `name`='".$name."',`mobile`='".$mobile."',`confirm_password`='".$confirm_password."',`pwd`='".$pwd."',`address`='".$address."',`rto`='".$rto."',`updated_on`='$cdate',`status`='$status' WHERE dealer_id='".$dealer_id."' ");

   
if($up){
  $result=success_alert(" Dealer Updated Successfully... ");

} else {
  $result=error_alert(" Can't able to update this dealer... ");
}
    
  
}


?>
<div class="col-md-12">
    <?php echo $result;?>
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-primary bg-lighten-1 height-50">
                      <div class="card-header col-md-12">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff"> Dealer List</h4>  
                    <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="dashboard.php" title="New Order"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                    </ul>
                </div>

                        </div>
                    </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">Dealer Id</th>
                          <th width="20%">Dealer Name</th>
                          <!-- <th width="30%">Email</th> -->
                          <th width="10%">Phone</th>
                           <th width="20%">Dealer Address</th>
                          <th width="20%">RTO</th> 
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
             $cnt=0;
              $res01=$db->query(" SELECT * FROM `a_dealer` order by dealer_id desc ");
            while( $row01=$res01->fetch_assoc() ){

              $rto_res=$db->query("select * from `techs_rto` where rto_id='".$row01['rto']."'");
              $rto_row=$rto_res->fetch_assoc();
             ?>
                          <tr>
                              <!-- <td><?=++$cnt?></td> -->
                              <td> <?=$row01['dealer_id']?></td>
                                <td> <?=$row01['name']?></td>
                                 <td> <?=$row01['mobile']?></td>
                                  <td> <?=$row01['address']?></td>
                                  <td> <?=$rto_row['code']?>-<?=$rto_row['area']?></td>
                                 <!-- <td><a href="javascript:;"  onclick="chge(<?=$row01['dealer_id']?>)" class="btn btn-sm <?=($row01['status']==1)?'btn-primary':'btn-danger'?>"><?=($row01['status']==1)?'Active':'Inactive'?></a></td> -->
                              <td>
                                <a href="javascript:;" data-toggle="modal" class="btn btn-success btn-sm" onClick="get(<?=$row01['dealer_id']?>)"><i class="ft-edit"></i></a>
                                <!--<button class="btn btn-danger btn-sm" onClick="del(<?=$row01['dealer_id']?>)" ><i class="ft-trash"></i></button>-->
                                </td>
                            </tr>
                       <?php  } ?>
                    
                      </tbody>
                      <tfoot>
                        <tr>
                          <th width="5%">Dealer Id</th>
                          <th width="30%">Dealer Name</th>
                          <!-- <th width="30%">Email</th> -->
                          <th width="30%">Phone</th>
                           <th width="30%">Dealer Address</th>
                          <th width="30%">RTO</th> 
                          <th width="20%">Action</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>


            <div class="modal fade text-xs-left" id="result" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel10" align="center">Update Dealer</h4>
        </div>
          
            <div class="record_result"></div>
        
      </div>
    </div>
  </div>
<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">



<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.zero-configuration').DataTable();
  $(".select2").select2();
  $('#form').validate({     
           rules: { 
            name : { required: true, },
            email : { required: true, },
            mobile : { required: true, },
            user : { required: true, },
            pwd : { required: true, },
           
        },
        messages:{
             name : {  required: "Please Enter Name", }, 
             email : {  required: "Please Enter Email", }, 
             mobile : {  required: "Please Enter Mobile Number", }, 
             user : {  required: "Please Enter user Name", }, 
             pwd : {  required: "Please Enter Password", }, 
             
        } ,
  });

    

});

function del(dealer_id){
    $.confirm({
       theme: 'light',
       title: 'Alert !!!',
       content: 'Do you want to remove this record ?',
         buttons: {
            confirm: function () {
              window.location="?dealer_id="+dealer_id+"&p=1"; 
            },
            cancel: function () {
            },
         }
    });
  }
    
  function get(dealer_id){
    $("#result").modal("hide");
    $("body").loading('start');
    $.post('ajax/getdata.php',{
                dealer_id:dealer_id,con:1
              },function(resp){
                $(".record_result").html(resp);
              }
    );
    $("body").loading('stop'); $("#result").modal("show");
  }
</script>