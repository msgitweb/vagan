<?php $page_title="Update Certificate Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['up']==1){
    $result=success_alert('Data updated successfully !');
}
extract($_REQUEST);
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  
    $renewal_date=date('Y-m-d', strtotime($date. ' + 365 day'));
    
    $up=$db->query(" update `d_dealer_certificate` set rto='$rto',date='$date',vehicle_no='$vehicle_no',email='$email',chassis_no='$chassis_no',sld_make='$sld_make',engine_no='$engine_no',sld_no='$sld_no',make_of_vehicle='$make_of_vehicle',tac_no='$tac_no',model_of_vehicle='$model_of_vehicle',speed='$speed',owner_name='$owner_name',cop='$cop',contact='$contact',owner_address='$owner_name',dealer_address='$dealer_address',updated_on='$cdate',renewal_date='$renewal_date',vts_no='$vts_no',vts_make='$vts_make',type='$type'  where d_dealer_certificate_id='$d_dealer_certificate_id' ");
    

    if( $up ){ 
      unlink("../qrimage/".$qimg);
    //   $res01=$db->query(" SELECT b.area FROM `techs_speed_governor` a left join `techs_rto` b on a.rto_id=b.rto_id where a.id='$speed_id' ");
    //   $row01=$res01->fetch_assoc();
      $qrcode_link='RTO:'.$rto_row['code'].'-'.$rto_row['area'].', Vehicle No:'.$vehicle_no.', Vehicle Make:'.$make_of_vehicle.', SLD No:'.$sld_no.', SLD Make:'.$sld_make.', Validity:'.$renewal_date;
      include"qrcode.php";
      $db->query(" update d_dealer_certificate set qr_code='$qimage' where d_dealer_certificate_id='$d_dealer_certificate_id' ");
      header("location:?up=1&d_dealer_certificate_id=".$d_dealer_certificate_id);  

    }else{ $result=error_alert('Have some error...'); }


}

$d_dealer_certificate_id=$db->real_escape_string($d_dealer_certificate_id);
if( !is_numeric($d_dealer_certificate_id) ){ header("location:dashboard.php"); }
$vehicle_res=$db->query(" SELECT * FROM `d_dealer_certificate` where d_dealer_certificate_id='$d_dealer_certificate_id' ");
if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-warning bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">Update Certificate Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="view_certificate.php" title="View List"><i class="fa fa-arrow-right fa-lg text-white"></i></a></li>
                              </ul>
                          </div>                
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body" style="margin-left:200px">
                        <div class="row">
                        <div class="form-group col-md-4">
                          <label for="donationinput1">RTO<span class="manfield">*</span></label>
                          <select name="rto" class="select2" style="width: 100%">
                            <option value="">Please select the RTO</option>
                            <?php
                              $speed_res=$db->query(" SELECT * FROM `techs_rto`");
                              while( $speed_row=$speed_res->fetch_assoc() ){
                            ?>
                            
                             <option value="<?=$speed_row['rto_id']?>" <?=($speed_row['rto_id']==$vehicle_row['rto'])?'selected':''?> ><?=$speed_row['code']?>-<?=$speed_row['area']?></option>
                            
                            <?php } ?>
                          </select>
                        </div>
                       <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder=" Date" value="<?= $vehicle_row['date']?>" name="date"
                             />
                          </div>
                        </div>
 
                      </div>
                      <div class="row">
                       
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_no" value="<?php echo $vehicle_row['vehicle_no']?>"/>
                          </div>
                        </div>

                           <div class="form-group col-md-4">
                            <label for="donationinput1">Email</label>
                            <input type="text" class="form-control square" placeholder="Email" name="email" value="<?php echo $vehicle_row['email']?>"/>
                          </div>

                      
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chassis_no"  value="<?php echo $vehicle_row['chassis_no']?>"  />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">ENGINE NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No " name="engine_no"  value="<?php echo $vehicle_row['engine_no']?>"  />
                          </div>
                        </div>
                        
                       
                      </div>
                      
                      
                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">CONTACT NO<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="CONTACT NO " name="contact"  value="<?php echo $vehicle_row['contact']?>"   />
                          </div>
                        </div>
                        
                        <div class="col-md-4">

                        <label>Type</label>

                        <fieldset>

                          <label class="display-inline-block custom-control custom-checkbox">

                            <input name="type" class="custom-control-input payment_type sld_method" type="radio" value="1" <?php if($vehicle_row['type']==1){ "checked=checked";} else { }?>>

                            <span class="c-indicator bg-info custom-control-indicator"></span>

                            <span class="custom-control-description">SLD</span>

                          </label>

                          <label class="display-inline-block custom-control custom-checkbox">
                              
                            <input name="type" class="custom-control-input payment_type vts_method" type="radio" value="2" <?php if($vehicle_row['type']==2){ "checked=checked";} else { }?> >

                            <span class="c-indicator bg-warning custom-control-indicator"></span>

                            <span class="custom-control-description">VTS</span>

                          </label>

                          

                        </fieldset>

                      </div>
                        
                        
                      </div>
                      
                      <div  class="row sld_input">
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SLD NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="SLD No" value="<?php echo $vehicle_row['sld_no']?>" name="sld_no" 
                              />
                          </div>
                        </div>
                        
                         <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SLD Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="SLD Make" value="<?php echo $vehicle_row['sld_make']?>" name="sld_make" 
                              />
                          </div>
                        </div>
                      </div>
                      
                     <div class="row vts_input">
                         
                          <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">VTS NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="VTS No " value="<?php echo $vehicle_row['vts_no']?>" name="vts_no" 
                              />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">VTS Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="VTS Make " value="<?php echo $vehicle_row['vts_make']?>"  name="vts_make" 
                              />
                          </div>
                        </div>
                       
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MAKE OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MAKE OF VEHICLE " name="make_of_vehicle"  value="<?php echo $vehicle_row['make_of_vehicle']?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">TAC NO </label>
                            <input type="text" class="form-control square" placeholder="TAC No" value="<?php echo $vehicle_row['tac_no']?>" name="tac_no" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MODEL OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MODEL OF VEHICLE " name="model_of_vehicle"  value="<?php echo $vehicle_row['model_of_vehicle']?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SPEED</label>
                            <input type="text" class="form-control square" placeholder="SPEED" value="<?php echo $vehicle_row['speed']?>" name="speed" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">OWNER NAME<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="OWNER NAME " name="owner_name"  value="<?php echo $vehicle_row['owner_name']?>"  />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">COP</label>
                            <input type="text" class="form-control square" placeholder="COP" value="<?php echo $vehicle_row['cop']?>" name="cop" 
                              />
                          </div>
                        </div>
                      </div>
                       
                       
                      <div class="form-group">
                        <label for="donationinput1">OWNER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Owner Address" name="owner_address" ><?php echo $vehicle_row['owner_address']?></textarea>
                      </div>

                       <div class="form-group">
                        <label for="donationinput1">DEALER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="DEALER Address" name="dealer_address" disabled><?php echo $vehicle_row['dealer_address']?></textarea>
                      </div>             


                    
                       <div class="row">
                        <!-- <div class="col-md-6">
                          <div class="form-group">
                            <label for="donationinput1">QR CODE<span class="manfield">*</span></label>
                            <input type="file" class="form-control square" name="qr_code"  />
                          </div>
                        </div> -->
                        <!--<div class="col-md-6">-->
                        <!--  <div class="form-group">-->
                        <!--    <label for="donationinput1">VEHICLE PHOTO </label>-->
                        <!--    <input type="file" class="form-control square" name="vehicle_photo"  />-->
                        <!--  </div>-->
                        <!--</div>-->
                      </div>
                        


                      <input type="hidden" name="d_dealer_certificate_id" value="<?=$d_dealer_certificate_id?>">
                        <input type="hidden" name="qimg" value="<?=$vehicle_row['qr_code']?>">
                        <button type="submit" name="submit" class="btn btn-primary pull-right">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
          rules: { 
                rto:{ required: true, }, 
                date:{ required: true, },
                vehicle_no :{  required: true, },
               
                chassis_no : { required: true,  }, 
               
                engine_no:{ required: true, },
               
                make_of_vehicle:{ required: true, },
               
                model_of_vehicle:{ required: true, },
                owner_name:{ required: true },
                contact:{ required: true },
                owner_address:{ required: true },
                
               
                               
                
            },
            messages:{
                 rto : { required: "Please Select Speed Governor", }, 
                 date : { required: "Please Fill the Dtae",}, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                
                 chassis_no : { required: "Please enter Chiss No", }, 
                 
                 engine_no : { required: "Please Enter Engine No",},
                
                 make_of_vehicle : { required: "Please Enter Vehicle Make",},
                
                 model_of_vehicle : { required: "Please Enter Vehicle Model",},
                 owner_name : { required: "Please Enter Owner Name",}, 
                 contact:{ required: "Please enter  mobile no" },
                 owner_address:{ required: "Please enter owner address" },
                 
                
                 
            } ,
    });
  });
</script>

 <script>
  $(".vts_input").hide();
      $(".vts_method").click(function(){
      
          $(".vts_input").show('slow');
          $(".sld_input").hide();
      });
      
      $('.sld_method').click(function()
      {
         $('.sld_input').show('slow');
         $(".vts_input").hide('slow');
         
      });
  </script>