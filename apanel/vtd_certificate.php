<?php $page_title="New VTD Certificate Details"; include("header.php");
date_default_timezone_set('Asia/Kolkata');
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$print_trigger=0;
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  
  if($certificate_row['vtd_type']=='1'){
    $logo = 'v1.jpeg';   
    $company_name = 'Volty IoT Solutions Pvt ltd';
    $field_name = 'ICICD';
  }else{
    $logo = 'vamo.jpg';   
    $company_name = 'VAMO System Pvt ltd';
    $field_name = 'UID';
  }
  
    $certificate_number='CRF'.date('Y').rand(1000,99999);
    $renewal_date=date('Y-m-d', strtotime($date. ' + 365 day'));
    $up=$db->query(" insert into `d_dealer_certificate` set model_no='$model_no', logo='$logo', company_name='$company_name',field_name='$field_name', dealer_id='12345',rto='$rto',date='$date',vehicle_no='$vehicle_no',email='$email',chassis_no='$chassis_no',sld_make='$sld_make',engine_no='$engine_no',sld_no='$sld_no',make_of_vehicle='$make_of_vehicle',tac_no='$tac_no',model_of_vehicle='$model_of_vehicle',speed='$speed',owner_name='$owner_name',cop='$cop',contact='$contact',owner_address='$owner_address',dealer_address='$dealer_address',created_on='$cdate',renewal_date='$renewal_date',vts_make='$vts_make',vts_no='$vts_no',type='$type',software_version='$software_version',imei_number='$imei_number',network='$network',iccid_number='$iccid_number',vtd_type='$payment_type',invoice_number='$invoice_number',certificate_type='vtd',certificate_number='$certificate_number'");
    
    $d_dealer_certificate_id=$db->insert_id;
    
   $rto_res=$db->query("select * from techs_rto where rto_id='".$_POST['rto']."'");
   
   $rto_row=$rto_res->fetch_assoc();
  
  
  if($up){ 
    $rto_res1=$db->query("select * from a_dealer where dealer_id='12345'");
     $rto_row1=$rto_res1->fetch_assoc();
   $qrcode_link='RTO:'.$rto_row['code'].'-'.$rto_row['area'].',Vehicle No:'.$vehicle_no.',Engine No:'.$engine_no.',Chassis No:'.$chassis_no.',Vehicle Make:'.$make_of_vehicle.',Vehicle Model:'.$model_of_vehicle.',VTD No:'.$vts_no.', VTD Make:'.$vts_make.',  Expiry Date:'.$renewal_date.',Dealer Name:'.$rto_row1['name'].',Owner Name:'.$owner_name;
      include "qrcode.php";
      $db->query(" update d_dealer_certificate set qr_code='".$qimage."' where d_dealer_certificate_id='".$d_dealer_certificate_id."' ");
      
   
    // include_once(PATH."dealer_apanel/pdf/index.php");

    $result=success_alert('Certificate added successfully'); 

$print_trigger=1;
   
  }  else { $result=error_alert('Have some error'); }

}

?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
  form label{
    text-transform: capitalize;
    font-weight: bold;
  }
</style>
<div class="content-body ">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-blue bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Certificate Details</h4> 
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <!-- <li><a href="dashboard.php" title="View List"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li> -->
                                <?php if($up)  {
                              $certificate_res=$db->query("select * from d_dealer_certificate where dealer_id='12345' order by d_dealer_certificate_id desc limit 1"); 
                                 $certificate_row=$certificate_res->fetch_assoc();    
                                
                                ?>
                                
                                 <!--<li><a href="mail.php" title="Email"><i class="fa fa-envelope-o fa-lg text-white email"></i></a></li> -->
                                <li><a href="http://ammaenterprises.in/apanel/certifys.php/html.php?d_dealer_certificate_id=<?php echo $certificate_row['d_dealer_certificate_id']; ?>" class="print_trigger" target="_blank" title="Print"><i class="fa fa-print fa-lg text-white print"></i></a></li>
                            <?php } ?>
                              </ul>
                          </div>                
                        </div>
                       </div>
                       
                  <div class="card-block">
                     
                    <form class="form" method="post" id="form" enctype="multipart/form-data">
                      <div class="form-body" style="margin-left:200px">
                          
                          <div class="row">
                               <div class="col-md-5">
                        <label>Type</label>
                        <fieldset>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" checked=""
                            value="1">
                            <span class="c-indicator bg-info custom-control-indicator"></span>
                            <span class="custom-control-description">Volty</span>
                          </label>
                          <label class="display-inline-block custom-control custom-checkbox">
                            <input name="payment_type" class="custom-control-input payment_type" type="radio" value="2">
                            <span class="c-indicator bg-warning custom-control-indicator"></span>
                            <span class="custom-control-description">VAMOSYS</span>
                          </label>
                          
                        </fieldset>
                      </div>
                          </div>
                          
                          
                        <div class="row">
                        <div class="form-group col-md-4">
                          <label for="donationinput1">RTO<span class="manfield">*</span></label>
                          <select name="rto" class="select2" style="width: 100%">
                            <option value="">Please select the RTO</option>
                            <?php
                              $speed_res=$db->query(" SELECT * FROM `techs_rto`");
                              while( $speed_row=$speed_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$speed_row['rto_id'];?>" ><?=$speed_row['code'].'-'.$speed_row['area'];?></option>
                            <?php } ?>
                          </select>
                        </div>
                       <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Fitment Date <span class="manfield">*</span></label>
                            <input type="text" class="form-control square date" placeholder=" Date" name="date"
                             />
                          </div>
                        </div>
 
                      </div>
                      <div class="row">
                       
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Vehicle No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Vehicle No" name="vehicle_no" />
                          </div>
                        </div>
                            
                             <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Invoice No </label>
                            <input type="text" class="form-control square" placeholder="Invoice No" name="invoice_number" />
                          </div>
                        </div>
 
                          

                      
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Chassis No <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Chassis No" name="chassis_no"    />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">ENGINE NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="Engine No " name="engine_no"    />
                          </div>
                        </div>
                        
                        
                       
                      </div>
                      <div  class="row">
                          
                          <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">CONTACT NO<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="CONTACT NO " name="contact"    />
                          </div>
                        </div>
                        
                         <div class="form-group col-md-4">
                            <label for="donationinput1">Email</label>
                            <input type="text" class="form-control square" placeholder="Email" name="email"    />
                          </div>
                        
                  
                        
                      </div>
                      
                     
                      
                      <div class="row vts_input">
                         
                          <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">VTD NO <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="VTS No " name="vts_no" 
                              />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">VTD Make <span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="VTS Make " name="vts_make" 
                              />
                          </div>
                        </div>
                       
                      </div>
                      
                       <div class="row ">
                         
                          <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Software Version</label>
                            <input type="text" class="form-control square" placeholder="Software Version" name="software_version" 
                              />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">IMEI Number </label>
                            <input type="text" class="form-control square" placeholder="IMEI Number" name="imei_number" 
                              />
                          </div>
                        </div>
                       
                      </div>
                      
                      
                      <div class="row ">
                         
                          <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">Network</label>
                            <input type="text" class="form-control square" placeholder="Network" name="network" 
                              />
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">UID Number </label>
                            <input type="text" class="form-control square" placeholder="ICCID Number" name="iccid_number" 
                              />
                          </div>
                        </div>
                       
                      </div>
                      

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MAKE OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MAKE OF VEHICLE " name="make_of_vehicle"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">TAC NO </label>
                            <input type="text" class="form-control square" placeholder="TAC No " name="tac_no" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">MODEL OF VEHICLE<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="MODEL OF VEHICLE " name="model_of_vehicle"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">SET SPEED</label>
                            <input type="text" class="form-control square" placeholder="SPEED " name="speed" 
                              />
                          </div>
                        </div>
                      </div>

                      <div  class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">OWNER NAME<span class="manfield">*</span></label>
                            <input type="text" class="form-control square" placeholder="OWNER NAME " name="owner_name"    />
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="donationinput1">COP</label>
                            <input type="text" class="form-control square" placeholder="COP " name="cop" 
                              />
                          </div>
                        </div>
                      </div>
                       <div  class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="donationinput1">Model No</label>
                                <input type="text" class="form-control square" placeholder="Model No" name="model_no"    />
                              </div>
                            </div>
                      </div>
                       
                      <div class="form-group">
                        <label for="donationinput1">OWNER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="Owner Address" name="owner_address" ></textarea>
                      </div>
                         <?php
                         $dealer_rows=$db->query("select * from a_dealer where dealer_id='".$_SESSION['dealer_id']."'");
                         $dealer_ress=$dealer_rows->fetch_assoc();
                         ?>
                       <div class="form-group">
                        <label for="donationinput1">DEALER ADDRESS <span class="manfield">*</span></label>
    <textarea class="form-control square" placeholder="DEALER Address" name="dealer_address" ></textarea>
                      </div>             


                    


                        <button type="submit" name="submit" class="btn btn-primary pull-right ">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>



<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
<style>
</style>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".date").pickadate({format:'yyyy-mm-dd'});
      $(".select2").select2();
      $('#form').validate({     
           rules: { 
                rto:{ required: true, }, 
                date:{ required: true, },
                vehicle_no :{  required: true, },
                chassis_no : { required: true,  }, 
                engine_no:{ required: true, },
                make_of_vehicle:{ required: true, },
                model_of_vehicle:{ required: true, },
                owner_name:{ required: true },
                contact:{ required: true },
                owner_address:{ required: true },
                
                
            },
            messages:{
                 rto : { required: "Please Select Speed Governor", }, 
                 date : { required: "Please Fill the Dtae",}, 
                 vehicle_no : { required: "Please enter Vehicle No",},
                 chassis_no : { required: "Please enter Chiss No", }, 
                 engine_no : { required: "Please Enter Engine No",},
                 make_of_vehicle : { required: "Please Enter Vehicle Make",},
                 model_of_vehicle : { required: "Please Enter Vehicle Model",},
                 owner_name : { required: "Please Enter Owner Name",}, 
                 contact:{ required: "Please enter  mobile no" },
                 owner_address:{ required: "Please enter owner address" },
                
                
            } ,
    });
  });
</script>

<script>
// <?php if($print_trigger==1){ ?>
// $(".print_trigger").trigger('click');
// $(".print_trigger")[0].click();
// <?php } ?>



  $(".email").click(function(event) {
           
// alert();
$.post('ajax/mail.php',$("#form").serialize(), function(data, textStatus, xhr) {
     
  if($.trim(data)==1)
  {
      
    // $('#view_interestenquiry')[0].reset(); 
$(".email_result").html('<a href="javascript:;" class="alert alert-success" >Thank You Mail sended successfully</a>');
  }
   if($.trim(data)==0)
   {
$(".email_result").html('<a href="javascript:;" class="alert alert-danger" >Unable to send mail</a>');

   }
});

});

$(".print").click(function(){
$.post('../pdf/index.php',function(res){
});
});


  </script>
  
 
  