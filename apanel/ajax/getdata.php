<?php
ob_start();
session_start();
if(!isset($_SESSION['tech360_admin_user'])){
  header("location:index.php");
  exit();
}
include '../../dbconfig.php';
$con=$_REQUEST['con'];
$cdate=date('Y-m-d H:i:s');
?>
<?php
  if($con==1){
    extract($_POST);
    $res=$db->query("select * from a_dealer where dealer_id=$dealer_id");
    $row=$res->fetch_assoc();
?>
  
    <form class="form-horizontal" role="form" method="post" id="form1" enctype="multipart/form-data" autocomplete="off">
            <div class="modal-body">
 <div class="form-body">
    
<div class="form-group col-md-6" style="padding:0px;">
                          <label for="donationinput1">Dealer Name <span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Name" name="name"  value="<?php echo $row['name'];?>" />
                        </div>

                         

                        <div class="form-group col-md-6" >
                          <label for="donationinput1">Mobile <span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Mobile" name="mobile" value="<?php echo $row['mobile'];?>"  />
                        </div>

                    <div class="form-group">
      <label for="donationinput1">RTO <span class="manfield">*</span></label>
      <div class="rtid1">
        <select name="rto" class="select2 rto_id" style="width: 100%">
          <option value="">Please select RTO</option>
          <?php $res01=$db->query(" SELECT * FROM `techs_rto`   ");
           while( $row01=$res01->fetch_assoc() ){ ?>
          <option value="<?=$row01['rto_id']?>" <?php if($row01['rto_id']==$row['rto']){ echo 'selected="selected"'; } ?> ><?=$row01['code'].' - '.$row01['area']?></option>
          <?php } ?>
        </select>
      </div>
    </div>

                        <div class="form-group col-md-6" style="padding:0px;">
                          <label for="donationinput1">Password<span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Password Name" name="pwd" value="<?php echo $row['pwd'];?>"  />
                        </div>

                        <div class="form-group col-md-6" >
                          <label for="donationinput1">Confirm Password<span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Confirm Pasword" name="confirm_password" value="<?php echo $row['confirm_password'];?>"  />
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Address</label>
                          <input type="text" class="form-control square" placeholder="Address" name="address" value="<?php echo $row['address'];?>" />
                        </div>
     <div class="form-group">
                  <label for="timesheetinput2">Enable Status  ?</label>
                  <div class="position-relative">
                    <label class="display-inline-block custom-control custom-radio">
                      <input type="radio" name="status" class="custom-control-input" <?php if($row['status']==1){ echo "checked"; } ?>  value="1">
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Yes</span>
                    </label>
                    <label class="display-inline-block custom-control custom-radio">
                      <input type="radio" name="status"  class="custom-control-input" value="0" <?php if($row['status']==0){ echo "checked"; } ?> >
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">No</span>
                    </label>                    
                  </div>
                </div> 

    

   
 </div>
                                                   
  </div>
  
<div class="modal-footer">
  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-outline-danger" name="up">Save changes</button>
  <input type="hidden" name="dealer_id" value="<?php echo $row['dealer_id']; ?>"  />
</div>
      
</form>

 <script>
 $(document).ready(function(e) {
    $(".select2").select2();
     $('#form1').validate({     
           rules: { 
            name : { required: true, },
            
            mobile : { required: true, },
            confirm_password : { required: true, },
            pwd : { required: true, },
           
        },
        messages:{
             name : {  required: "Please Enter Name", }, 
             
             mobile : {  required: "Please Enter Mobile Number", }, 
             confirm_password : {  required: "Please Confirm your Password", }, 
             pwd : {  required: "Please Enter Password", }, 
             
        } ,
    });

     
        
});
</script>
<?php
  }
?>


<?php
  if($con==2){
    extract($_REQUEST);
    $res=$db->query(" select * from techs_rto where rto_id='$id' ");
    $row=$res->fetch_assoc();
?>
  
    <form class="form-horizontal" role="form" method="post" id="form1" enctype="multipart/form-data" autocomplete="off">
            <div class="modal-body">
 <div class="form-body">
    
    <div class="form-group">
      <label for="donationinput1">State</label>
      <select name="state_id" class="select2" style="width: 100%">
        <?php
          $state_res=$db->query(" SELECT * FROM `techs_state`  ");
          while( $state_row=$state_res->fetch_assoc() ){
        ?>
          <option value="<?=$state_row['state_id']?>" <?php if($state_row['state_id']==$row['state_id']){ echo 'selected="selected"'; } ?> ><?=$state_row['state']?></option>
        <?php } ?>
      </select>
    </div>
    
    <div class="form-group">
      <label for="donationinput1">Code</label>
      <input type="text" class="form-control square" placeholder="Code" name="code" value="<?=$row['code']?>"  />
    </div>

    <div class="form-group">
      <label for="donationinput1">Area</label>
      <input type="text" class="form-control square" placeholder="Area" name="area" value="<?=$row['area']?>"  />
    </div>

 </div>
                                                   
  </div>
  
<div class="modal-footer">
  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-outline-danger" name="up">Save changes</button>
  <input type="hidden" name="id" value="<?php echo $row['rto_id']; ?>"  />
</div>
      
</form>

 <script>
 $(document).ready(function(e) {
    $(".select2").select2();
     $('#form1').validate({     
           rules: { 
            title : { required: true, }, 
        },
        messages:{
             title : {  required: "Please Enter the Title", }, 
        } ,
    });
        
});
</script>
<?php
  }
?>

<?php
  if($con==3){
    extract($_REQUEST);
     $res01=$db->query(" SELECT * FROM `techs_rto` where state_id='$state_id'  ");
?>
    <select name="rto_id" class="select2 rto_id" style="width: 100%">
      <option value="">Please select RTO</option>
      <?php while( $row01=$res01->fetch_assoc() ){ ?>
      <option value="<?=$row01['rto_id']?>"><?=$row01['code'].' - '.$row01['area']?></option>
      <?php } ?>
    </select>
<?php
}
?>

<?php
if($con==4){
extract($_REQUEST);
$arr=array();
$vehicle_res=$db->query(" SELECT * FROM `a_dealer_credits` where dealer_id='$dealer_id' limit 5 ");
while($vehicle_row=$vehicle_res->fetch_assoc()){

// if($vehicle_res->num_rows>0 ){
$arr=array("type"=>$vehicle_row['type'],
  "credit_debit_count"=>$vehicle_row['credit_debit_count'],
  "created_on"=>$vehicle_row['created_on']
);
// } else {
//   $arr=array("type"=>0,
//   "credit_debit_count"=>0,
//   "created_on"=>0,
 
// );
// }
}
echo json_encode($arr);
}
?>

<?php
if($con==5){
extract($_REQUEST);
$arr=array();
$vehicle_res1=$db->query(" SELECT * FROM `a_dealer_credit_account` where dealer_id='$dealer_id' ");
$vehicle_row1=$vehicle_res1->fetch_assoc();


if($vehicle_res1->num_rows>0 ){
$arr=array("credit_balance"=>$vehicle_row1['credit_balance'],
  "total_credit"=>$vehicle_row1['total_credit'],
  "used_credit"=>$vehicle_row1['used_credit']
 
);
} else {
  $arr=array("credit_balance"=>0,
  "total_credit"=>0,
  "used_credit"=>0
  
);
}

echo json_encode($arr);
}
?>

<?php
  if($con==6){
    extract($_POST);
    $res=$db->query("select * from techs_speed_governor where id=$id");
    $row=$res->fetch_assoc();
?>
  
    <form class="form-horizontal" role="form" method="post" id="form1" enctype="multipart/form-data" autocomplete="off">
            <div class="modal-body">
 <div class="form-body">
    <div class="form-group">
      <label for="donationinput1">Title</label>
      <input type="text" class="form-control square" placeholder="Title" name="title" value="<?php echo $row['title'] ?>" />
    </div>
    <div class="form-group">
      <label for="donationinput1">State <span class="manfield">*</span></label>
      <select name="state_id" class="select2 state_id1" style="width: 100%">
        <option value="">Please select the State</option>
        <?php
          $state_res=$db->query(" SELECT * FROM `techs_state`  ");
          while( $state_row=$state_res->fetch_assoc() ){
        ?>
          <option value="<?=$state_row['state_id']?>" <?php if($state_row['state_id']==$row['state_id']){ echo 'selected="selected"'; } ?> ><?=$state_row['state']?></option>
        <?php } ?>
      </select>
    </div>

    <div class="form-group">
      <label for="donationinput1">RTO <span class="manfield">*</span></label>
      <div class="rtid1">
        <select name="rto_id" class="select2 rto_id" style="width: 100%">
          <option value="">Please select RTO</option>
          <?php $res01=$db->query(" SELECT * FROM `techs_rto` where state_id='".$row['state_id']."'  ");
           while( $row01=$res01->fetch_assoc() ){ ?>
          <option value="<?=$row01['rto_id']?>" <?php if($row01['rto_id']==$row['rto_id']){ echo 'selected="selected"'; } ?> ><?=$row01['code'].' - '.$row01['area']?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="donationinput1">Agency Name <span class="manfield">*</span></label>
      <input type="text" class="form-control square" placeholder="Agency Name" name="agency_name" value="<?php echo $row['agency_name'] ?>"  />
    </div>

    <div class="form-group">
      <label for="donationinput1">Agency Address <span class="manfield">*</span></label>
      <textarea class="form-control square" placeholder="Agency Address" name="agency_address"><?php echo $row['agency_address'] ?></textarea>
    </div>

    <div class="form-group">
      <label for="donationinput1">Agency Mobile No 1 <span class="manfield">*</span></label>
    <input type="text" class="form-control square" placeholder="Agency Mobile No 1" name="mobile01" value="<?php echo $row['mobile01'] ?>"  />
    </div>

    <div class="form-group">
      <label for="donationinput1">Agency Mobile No 2</label>
    <input type="text" class="form-control square" placeholder="Agency Mobile No 2" name="mobile02" value="<?php echo $row['mobile02'] ?>"  />
    </div>
 </div>
                                                   
  </div>
  
<div class="modal-footer">
  <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-outline-danger" name="up">Save changes</button>
  <input type="hidden" name="id" value="<?php echo $row['id']; ?>"  />
</div>
      
</form>

 <script>
 $(document).ready(function(e) {
    $(".select2").select2();
     $('#form1').validate({     
           rules: { 
            title : { required: true, },
            state_id : { required: true, },
            rto_id : { required: true, },
            agency_name : { required: true, },
            agency_address : { required: true, },
            mobile01 : { required: true, }, 
          },
          messages:{
               title : {  required: "Please Enter title", }, 
               state_id : {  required: "Please Select State", }, 
               rto_id : {  required: "Please Select RTO", }, 
               agency_name : {  required: "Please Enter Agency Name", }, 
               agency_address : {  required: "Please Enter Agency Address", }, 
               mobile01 : {  required: "Please Enter Mobile Number", }, 
          } ,
    });

     $(".state_id1").change(function(event) {
        $("body").loading('start');
        if( $(this).val()!='' ){
            $.post('ajax/getdata.php', {con: '3',state_id:$(this).val()}, function(resp) {
              $(".rtid1").html(resp); $(".select2").select2(); $("body").loading('stop');
            });
        }else{
          $(".rtid1").html('<select name="rto_id" class="select2 rto_id" style="width: 100%">'
                                +'<option value="">Please select RTO</option>'
                              +'</select>'); $(".select2").select2(); $("body").loading('stop');
        }
      });
        
});
</script>
<?php
  }
?>
