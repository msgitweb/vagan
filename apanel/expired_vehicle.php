<?php $page_title="Expired Vehicle List"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['ap']==1){
    $result=success_alert('Order removed from the list successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this record !');
}
// if($_GET['p']==1){
//     $order_id=$_GET['id'];
//     $del=$db->query(" delete from sg360_order where order_id='".$order_id."' ");
//     $db->query(" delete from `sg360_order_product` where order_id='".$order_id."' ");
//     if($del){ header("location:?ap=1"); }else{ header("location:?ak=1"); }
// }
?>

<div class="content-body">
			<?php echo $result; ?> 	
  <div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header bg-danger" style="height: 50px;">
                <h4 class="card-title text-white">View Expired Vehicle List</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="new_vehicle.php" title="New Order"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    
                    <div class="row" style="border: 1px solid  #7e7e7e; padding: 15px; border-radius: 4px">
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Vehicle No</label>
                              <input type="text" name="vehicle_no" class="form-control vehicle_no" placeholder="Vehicle No" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                              <label>Speed Governor</label>
                              <select name="speed_id" class="bs-select speed_id" style="width: 100%">
                                  <option value="">Select Speed Governor</option>
                                  <?php
                                  $order_status_res=$db->query(" SELECT * FROM `techs_speed_governor`  ");
                                  while( $order_status_row=$order_status_res->fetch_assoc() ){
                                  ?>
                                  <option value="<?=$order_status_row['id']; ?>"><?=$order_status_row['title']; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                              <label>Date</label>
                              <input type="text" name="odate" class="form-control date odate" placeholder="Date" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="javascript:;" class="btn btn-info filter" style="margin-top: 28%">Filter</a>
                        </div>
                    </div><br>
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="15%">Governor</th>
                          <th width="15%">Vehicle No</th>
                          <th width="10%">Serial No</th>
                          <th width="10%">Renewal Date</th>
                          <th width="20%">Vehicle Owner</th>
                          <!-- <th width="12%">Vehicle Owner Contact</th> -->
                          <th width="12%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                    
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>SNo</th>
                          <th>Governor</th>
                          <th>Vehicle No</th>
                          <th>Serial No</th>
                          <th>Renewal Date</th>
                          <th>Vehicle Owner</th>
                          <!-- <th>Vehicle Owner Contact</th> -->
                          <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>

                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<link href="https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css" rel="stylesheet" />
<script type="text/javascript">
jQuery(document).ready(function($) { 
    $('.bs-select').select2({});
    $(".date").pickadate({format:'yyyy-mm-dd'});
    $('.zero-configuration').DataTable({
        "ajax": 'table_ajax.php?con=1&rrtype=2',
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page  
        
    });

    $(".filter").click(function(event) {

        $('.zero-configuration').DataTable({
          destroy:true,
          "ajax": 'table_ajax.php?con=1&rrtype=2&vehicle_no='+$(".vehicle_no").val()+'&odate='+$(".odate").val()+'&speed_id='+$(".speed_id").val(),
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
          "pageLength": 10, // default record count per page
        });
    });

    $('input[name=\'vehicle_no\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'autocomplete.php?filter_name='+encodeURIComponent(request.term)+'&con=1',
          dataType: 'json',
          success: function(json) {
            json.unshift({
              vehicle_id: 0,
              name: '--None--'
            });
            response($.map(json, function(item) {
              return {
                label: item.name,
                value: item.vehicle_id
              }
            }));
          }
        });
      },
      'select': function(event, ui) {
        setTimeout(function(){ $('input[name=\'vehicle_no\']').val(ui.item.label); },100);
        $('input[name=\'vehicle_id\']').val(ui.item.value);
      }
    });

});
function del(id){
        $.confirm({
             theme: 'light',
             title: 'Alert !!!',
             content: 'Do you want to remove this record ?',
                 buttons: {
                        confirm: function () {
                            window.location="?id="+id+"&p=1";   
                        },
                        cancel: function () {
                        },
                 }
        });
}
</script>