<?php
//include"../dbconfig.php";
$vehicle_res=$db->query(" SELECT * FROM `techs_vehicle` where vehicle_id='$vehicle_id' ");
$vehicle_row=$vehicle_res->fetch_assoc();
$res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");
$row01=$res01->fetch_assoc();

$rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$row01['rto_id']."' ");
$rto_row01=$rto_res01->fetch_assoc();
?>
<body style="font-family: 'Open Sans',-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif;
      font-size: 1rem;
      line-height: 1.45;
      color: #404E67;">
<table class="table tab1" style="max-width: 100%;border-collapse: collapse;background-color: transparent;">
    <tr>
      <th colspan="4" style="text-align: center; background: #a0a0ff; font-size: 18px; color: #fff; border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;">Techno Service -  Vehicle Details</th>
    </tr>
    <tr style="background: #f2f2f2;">
        <th width="20%" style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;" >RTO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$rto_row01['code'].' '.$rto_row01['area']?></td>
        <th width="20%" style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">DATE</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=date('d-M Y',strtotime($vehicle_row['created_on']));?></td>
    </tr>
    <tr>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">VEHICLE NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['vehicle_no']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">SPEED GOVERNOR MAKE</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$row01['title']?></td>
    </tr>
    <tr style="background: #f2f2f2;">
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">CHISS NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['chiss_no']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">SL NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['sl_no']?></td>
    </tr>
    <tr>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">ENGINE NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['engine_no']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">REF NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['ref_no']?></td>
    </tr>
    <tr style="background: #f2f2f2;">
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">VEHICLE MAKE</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['vehicle_make']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">INVOICE NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['invoice_no']?></td>
    </tr>
    <tr>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">VEHICLE MODEL</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['vehicle_model']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">SET SPEED</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['speed']?></td>
    </tr>
    <tr style="background: #f2f2f2;">
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">VEHICLE OWNER NAME & ADDRESS</th>
        <td style="word-wrap: normal;width: 400px;border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['vehicle_address']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">DEALER NAME & ADDRESS</th>
        <td style="word-wrap: normal;width: 400px;border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['dealer_name'].', '.$vehicle_row['dealer_address'].', '.$vehicle_row['dealer_mobile']?></td>
    </tr>
    <tr>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">OWNER PHONE NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['mobile_num01']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">ELECTRONIC SPEED GOVERNER FITTED DATE</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=date('d-M Y',strtotime($vehicle_row['go_date']));?></td>
    </tr>
    <tr style="background: #f2f2f2;">
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">ELECTRONIC SPEED GOVERNER SERIAL NO</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=$vehicle_row['sl_no']?></td>
        <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">ELECTRONIC SPEED GOVERNER RENEWAL DATE</th>
        <td style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;"><?=date('d-M Y',strtotime($vehicle_row['renewal_date']));?></td>
    </tr>
</table>

<table class="table" style="width: 100%;border-collapse: collapse;background-color: transparent;">
    <tr>
      <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">Speed Governor Photo</th>
      <th style=" border-top: table-borderless;padding: .75rem 2rem;border-bottom: 1px solid #E3EBF3;padding: .75rem; vertical-align: top;border-top: 1px solid #98A4B8;text-align: left; font-size: 14px;">Vehicle Photo</th>
    </tr>
    <tr>
        <td><img src="<?='http://'.$_SERVER['HTTP_HOST'].'/technoservice/document/'.$vehicle_row['governor_photo']?>" class="img-thumbnail" 
          style="width: 250px; height: 150px" alt="Speed Governor Photo" ></td>
        <td><img src="<?='http://'.$_SERVER['HTTP_HOST'].'/technoservice/document/'.$vehicle_row['vehicle_photo']?>" class="img-thumbnail"
        style="width: 250px; height: 150px" alt="Vehicle Photo" ></td>
    </tr>
    <tr>
        <td><img src="<?='http://'.$_SERVER['HTTP_HOST'].'/technoservice/qrimage/'.$vehicle_row['qrimage']?>" class="img-thumbnail" 
          alt="QR Code" ></td>
        <td></td>
    </tr>
</table>
</body>