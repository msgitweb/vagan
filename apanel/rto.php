<?php $page_title="RTO Office Details"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['ap']==1){
    $result=success_alert('Data removed from list successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this data !');
}
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  $area=trim($db->real_escape_string($area),' ');
  $code=trim($db->real_escape_string($code),' ');
  $res01=$db->query(" SELECT * FROM `techs_rto` where state_id='$state_id' and code='$code' and area='$area' ");
  if( $res01->num_rows==0 ){
    $db->query(" insert into `techs_rto`(state_id,code,area)values('$state_id','$code','$area') "); 
    $result=success_alert('Data added successfully !');
  }else{
     $result=error_alert('Sorry.already exist !');
  }

}

if($_GET['p']==1){
  $del=$db->query("delete from `techs_rto` where rto_id='".$_GET['id']."'");
  if($del){
    header("location:?ap=1");   
  }else{
    header("location:?ak=1");     
  }
}

if(isset($_POST['up'])){
  extract($_REQUEST);
  $area=trim($db->real_escape_string($area),' ');
  $code=trim($db->real_escape_string($code),' ');
  $res01=$db->query(" select rto_id from `techs_rto` where state_id='$state_id' and code='$code' and area='$area' and rto_id!='$id' ");
  if( $res01->num_rows==0 ){ 
    $up=$db->query("update `techs_rto` set state_id='".$state_id."',code='$code',area='$area' where rto_id='".$id."'");
    $result=success_alert(" Data Updated Successfully... ");
  }else{
  $result=error_alert(" Data Already Exists. Please check the list ");  
  }
}

?>

<div class="content-body">
			<?php echo $result; ?> 	

<div class="row">
        
          <div class="col-md-5">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-warning bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Record</h4>                  
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" autocomplete="off">
                      <div class="form-body">

                        <div class="form-group">
                          <label for="donationinput1">State</label>
                          <select name="state_id" class="select2" style="width: 100%">
                            <?php
                              $state_res=$db->query(" SELECT * FROM `techs_state`  ");
                              while( $state_row=$state_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$state_row['state_id']?>"><?=$state_row['state']?></option>
                            <?php } ?>
                          </select>
                        </div>
                        
                        <div class="form-group">
                          <label for="donationinput1">Code</label>
                          <input type="text" class="form-control square" placeholder="Code" name="code"  />
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Area</label>
                          <input type="text" class="form-control square" placeholder="Area" name="area"  />
                        </div>

                        <button type="submit" name="submit" class="btn btn-primary">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                <div class="col-md-7">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-warning bg-lighten-1 height-50">
                      <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">List Of Records</h4>                  
                        </div>
                    </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="30%">State</th>
                          <th width="30%">Code</th>
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                    
                      </tbody>
                    </table>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
    </div>


</div>

 <div class="modal fade text-xs-left" id="result" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel10" align="center">Update Record</h4>
        </div>
          
            <div class="record_result"></div>
        
      </div>
    </div>
  </div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">



<script type="text/javascript">
jQuery(document).ready(function($) {
  $(".select2").select2();
  $('.zero-configuration').DataTable({ ajax:'table_ajax.php?con=2'});
  $('#form').validate({     
           rules: { 
            code : { required: true, }, 
            area : { required: true, }, 
        },
        messages:{
             code : {  required: "Please Enter the code", }, 
             area : {  required: "Please Enter the area", }, 
        } ,
    });

});

function del(id){
    $.confirm({
       theme: 'light',
       title: 'Alert !!!',
       content: 'Do you want to remove this record ?',
         buttons: {
            confirm: function () {
              window.location="?id="+id+"&p=1"; 
            },
            cancel: function () {
            },
         }
    });
  }
    
  function get(id){
    $("#result").modal("hide");
    $("body").loading('start');
    $.post('ajax/getdata.php',{
                id:id,con:2
              },function(resp){
                $(".record_result").html(resp);
              }
    );
    $("body").loading('stop'); $("#result").modal("show");
  }
</script>