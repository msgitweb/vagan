<?php $page_title="Certificate List"; include("header.php");
$cdate=date('Y-m-d H:i:s');

?>

<div class="content-body">
			<?php echo @$result; ?> 	
  <div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header bg-warning" style="height: 50px;background:#000 !important;">
                <h4 class="card-title text-white">View Certificate List</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="certificate.php" title="New Certificate"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    
                     <div class="row" style="padding: 15px; background:#fff;">
                         
                         <div class="col-md-3">
                            <div class="form-group">
                              <label>Dealer Name</label>
                             <select name="dealer_id" class="bs-select dealer_id" style="width: 100%">
                                  <option value="">Select Dealer</option>
                                  <?php
                                //   $order_status_res=$db->query(" SELECT * FROM `d_dealer_certificate` ");
                                //   while( $order_status_row=$order_status_res->fetch_assoc() ){
                                      
                                  $order_status_res1=$db->query(" SELECT * FROM `a_dealer` ");
                                  while($order_status_row1=$order_status_res1->fetch_assoc()){
                                   
                                  ?>
                                  <option value="<?=$order_status_row1['dealer_id']; ?>"><?=$order_status_row1['name']; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                         
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Vehicle No</label>
                              <input type="text" name="vehicle_no" class="form-control vehicle_no" placeholder="Vehicle No" />
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label>RTO</label>
                              <select name="rto" class="bs-select rto" style="width: 100%">
                                  <option value="">Select RTO</option>
                                  <?php
                                //   $order_status_res=$db->query(" SELECT * FROM `d_dealer_certificate`  ");
                                //   while( $order_status_row=$order_status_res->fetch_assoc() ){
                                    $res01=$db->query(" SELECT * FROM `techs_rto` ");
                                    while($row01=$res01->fetch_assoc()){
                                  ?>
                                  <option value="<?=$row01['rto_id']; ?>"><?=$row01['code']; ?>-<?=$row01['area']; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Date</label>
                              <input type="text" name="date" class="form-control date " placeholder="Date" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="javascript:;" class="btn btn-info filter" style="margin-top: 28%">Filter</a>
                        </div>
                    </div><br> 
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="15%">Dealer Name</th>
                          <th width="15%">Owner Name</th>
                           <th width="15%">RTO</th>
                          <th width="10%">Renewal Date</th>
                           <th width="10%">Date</th>
                          <th width="10%">Status</th>
                          <th width="12%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      </tbody>
                      <tfoot>
                        <tr>
                         <th width="5%">SNo</th>
                          <th width="15%">Dealer Name</th>
                          <th width="15%">Owner Name</th>
                           <th width="15%">RTO</th>
                          <th width="10%">Renewal Date</th>
                           <th width="10%">Date</th>
                          <th width="10%">Status</th>
                          <th width="12%">Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>

 <div class="modal fade text-xs-left" id="result" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel10" align="center">View Certificate Details</h4>
        </div>
          
            <div class="record_result"></div>
        
      </div>
    </div>
  </div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<link href="https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css" rel="stylesheet" />
 <script type="text/javascript">
jQuery(document).ready(function($) { 
    $('.bs-select').select2({});
    $(".date").pickadate({format:'yyyy-mm-dd'});
    $('.zero-configuration').DataTable({
        "ajax": 'table_ajax.php?con=3&rrtype=0',
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page  
        
    });

    $(".filter").click(function(event) {

        $('.zero-configuration').DataTable({
          destroy:true,
          "ajax": 'table_ajax.php?con=3&rrtype=0&vehicle_no='+$(".vehicle_no").val()+'&date='+$(".date").val()+'&rto='+$(".rto").val()+'&dealer_id='+$(".dealer_id").val(),
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
          "pageLength": 10, // default record count per page
        });
    });

    // $('input[name=\'dealer_name\']').autocomplete({
    //   'source': function(request, response) {
    //     $.ajax({
    //       url: 'autocomplete.php?filter_name='+encodeURIComponent(request.term)+'&con=1',
    //       dataType: 'json',
    //       success: function(json) {
    //         json.unshift({
    //           vehicle_id: 0,
    //           name: '--None--'
    //         });
    //         response($.map(json, function(item) {
    //           return {
    //             label: item.name,
    //             value: item.vehicle_id
    //           }
    //         }));
    //       }
    //     });
    //   },
    //   'select': function(event, ui) {
    //     setTimeout(function(){ $('input[name=\'dealer_name\']').val(ui.item.label); },100);
    //     $('input[name=\'dealer_name\']').val(ui.item.value);
    //   }
    // });

});
// function del(id){
//         $.confirm({
//              theme: 'light',
//              title: 'Alert !!!',
//              content: 'Do you want to remove this record ?',
//                  buttons: {
//                         confirm: function () {
//                             window.location="?id="+id+"&p=1";   
//                         },
//                         cancel: function () {
//                         },
//                  }
//         });
// }

//   function get(d_dealer_certificate_id){
//     $("#result").modal("hide");
//     $("body").loading('start');
//     $.post('ajax/getdata.php',{
//                 d_dealer_certificate_id:d_dealer_certificate_id,con:4
//               },function(resp){
//                 $(".record_result").html(resp);
//               }
//     );
//     $("body").loading('stop'); $("#result").modal("show");
//   }
</script> 
<style>
    .btn-primary{background:#000 !important;}
.btn-info{background:#000 !important;}
</style>