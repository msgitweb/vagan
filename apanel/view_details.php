<?php $page_title="Vehicle Dealer Credits"; include("header.php");
$cdate=date('Y-m-d H:i:s');
extract($_REQUEST);
$dealer_credits_id=$db->real_escape_string($dealer_credits_id);
if( !is_numeric($dealer_credits_id) ){ header("location:dashboard.php"); }
$vehicle_res=$db->query(" SELECT * FROM `a_dealer_credits` where dealer_credits_id='$dealer_credits_id' ");
if( $vehicle_res->num_rows==0 ){ header("location:dashboard.php"); }
$vehicle_row=$vehicle_res->fetch_assoc();
// $res01=$db->query(" SELECT * FROM `techs_speed_governor` where id='".$vehicle_row['speed_id']."' ");
// $row01=$res01->fetch_assoc();

// $rto_res01=$db->query(" SELECT * FROM `techs_rto` where rto_id='".$vehicle_row['speed_id']."' ");
// $rto_row01=$rto_res01->fetch_assoc();
?>
<style type="text/css">
  .tab1 tr:nth-child(even){ background: #f2f2f2; }
  .tab1 td:hover{ background: #2f93f7;font-weight: bold;color:#fff  }
</style>
<div class="content-body">
<div class="row">
        
          <div class="col-md-12">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-lighten-1 height-50">
                        <div class="card-header" style="background:  #575757; height: 50px">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">Dealer Credit Details</h4> 
                          <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                          <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                <li><a href="view_dealer_credit.php" title="View List"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                                <!-- <li><a href="print_details.php?vddid=<?=$vehicle_id;?>&token=<?=$vehicle_row['token'] ?>" target="_blank" title="Print"><i class="fa fa-print fa-lg text-white"></i></a></li> -->
                              </ul>
                          </div>                
                        </div>
                       </div>
                  <div class="card-block">
                    
                      <table class="table tab1">

                        <tr>
                              <th width="20%">Dealer Name</th>
                              <td><?=$vehicle_row['dealer_name']?></td>
                              
                            </tr>
                            <tr>
                              <th width="20%">Dealer Mobile</th>
                              <td><?=$vehicle_row['dealer_mobile'];?></td>
                          </tr>
                          <tr>
                              <th width="20%">Reference Number</th>
                              <td><?=$vehicle_row['reference_number']?></td>
                            </tr>
                            <tr>
                              <th width="20%">DATE</th>
                              <td><?=date('d-M Y',strtotime($vehicle_row['created_on']));?></td>
                          </tr>
                          <tr>
                              <th>Credits</th>
                              <td><?=$vehicle_row['credit_debit_count']?></td>
                            </tr>
                            <tr>
                              <th>Entry By</th>
                              <td> <?=$vehicle_row['entry_by']?></td>
                          </tr>
                          <tr>
                              <th>Notes</th>
                              <td><?=$vehicle_row['notes']?></td>
                              
                          </tr>
                         
                      </table>

                      

                  </div>
                </div>
              </div>
            </div>
            
                
    </div>


</div>

<?php include("footer.php") ?>