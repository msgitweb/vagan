<?php $page_title="Dealer Credits List"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['ap']==1){
    $result=success_alert('Dealer Credits removed from the list successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this record !');
}
if($_GET['p']==1){
    $dealer_credits_id=$_GET['dealer_credits_id'];
    
      $del=$db->query(" delete from a_dealer_credits where dealer_credits_id='".$dealer_credits_id."' and approval_status=0");
    
    if($del){
     header("location:?ap=1"); 
   } else { 
    header("location:?ak=1");
    }
}

/*approve status*/

if( $_GET['approve'] == 1){
   $dealer_credits_id=$db->real_escape_string($_GET['dealer_credits_id']);
  $allow=0;

 $credit_res=$db->query("select * from a_dealer_credits where dealer_credits_id='$dealer_credits_id' and approval_status=0");
 $credit_res_row=$credit_res->fetch_assoc();
$credit=$credit_res_row['credit_debit_count'];
 if($credit_res->num_rows > 0){

    /*credit*/
    if($credit_res_row['type'] == 1){

  $dealer_account=$db->query("select * from a_dealer_credit_account where dealer_id='".$credit_res_row['dealer_id']."'");
      if($dealer_account->num_rows ==0){
      /*if account not availble for dealer creat*/
      $db->query("insert into a_dealer_credit_account set dealer_id='".$credit_res_row['dealer_id']."',
        credit_balance='$credit',total_credit='$credit',created_on='$cdate'");

      }

      if($dealer_account->num_rows >0){
      /*update dealer account*/
        $db->query("update  a_dealer_credit_account set 
        credit_balance=credit_balance+$credit,total_credit=total_credit+$credit
        where dealer_id='".$credit_res_row['dealer_id']."'");

      }
    }

    /*debit*/
    if($credit_res_row['type'] == 2){
    
    $dealer_account=$db->query("select * from a_dealer_credit_account where dealer_id='".$credit_res_row['dealer_id']."' and credit_balance>=$credit");
     

      if($dealer_account->num_rows >0){
      /*update dealer account*/
        $db->query("update  a_dealer_credit_account set 
        credit_balance=credit_balance-$credit,total_credit=total_credit-$credit
        where dealer_id='".$credit_res_row['dealer_id']."'");

      } else {
     ++$allow;
      $result=error_alert("Insufficient Credit Balnace");

      }

    }
 
     if($allow == 0){

    $up=$db->query("update a_dealer_credits set approval_status=1 where dealer_credits_id='".$dealer_credits_id."'");
    if($up){
  $result=success_alert("Approved Successfully");
     } else {

   $result=error_alert("Can't Approve this Credit");
     }
   }
 } else {
 $result=error_alert("This Credit already Approved");

 }
 
}

?>

<div class="content-body">
			<?php echo $result; ?> 	
  <div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header bg-blue" style="height: 50px;background:#030b18 !important">
                <h4 class="card-title text-white">View Renewal History List</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a href="new_dealer_credits.php" title="New Order"><i class="fa fa-arrow-left fa-lg text-white"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    
                    <div class="row" style="border: 1px solid  #7e7e7e; padding: 15px; border-radius: 4px">
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Entry By</label>
                              <input type="text" name="entry_by" class="form-control entry_by" placeholder="Entry By" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                              <label>Dealer Name</label>
                              <select name="dealer_id" class="bs-select dealer_id" style="width: 100%">
                                  <option value="">Select Your Dealer</option>
                                  <?php
                                  $order_status_res=$db->query(" SELECT * FROM `a_dealer_credits`  ");
                                  while( $order_status_row=$order_status_res->fetch_assoc() ){
                                    $dealer=$db->query("select * from a_dealer where dealer_id='".$order_status_row['dealer_id']."'");
                                    $dealer_res=$dealer->fetch_assoc();
                                  ?>
                                   <option value="<?php echo $order_status_row['dealer_id'];?>"><?php echo $dealer_res['name']; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                              <label>Date</label>
                              <input type="text" name="date" class="form-control date odate" placeholder="Date" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="javascript:;" class="btn btn-info filter" style="margin-top: 28%">Filter</a>
                        </div>
                    </div><br>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="15%">Reference Number</th>
                           <th width="15%">Dealer Name</th>
                            <th width="15%">Credit Type</th>
                          <th width="10%">Credits</th>
                          <th width="15%">Date</th>
                          
                          
                         
                          <!-- <th width="12%">Vehicle Owner Contact</th> -->
                          <th width="12%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                    
                      </tbody>
                      <tfoot>
                        <tr>
                         <th width="5%">SNo</th>
                          <th width="15%">Reference Number</th>
                           <th width="15%">Dealer Name</th>
                            <th width="15%">Credit Type</th>
                          <th width="10%">Credits</th>
                          <th width="15%">Date</th>
                         
                          <!-- <th>Vehicle Owner Contact</th> -->
                          <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<script src="../app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<link href="https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css" rel="stylesheet" />
<script type="text/javascript">
jQuery(document).ready(function($) { 

  $(document).on('click','.approvel_credit',function(){
    dealer_credits_id = $(this).attr('data-dealer_credits_id');

    $.confirm({
             theme: 'light',
             title: 'Alert !!!',
             content: 'Do you want to Approve this Credit ?',
                 buttons: {
                        confirm: function () {
                            window.location="?dealer_credits_id="+dealer_credits_id+"&approve=1";   
                        },
                        cancel: function () {
                        },
                 }
        });
  })
    $('.bs-select').select2({});
    $(".date").pickadate({format:'yyyy-mm-dd'});
    $('.zero-configuration').DataTable({
        "ajax": 'table_ajax.php?con=1&rrtype=0',
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page  
        
    });

    $(".filter").click(function(event) {

        $('.zero-configuration').DataTable({
          destroy:true,
          "ajax": 'table_ajax.php?con=1&rrtype=0&entry_by='+$(".entry_by").val()+'&odate='+$(".odate").val()+'&dealer_id='+$(".dealer_id").val(),
         "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
          "pageLength": 10, // default record count per page
        });
    });

   

});

function del(dealer_credits_id){
        $.confirm({
             theme: 'light',
             title: 'Alert !!!',
             content: 'Do you want to remove this record ?',
                 buttons: {
                        confirm: function () {
                            window.location="?dealer_credits_id="+dealer_credits_id+"&p=1";   
                        },
                        cancel: function () {
                        },
                 }
        });
}
</script>