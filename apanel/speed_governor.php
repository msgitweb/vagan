<?php $page_title="Speed Governor"; include("header.php");
$cdate=date('Y-m-d H:i:s');
if($_GET['ap']==1){
    $result=success_alert('Data removed from list successfully !');
}
if($_GET['ak']==1){
    $result=error_alert('Sorry. We cant remove this data !');
}
if(isset($_REQUEST['submit'])){
  extract($_REQUEST);
  $title=trim($db->real_escape_string($title),' ');
  $agency_name=trim($db->real_escape_string($agency_name),' ');
  $agency_address=trim($db->real_escape_string($agency_address),' ');
  $res01=$db->query(" SELECT * FROM `techs_speed_governor` where title='$title' ");
  if( $res01->num_rows==0 ){
    $db->query(" insert into `techs_speed_governor`(title,state_id,rto_id,agency_name,agency_address,mobile01,mobile02,created_on,updated_on)values('$title','$state_id','$rto_id','$agency_name','$agency_address','$mobile01','$mobile02','$cdate','$cdate') "); 
    $result=success_alert('Data added successfully !');
  }else{
     $result=error_alert('Sorry.already exist !');
  }

}

if($_GET['p']==1){
  $del=$db->query("delete from `techs_speed_governor` where id='".$_GET['id']."'");
  if($del){
    header("location:?ap=1");   
  }else{
    header("location:?ak=1");     
  }
}

if(isset($_POST['up'])){
  extract($_REQUEST);
  $title=trim($db->real_escape_string($title),' ');
  $agency_name=trim($db->real_escape_string($agency_name),' ');
  $agency_address=trim($db->real_escape_string($agency_address),' ');
  $res01=$db->query(" select id from `techs_speed_governor` where title='$title' and id!='$id' ");
  if( $res01->num_rows==0 ){ 
    $up=$db->query(" update `techs_speed_governor` set title='".$title."',state_id='$state_id',rto_id='$rto_id',
      agency_name='$agency_name',agency_address='$agency_address',mobile01='$mobile01',mobile02='$mobile02',updated_on='$cdate' 
      where id='".$id."' ");
    $result=success_alert(" Data Updated Successfully... ");
  }else{
    $result=error_alert(" Data Already Exists. Please check the list ");  
  }
}

?>
<style type="text/css">
  .manfield{ color:  #fa5635;  }
</style>
<div class="content-body">
      <?php echo $result; ?>  

<div class="row">
        
          <div class="col-md-5">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-primary bg-lighten-1 height-50">
                        <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">New Record</h4>                  
                        </div>
                       </div>
                  <div class="card-block">
                    <form class="form" method="post" id="form" autocomplete="off">
                      <div class="form-body">
                        
                        <div class="form-group">
                          <label for="donationinput1">Title <span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Title" name="title"  />
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">State <span class="manfield">*</span></label>
                          <select name="state_id" class="select2 state_id" style="width: 100%">
                            <option value="">Please select the State</option>
                            <?php
                              $state_res=$db->query(" SELECT * FROM `techs_state`  ");
                              while( $state_row=$state_res->fetch_assoc() ){
                            ?>
                              <option value="<?=$state_row['state_id']?>"><?=$state_row['state']?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">RTO <span class="manfield">*</span></label>
                          <div class="rtid">
                            <select name="rto_id" class="select2 rto_id" style="width: 100%">
                              <option value="">Please select RTO</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Agency Name <span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Agency Name" name="agency_name"  />
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Agency Address <span class="manfield">*</span></label>
                          <textarea class="form-control square" placeholder="Agency Address" name="agency_address"></textarea>
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Agency Mobile No 1 <span class="manfield">*</span></label>
                          <input type="text" class="form-control square" placeholder="Agency Mobile No 1" name="mobile01"  />
                        </div>

                        <div class="form-group">
                          <label for="donationinput1">Agency Mobile No 2</label>
                          <input type="text" class="form-control square" placeholder="Agency Mobile No 2" name="mobile02"  />
                        </div>
                        

                        <button type="submit" name="submit" class="btn btn-primary">
                          <i class="fa fa-check-square-o"></i> Save
                        </button>
                        
                      </div>
                      <div class="form-actions right"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
                <div class="col-md-7">
              <div class="card">
                <div class="card-body collapse in">
                  <div class="bg-primary bg-lighten-1 height-50">
                      <div class="card-header">
                          <h4 class="card-title" id="basic-layout-square-controls" style="color:#fff">List Of Speed Governor</h4>                  
                        </div>
                    </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th width="5%">SNo</th>
                          <th width="70%">Title</th>
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
             $cnt=0;
              $res01=$db->query(" SELECT * FROM `techs_speed_governor` order by id desc ");
            while( $row01=$res01->fetch_assoc() ){
             ?>
                          <tr>
                              <td><?=++$cnt?></td>
                                <td> <?=$row01['title']?></td>
                              <td>
                                <a href="javascript:;" data-toggle="modal" class="btn btn-success btn-sm" onClick="get(<?=$row01['id']?>)"><i class="ft-edit"></i></a>
                                <button class="btn btn-danger btn-sm" onClick="del(<?=$row01['id']?>)" ><i class="ft-trash"></i></button></td>
                            </tr>
                       <?php  } ?>
                    
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>SNo</th>
                          <th>Title</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
    </div>


</div>

 <div class="modal fade text-xs-left" id="result" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel10" align="center">Update Record</h4>
        </div>
          
            <div class="record_result"></div>
        
      </div>
    </div>
  </div>

<?php include("footer.php") ?>
<script src="../app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">



<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.zero-configuration').DataTable();
  $(".select2").select2();
  $('#form').validate({     
           rules: { 
            title : { required: true, },
            state_id : { required: true, },
            rto_id : { required: true, },
            agency_name : { required: true, },
            agency_address : { required: true, },
            mobile01 : { required: true, }, 
        },
        messages:{
             title : {  required: "Please Enter title", }, 
             state_id : {  required: "Please Select State", }, 
             rto_id : {  required: "Please Select RTO", }, 
             agency_name : {  required: "Please Enter Agency Name", }, 
             agency_address : {  required: "Please Enter Agency Address", }, 
             mobile01 : {  required: "Please Enter Mobile Number", }, 
        } ,
  });

    $(".state_id").change(function(event) {
      $("body").loading('start');
      if( $(this).val()!='' ){
          $.post('ajax/getdata.php', {con: '3',state_id:$(this).val()}, function(resp) {
            $(".rtid").html(resp); $(".select2").select2(); $("body").loading('stop');
          });
      }else{
        $(".rtid").html('<select name="rto_id" class="select2 rto_id" style="width: 100%">'
                              +'<option value="">Please select RTO</option>'
                            +'</select>'); $(".select2").select2(); $("body").loading('stop');
      }
    });

});

function del(id){
    $.confirm({
       theme: 'light',
       title: 'Alert !!!',
       content: 'Do you want to remove this record ?',
         buttons: {
            confirm: function () {
              window.location="?id="+id+"&p=1"; 
            },
            cancel: function () {
            },
         }
    });
  }
    
  function get(id){
    $("#result").modal("hide");
    $("body").loading('start');
    $.post('ajax/getdata.php',{
                id:id,con:6
              },function(resp){
                $(".record_result").html(resp);
              }
    );
    $("body").loading('stop'); $("#result").modal("show");
  }
</script>